#!/bin/bash
# Script
#     foamRunTutorialsBatch.sh
#
# Description
#     Submit jobs of OpenFOAM tutorial cases using foamRunTutorialsBatch script

## Sample settings
case $HOSTNAME in
    wisteria0[1-6])
	# Witeria/BDEC-01
	export FOAM_RUN_TUTORIALS_BATCH_SUBMIT='pjsub -g gz00 -L rscgrp=regular-o -L node=1 -L elapse=48:00:00 --mpi proc=48 -j -S -N log-foamRunTutorials'
	export FOAM_RUN_TUTORIALS_BATCH_JOBS='pjstat | grep "^[0-9]" | wc -l'
	export FOAM_RUN_TUTORIALS_BATCH_LIMIT=128
	export FOAM_RUN_TUTORIALS_BATCH_HEADER='module load openfoam/v2106;source $WM_PROJECT_DIR/etc/bashrc'
	;;
    flow-fx0[1-9])
	# Nagoya university information technology center "Flow" Type I
	export FOAM_RUN_TUTORIALS_BATCH_SUBMIT='pjsub -L rscunit=fx -L rscgrp=fx-small -L node=1 -L elapse=168:00:00 --mpi proc=48 -j -S -N log-foamRunTutorials'
	export FOAM_RUN_TUTORIALS_BATCH_JOBS='pjstat | grep "^[0-9]" | wc -l'
	export FOAM_RUN_TUTORIALS_BATCH_LIMIT=100
	export FOAM_RUN_TUTORIALS_BATCH_HEADER='module purge;module load tcs/1.2.31 openfoam/v2006;source $WM_PROJECT_DIR/etc/bashrc'
	;;
esac

# Execute foamRunTutorialsBatch
foamRunTutorialsBatch $*
