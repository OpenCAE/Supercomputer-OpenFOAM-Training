#!/bin/bash

module load vtune

for dir in vtune.*
do
    [ -d $dir ] || continue
    [ -f $dir/.norun ] || continue
    output=$dir.csv
    [ -f $output ] || amplxe-cl -R hotspots -r $dir -format=csv -csv-delimiter=',' > $output
done
