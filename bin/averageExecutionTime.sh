#!/bin/bash

echo  "#Filename,TimeSteps[-],InitTime[s],LastTime[s],Time[s],AveTime[s]"

for log in $*
do
    grep "^ExecutionTime" $log >& /dev/null || continue
    awk \
'/^ExecutionTime/ {(n==0) && t0=$3;n++;t=$3} \
END {printf "%s,%d,%g,%g,%g,%g\n",FILENAME,n,t0,t,t-t0,(t-t0)/(n-1)}' \
$log
done
