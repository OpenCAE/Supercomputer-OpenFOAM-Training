#!/bin/bash
#PJM -g gt00
#PJM -L rg=lecture-o
#PJM -L node=1
#PJM --mpi proc=48
#PJM -L elapse=0:15:00
#PJM -S
module purge                      # 標準で有効なmoduleをpurgeで全てunload
module load fj/1.2.35             # FJのmoduleをload
module load fjmpi/1.2.35          # FJ MPIのmoduleをload
module load openfoam/v2106        # OpenFOAMのmoduleをload
source $WM_PROJECT_DIR/etc/bashrc # OpenFOAMの環境設定
# foamRunTutorialsコマンドでチュートリアルを実行
foamRunTutorials
