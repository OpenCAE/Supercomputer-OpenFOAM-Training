# OpenFOAM
alias OFv1812='module purge;module load gcc/4.8.5;module load openfoam/v1812;source $WM_PROJECT_DIR/etc/bashrc'
alias OF6='module purge;module load gcc/4.8.5;module load openfoam/6;source $WM_PROJECT_DIR/etc/bashrc'
# ParaView
alias PV561='module purge;module load intel/2019.4.243;module load llvm/7.1.0;module load paraview/5.6.1'
