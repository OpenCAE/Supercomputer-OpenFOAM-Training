#!/bin/sh

# Versions
latest_gcc_module_versions=gcc/7.5.0
gcc_versions=gcc@10.2.0
adios_versions=adios2@2.5.0
impi_versions=intel-mpi@2019.5.281
openfoam_versions_list="openfoam@2006"
#openfoam_versions_list="openfoam@2006 openfoam@1912"

# Spack root directory
export SPACK_ROOT=$HOME/spack

# Install spack
WORKDIR=/work/$(id -gn)/$(id -un)
cd $WORKDIR
[ -d spack ] || git clone https://github.com/spack/spack.git
[ -L $SPACK_ROOT ] || ln -s $WORKDIR/spack $SPACK_ROOT
[ -d $WORKDIR/.spack ] || mkdir $WORKDIR/.spack
[ -L $HOME/.spack ] || ln -s $WORKDIR/.spack $HOME/.spack
source $SPACK_ROOT/share/spack/setup-env.sh
export LC_ALL=ja_JP.utf8
cd $SPACK_ROOT

# Load latest gcc module
module purge
module load $latest_gcc_module_versions

# Update compiler list
spack compiler find
spack compiler list

# Install gcc
spack compiler list | grep $gcc_versions >& /dev/null || \
spack install $gcc_versions
spack load $gcc_versions

# Update compiler list
spack compiler find
spack compiler list

file=$HOME/.spack/linux/packages.yaml
if [ ! -f $file ]
then
    cat > $file <<EOF
packages:
  intel-mpi:
    paths:
EOF
    (
	for dir in /work/opt/local/cores/intel/compilers_and_libraries_*.*
	do
	    echo "      intel-mpi@${dir##*_}: $dir/linux/mpi/intel64"
	done
    ) >> $file
    cat >> $file <<EOF
  all:
    compiler: [ $gcc_versions ]
EOF
fi

# Install OpenFOAM
for openfoam_versions in $openfoam_versions_list
do
    args="$openfoam_versions %$gcc_versions ^$impi_versions"
    case "$openfoam_versions" in
	openfoam-org*)
	    spack install $args
	    ;;
	*)
	    spack install $args ^$adios_versions
	    ;;
    esac
done
