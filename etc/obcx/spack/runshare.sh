#!/bin/bash
module purge
module load gcc/7.5.0
module load impi/2019.5.281
export HOME=/work/$(id -gn)/$(id -un)
export SPACK_ROOT=$HOME/spack
source $SPACK_ROOT/share/spack/setup-env.sh
spack load openfoam@2006 ^intel-mpi@2019.5.281
source $WM_PROJECT_DIR/etc/bashrc
jobid=${PJM_SUBJOBID:-$PJM_JOBID}
