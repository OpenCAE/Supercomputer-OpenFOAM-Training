# OpenFOAM
alias OFv1706='module purge;module load openfoam/v1706;source $WM_PROJECT_DIR/etc/bashrc'
alias OFv1812='module purge;module load openfoam/v1812_ompi;source $WM_PROJECT_DIR/etc/bashrc'
alias OFv1912='module purge;module load openfoam/v1912_ompi;source $WM_PROJECT_DIR/etc/bashrc'
alias OFv2106OM='module purge;module load cmake/3.18.2 gcc/9.2.0 openmpi/3.1.6-nocuda-gcc9.2.0;source $HOME/OpenFOAM/OpenFOAM-v2106/etc/bashrc WM_COMPILER=Gcc9.2.0 WM_MPLIB=SYSTEMOPENMPI'
alias OFv2106IM='module purge;module load cmake/3.18.2 gcc/10.2.0 intel/2020.1;source $HOME/OpenFOAM/OpenFOAM-v2106/etc/bashrc WM_COMPILER=Gcc10.2.0 WM_MPLIB=INTELMPI'
