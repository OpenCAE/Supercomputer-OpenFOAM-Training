#!/bin/bash
#PJM -L "rscunit=ito-a"
#PJM -L "rscgrp=ito-ss-dbg"
#PJM -L "vnode=1"
#PJM -L "vnode-core=36"
#PJM -L "elapse=0:05:00"
#PJM --mpi "proc=1"
#PJM -j
#PJM -S
# Share settings
source ./runShare.sh
# Run solver
$application &> log.$application.$jobid
