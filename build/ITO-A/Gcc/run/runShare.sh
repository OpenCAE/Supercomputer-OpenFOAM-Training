#!/bin/bash

# LANG
export LANG=C
export LC_ALL=C

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# module
module purge

# OpenFOAM and MPI settings
# OpenMPI
#module load cmake/3.18.2 gcc/9.2.0 openmpi/3.1.6-nocuda-gcc9.2.0
#source $HOME/OpenFOAM/OpenFOAM-v2106/etc/bashrc WM_COMPILER_TYPE=system WM_COMPILER=Gcc9.2.0 WM_PRECISION_OPTION=DP WM_LABEL_SIZE=32 WM_COMPILE_OPTION=Opt WM_MPLIB=SYSTEMOPENMPI
#mpirun="mpiexec --report-bindings --display-map -display-devel-map  -machinefile "${PJM_O_NODEINF}" -mca orte_rsh_agent /bin/pjrsh --map-by ppr:18:socket"

# INTEL MPI
module load cmake/3.18.2 gcc/10.2.0 intel/2020.1
source $HOME/OpenFOAM/OpenFOAM-v2106/etc/bashrc WM_COMPILER_TYPE=system WM_COMPILER=Gcc10.2.0 WM_PRECISION_OPTION=DP WM_LABEL_SIZE=32 WM_COMPILE_OPTION=Opt WM_MPLIB=INTELMPI
mpirun=mpiexec.hydra
unset I_MPI_PIN_DOMAIN
export I_MPI_PERHOST=36
export I_MPI_PIN_PROCESSOR_LIST=18-35,0-17
export I_MPI_HYDRA_BOOTSTRAP=rsh
export I_MPI_HYDRA_BOOTSTRAP_EXEC=/bin/pjrsh
export I_MPI_HYDRA_HOST_FILE="${PJM_O_NODEINF}"
export I_MPI_DEBUG=6

# Tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

# Application name
application=$(getApplication)
