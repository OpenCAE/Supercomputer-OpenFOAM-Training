#!/bin/bash
#PJM -L "rscunit=ito-a"
#PJM -L "rscgrp=ito-s-dbg"
#PJM -L "vnode=2"
#PJM -L "vnode-core=36"
#PJM -L "elapse=0:05:00"
#PJM --mpi "proc=72"
#PJM -j
#PJM -S
# Share settings
source ./runShare.sh
# Run solver
$mpirun -np $PJM_MPI_PROC $application  -parallel &> log.$application.$jobid
