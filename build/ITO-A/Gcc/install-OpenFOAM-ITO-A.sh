#!/bin/bash

# OpenFOAM version
openfoam_version=v2106

# Versions of third-party software 
cmake_version=3.18.2
gcc_version=10.2.0
intelmpi_version=2020.1
#gcc_version=9.2.0
#openmpi_version=3.1.6

# Set variable of precision
WM_PRECISION_OPTION=DP # DP | SP | SPDP

# Set variable of compile option name
WM_COMPILE_OPTION=Opt

# Set variable of compiler
WM_COMPILER=Gcc${gcc_version}

# Work directory
work=$HOME

# Make OpenFOAM direcrory
[ ! -d $work/OpenFOAM ] && \
mkdir $work/OpenFOAM

# Fetch OpenFOAM source files
pushd $work/OpenFOAM
[ ! -f OpenFOAM-${openfoam_version}.tgz ] && \
wget --no-check-certificate https://sourceforge.net/projects/openfoam/files/${openfoam_version}/OpenFOAM-${openfoam_version}.tgz

[ ! -d OpenFOAM-${openfoam_version} ] && \
tar xf OpenFOAM-${openfoam_version}.tgz

[ ! -f ThirdParty-${openfoam_version}.tgz ] &&\
wget --no-check-certificate https://sourceforge.net/projects/openfoam/files/${openfoam_version}/ThirdParty-${openfoam_version}.tgz

[ ! -d ThirdParty-${openfoam_version} ] && \
tar xf ThirdParty-${openfoam_version}.tgz

popd

# Fetch metis source files
pushd $work/OpenFOAM/ThirdParty-${openfoam_version}
[ ! -f metis-5.1.0.tar.gz ] && \
wget --no-check-certificate http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz

[ ! -d metis-5.1.0 ] && \
tar xf metis-5.1.0.tar.gz
popd

# Adjust hardcoded installation versions
pushd $work/OpenFOAM/OpenFOAM-${openfoam_version}
bin/tools/foamConfigurePaths -system ${WM_COMPILER}
bin/tools/foamConfigurePaths -cmake cmake-system
#bin/tools/foamConfigurePaths -adios ADIOS2-2.6.0
bin/tools/foamConfigurePaths -adios nons
bin/tools/foamConfigurePaths -mpi INTELMPI
#bin/tools/foamConfigurePaths -mpi SYSTEMOPENMPI
popd

# Create job file of OpenFOAM build
job=${0#*/}-${openfoam_version}-${WM_COMPILER}${WM_PRECISION_OPTION}${WM_COMPILE_OPTION}.pjm

cat > $job <<EOF
#!/bin/bash
#PJM -L "rscunit=ito-a"
#PJM -L "rscgrp=ito-ss"
#PJM -L "vnode=1"
#PJM -L "vnode-core=36"
#PJM -L "elapse=8:00:00"
#PJM --mpi "proc=36"
#PJM -j
#PJM -S
# Purge modules
module purge
# Load dependant modules need for building OpenFOAM
module load cmake/${cmake_version}
module load gcc/${gcc_version}
module load intel/${intelmpi_version}
#module load openmpi/${openmpi_version}-nocuda-gcc${gcc_version}
# Work directory
work=$work
# Build with full cores
export WM_NCOMPPROCS=36
# OpenFOAM environmental settings
source \$work/OpenFOAM/OpenFOAM-${openfoam_version}/etc/bashrc \
WM_COMPILER=${WM_COMPILER} \
WM_PRECISION_OPTION=${WM_PRECISION_OPTION} \
WM_COMPILE_OPTION=${WM_COMPILE_OPTION}
# Move to OpenFOAM project directory
cd \$WM_PROJECT_DIR
# Build third party libraries and OpenFOAM libraries and applications
./Allwmake -k
EOF

pjsub $job
