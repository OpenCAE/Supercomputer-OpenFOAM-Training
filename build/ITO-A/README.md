[[_TOC_]]

# 九州大学情報基盤研究開発センターITO-AにおけるGccを用いたOpenFOAMのビルド

## OpenFOAMの自動ビルド方法

以下では，例としてGcc-10.2.0を用いてOpenFOAM-v2106を自動でビルドする方法を示す．
異なるバージョンのビルドも可能であるが，スクリプトやパッチを適宜変更する必要がある．

### レポジトリの取得

```bash
git clone git@gitlab.com:OpenCAE/Supercomputer-OpenFOAM-Training.git
# 上記が失敗する場合には，以下を実行
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
```

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd Supercomputer-OpenFOAM-Training/build/ITO-A/Gcc
```

[Gccディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/ITO-A/Gcc/)

### 自動ビルドスクリプトの実行

以下のようにして自動ビルドスクリプトを実行する．

```bash
./install-OpenFOAM-ITO-A.sh
```

[install-OpenFOAM-ITO-A.shの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/ITO-A/Gcc/install-OpenFOAM-ITO-A.sh)

## OpenFOAM実行用のジョブファイルの例
OpenFOAM実行用のジョブファイルの例は，`run`ディレクトリにある．

[runディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/ITO-A/Gcc/run/)
