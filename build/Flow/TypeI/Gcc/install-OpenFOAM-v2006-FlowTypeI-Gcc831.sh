#!/bin/sh

# Make OpenFOAM direcrory
[ ! -d /data/group1/$(id -un)/OpenFOAM ] && \
mkdir /data/group1/$(id -un)/OpenFOAM

[ ! -L ~/OpenFOAM ] && \
ln -s /data/group1/$(id -un)/OpenFOAM ~/OpenFOAM

# Fetch OpenFOAM source files
pushd ~/OpenFOAM

[ ! -f OpenFOAM-v2006.tgz ] && \
wget https://sourceforge.net/projects/openfoam/files/v2006/OpenFOAM-v2006.tgz

[ ! -d OpenFOAM-v2006 ] && \
tar xf OpenFOAM-v2006.tgz

[ ! -f ThirdParty-v2006.tgz ] &&\
wget https://sourceforge.net/projects/openfoam/files/v2006/ThirdParty-v2006.tgz

[ ! -d ThirdParty-v2006 ] && \
tar xf ThirdParty-v2006.tgz

popd

# Apply patches
[ ! -f ~/OpenFOAM/OpenFOAM-v2006/PATCHED ] && \
patch -d ~/OpenFOAM/OpenFOAM-v2006 -b -p1 < patches/OpenFOAM-v2006.diff &> ~/OpenFOAM/OpenFOAM-v2006/PATCHED

# Create job file of OpenFOAM build
cat > install-OpenFOAM-v2006-FlowTypeI-Gcc831.pjm <<'EOF'
#!/bin/bash
#PJM -L rscunit=fx
#PJM -L rscgrp=fx-extra
#PJM -L node=1
#PJM -L elapse=12:00:00
#PJM --mpi proc=48
#PJM -j
#PJM -S
# 標準で設定されているmoduleを無効化
module purge
# OpenFOAMの依存moduleのロード(tcs/1.2.31は富士通MPI用)
module load tcs/1.2.31 scotch/6.0.8 petsc/3.13.1 fftw-tune/3.3.8 metis/5.1.0 cmake/3.17.1
# 最大48並列でビルド
export WM_NCOMPPROCS=48
# OpenFOAMの環境設定(Gccのバージョンに応じてGcc831を変更)
source ~/OpenFOAM/OpenFOAM-v2006/etc/bashrc WM_COMPILER=Gcc831
# OpenFOAMのソースディレクトリに移動
cd $WM_PROJECT_DIR
# OpenFOAMのビルド
./Allwmake -k
EOF

# Submit job of OpenFOAM build
pjsub install-OpenFOAM-v2006-FlowTypeI-Gcc831.pjm
