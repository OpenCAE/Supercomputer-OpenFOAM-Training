#!/bin/bash
#PJM -L rscunit=fx
#PJM -L rscgrp=fx-extra
#PJM -L node=1
#PJM -L elapse=12:00:00
#PJM --mpi proc=1
#PJM -j
#PJM -S
# 標準で設定されているmoduleを無効化
module purge
# OpenFOAMの依存moduleのロード(tcs/1.2.31は富士通MPI用)
module load tcs/1.2.31 scotch/6.0.8 petsc/3.13.1 fftw-tune/3.3.8 metis/5.1.0 cmake/3.17.1
# OpenFOAMの環境設定(Gccのバージョンに応じてGcc1030を変更)
source ~/OpenFOAM/OpenFOAM-v2006/etc/bashrc WM_COMPILER=Gcc1030
# 実行用関数定義
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions
# 実行アプリケーション名
application=$(getApplication)
# ジョブID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}
# 領域分割数
np=$(getNumberOfProcessors)
# ログファイル名
log=log.$application.$jobid
# アプリケーション実行
$application &> $log
