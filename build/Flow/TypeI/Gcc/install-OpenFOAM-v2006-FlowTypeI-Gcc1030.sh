#!/bin/sh

# Make OpenFOAM direcrory
[ ! -d /data/group1/$(id -un)/OpenFOAM ] && \
mkdir /data/group1/$(id -un)/OpenFOAM

[ ! -L ~/OpenFOAM ] && \
ln -s /data/group1/$(id -un)/OpenFOAM ~/OpenFOAM

# Fetch OpenFOAM source files
pushd ~/OpenFOAM

[ ! -f OpenFOAM-v2006.tgz ] && \
wget https://sourceforge.net/projects/openfoam/files/v2006/OpenFOAM-v2006.tgz

[ ! -d OpenFOAM-v2006 ] && \
tar xf OpenFOAM-v2006.tgz

[ ! -f ThirdParty-v2006.tgz ] &&\
wget https://sourceforge.net/projects/openfoam/files/v2006/ThirdParty-v2006.tgz

[ ! -d ThirdParty-v2006 ] && \
tar xf ThirdParty-v2006.tgz

popd

# Fetch ThirdParty source files
pushd ~/OpenFOAM/ThirdParty-v2006

[ ! -f gcc-10.3.0.tar.xz ] && \
wget http://ftp.tsukuba.wide.ad.jp/software/gcc/releases/gcc-10.3.0/gcc-10.3.0.tar.xz

[ ! -d gcc-10.3.0 ] && \
tar xf gcc-10.3.0.tar.xz

[ ! -f gmp-6.2.1.tar.xz ] && \
wget ftp://ftp.gnu.org/gnu/gmp/gmp-6.2.1.tar.xz

[ ! -d gmp-6.2.1 ] && \
tar xf gmp-6.2.1.tar.xz

[ ! -f mpfr-4.1.0.tar.xz ] && \
wget ftp://ftp.gnu.org/gnu/mpfr/mpfr-4.1.0.tar.xz

[ ! -d mpfr-4.1.0 ] && \
tar xf mpfr-4.1.0.tar.xz

[ ! -f mpc-1.1.0.tar.gz ] && \
wget ftp://ftp.gnu.org/gnu/mpc/mpc-1.1.0.tar.gz

[ ! -d mpc-1.1.0 ] && \
tar xf mpc-1.1.0.tar.gz

popd

# Apply patches
[ ! -f ~/OpenFOAM/OpenFOAM-v2006/PATCHED ] && \
patch -d ~/OpenFOAM/OpenFOAM-v2006 -b -p1 < patches/OpenFOAM-v2006.diff &> ~/OpenFOAM/OpenFOAM-v2006/PATCHED

# Adjust hardcoded installation versions
source ~/OpenFOAM/OpenFOAM-v2006/etc/bashrc
$WM_PROJECT_DIR/bin/tools/foamConfigurePaths gmp-6.2.1
$WM_PROJECT_DIR/bin/tools/foamConfigurePaths mpfr-4.1.0
$WM_PROJECT_DIR/bin/tools/foamConfigurePaths mpc-1.1.0

# Create job file of OpenFOAM build
cat > install-OpenFOAM-v2006-FlowTypeI-Gcc1030.pjm <<'EOF'
#!/bin/bash
#PJM -L rscunit=fx
#PJM -L rscgrp=fx-extra
#PJM -L node=1
#PJM -L elapse=12:00:00
#PJM --mpi proc=48
#PJM -j
#PJM -S
# 標準で設定されているmoduleを無効化
module purge
# OpenFOAMの依存moduleのロード(tcs/1.2.31は富士通MPI用)
module load tcs/1.2.31 scotch/6.0.8 petsc/3.13.1 fftw-tune/3.3.8 metis/5.1.0 cmake/3.17.1
# 最大48並列でビルド
export WM_NCOMPPROCS=48
# OpenFOAMの環境設定(Gccのバージョンに応じてGcc1030を変更)
source ~/OpenFOAM/OpenFOAM-v2006/etc/bashrc WM_COMPILER=Gcc1030
# 依存ライブラリのディレクトリに移動(Gccをビルドしない場合不要)
cd $WM_THIRD_PARTY_DIR
# Gccと依存ライブラリのビルド(Gccをビルドしない場合不要)
./makeGcc gcc-10.3.0 gmp-6.2.1 mpfr-4.1.0 mpc-1.1.0
# OpenFOAMのソースディレクトリに移動
cd $WM_PROJECT_DIR
# OpenFOAMのビルド
./Allwmake -k
EOF

# Submit job of OpenFOAM build
pjsub install-OpenFOAM-v2006-FlowTypeI-Gcc1030.pjm

