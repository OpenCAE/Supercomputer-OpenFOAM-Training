#!/bin/bash
#PJM -L rscunit=fx
#PJM -L rscgrp=fx-small
#PJM -L node=1
#PJM -L elapse=01:00:00
#PJM --mpi proc=48
#PJM -j
#PJM -S

# Installation prefix and versions
prefix=/data/group2/others/opt/local
openfoam_version=v2406
tcs_version=1.2.39

# Set variable of precision
WM_PRECISION_OPTION=DP # DP | SP | SPDP

# Set variable of compile option name
WM_COMPILE_OPTION=Opt

# Installation directory
dir=${prefix}/aarch64/apps/tcsds/${tcs_version}
openfoam_dir=${dir}/openfoam/${openfoam_version}

# Load dependant modules
module purge
module load tcs/${tcs_version}
module load cmake fftw petsc scotch

# Perform environmental settings of OpenFOAM
source ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/bashrc \
WM_PRECISION_OPTION=${WM_PRECISION_OPTION} \
WM_COMPILE_OPTION=${WM_COMPILE_OPTION}
eval $(foamEtcFile -sh -config petsc -- -force)

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Log filename
log=log.${application}.${jobid}

# Run solver
mpiexec -of ${log} ${application} -parallel
