#!/bin/bash
module load tcs/${tcs_version}
env
cd src/petsc
CC=mpifcc
CXX=mpiFCC
./configure\
    --prefix=${PETSC_ARCH_PATH}\
    --PETSC_ARCH=${PETSC_ARCH}\
    --with-64-bit-indices=0\
    --with-precision=double\
    --with-default-arch=0\
    --with-clanguage=C\
    --with-fc=0\
    --with-x=0\
    --with-packages-download-dir=..\
    --download-f2cblaslapack=../f2cblaslapack-3.8.0.q2.tar.gz\
    --download-hypre\
    --with-debugging=0\
    --COPTFLAGS="-O3"\
    --CXXOPTFLAGS="-O3"\
    --with-cc=${CC}\
    --with-cxx=${CXX}
make -j ${PJM_PROC_BY_NODE:-4} all
make install
