#!/bin/bash

# Installation prefix and versions
if [ "$#" -ne 3 ]
then
    echo "usage: $0 installation_prefix openfoam_version tcs_version"
    echo
    echo "usage(eg): $0 ~/opt/local v2406 1.2.39"
    exit 1
fi
prefix=$1
openfoam_version=$2
tcs_version=$3

# Set variable of precision
WM_PRECISION_OPTION=DP # DP | SP | SPDP

# Set variable of compile option name
WM_COMPILE_OPTION=Opt

# Set variable of compiler
WM_COMPILER=FClang

# Set variable of MPI implementation
WM_MPLIB=FJMPI

# Make installation and source directory
dir=${prefix}/aarch64/apps/tcsds/${tcs_version}
openfoam_dir=${dir}/openfoam/${openfoam_version}
src_dir=${PWD}/src
export PETSC_ARCH=arch-linux-c-opt
export PETSC_ARCH_PATH=${dir}/petsc/${PETSC_ARCH}
[ ! -d n${openfoam_dir} ] && mkdir -p ${openfoam_dir}
[ ! -d ${src_dir} ] && mkdir ${src_dir}

# Build flex-2.6.4
export PATH=/home/center/opt/x86_64/cores/gcc/8.4.0/bin:$PATH
export LD_LIBRARY_PATH=/home/center/opt/x86_64/cores/gcc/8.4.0/lib64:$LD_LIBRARY_PATH
flex_dir=${prefix}/x86_64/apps/gcc/8.4.0/flex/2.6.4
if [ ! -x ${flex_dir}/bin/flex ]
then
    (
	cd ${src_dir}
	[ ! -d flex-2.6.4 ] && wget --no-check-certificate -o /dev/null -O - https://github.com/westes/flex/releases/download/v2.6.4/flex-2.6.4.tar.gz | tar zx
	(
	    cd flex-2.6.4
	    [ ! -d build ] && mkdir build
	    (
		cd build
		../configure --prefix=${flex_dir}
		make -j 4
		make install
	    )
	)
    )
fi
export PATH=${flex_dir}/bin:$PATH

# Build PETSc, HYPRE
if [ ! -f ${PETSC_ARCH_PATH}/lib/libpetsc.so ]
then
    [ ! -d ${src_dir}/petsc ] && (cd ${src_dir};git clone -b release https://gitlab.com/petsc/petsc.git)
    [ ! -f ${src_dir}/f2cblaslapack-3.8.0.q2.tar.gz ] && (cd ${src_dir};wget --no-check-certificate -o /dev/null https://web.cels.anl.gov/projects/petsc/download/externalpackages/f2cblaslapack-3.8.0.q2.tar.gz)
    [ ! -d ${src_dir}/hypre ] && (cd ${src_dir};git clone https://github.com/hypre-space/hypre)
    while [ ! -f ${PETSC_ARCH_PATH}/lib/libpetsc.so ]
    do
	jid=$(pjsub -L rscunit=fx,rg=fx-workshop,elapse=00:30:00,node=1 --mpi proc=48 -s -z jid -x "PETSC_ARCH=${PETSC_ARCH},PETSC_ARCH_PATH=${PETSC_ARCH_PATH},tcs_version=${tcs_version}" install-petsc-FlowTypeI-Fcc.sh)
	pjwait -z ${jid}
    done
fi

# Fetch OpenFOAM source files
(cd ${openfoam_dir}
 if [ ! -d OpenFOAM-${openfoam_version} ];then
     wget --no-check-certificate -o /dev/null -O - https://sourceforge.net/projects/openfoam/files/${openfoam_version}/OpenFOAM-${openfoam_version}.tgz | tar zx
     (cd OpenFOAM-${openfoam_version}
      git init
      git add .
     )
 fi
 if   [ ! -d ThirdParty-${openfoam_version} ];then
     wget --no-check-certificate -o /dev/null -O - https://sourceforge.net/projects/openfoam/files/${openfoam_version}/ThirdParty-${openfoam_version}.tgz | tar zx
     (cd ThirdParty-${openfoam_version}
      git init
      git add .
     )
 fi
)

# Apply patches
patch=OpenFOAM-${openfoam_version}.diff
if [ ! -f ${openfoam_dir}/OpenFOAM-${openfoam_version}/${patch} ]
then
    patch -N -b -p1 -d ${openfoam_dir}/OpenFOAM-${openfoam_version} < ../../../FX1000/Fcc/patches/${patch}
    (
	cd ${openfoam_dir}/OpenFOAM-${openfoam_version} 
	git add -N wmake/rules/A64FXFClang/{c,c++,c++Debug,c++Opt,c++Prof,cDebug,cOpt,cProf,general}
	[ -f etc/config.sh/readline ] && git add -N etc/config.sh/readline
	git diff > ${patch}
    )
fi
patch=ThirdParty-${openfoam_version}.diff
if [ ! -f ${openfoam_dir}/ThirdParty-${openfoam_version}/${patch} ]
then
    patch -N -b -p1 -d ${openfoam_dir}/ThirdParty-${openfoam_version} < ../../../FX1000/Fcc/patches/${patch}
    (
	cd ${openfoam_dir}/ThirdParty-${openfoam_version} 
	git diff > ${patch}
    )
fi

# Load dependant modules need for building OpenFOAM
module purge
module load tcs/${tcs_version}
module load cmake fftw petsc scotch
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SCOTCH_LIB

# Adjust hardcoded installation versions
cd ${openfoam_dir}/OpenFOAM-${openfoam_version}
bin/tools/foamConfigurePaths -fftw fftw-system -fftw-path ${FFTW_DIR}
bin/tools/foamConfigurePaths -scotch scotch-system -scotch-path $(expr ":$INCLUDE_PATH:" : ".*:\(/[^:]*/scotch/[^:]\+\)/include:.*")
[ -f etc/config.sh/petsc ] && \
sed -i -e s"|^\(petsc_version=\).*$|\1petsc-system|" -e s"|^\(export PETSC_ARCH_PATH=\).*$|\1${PETSC_ARCH_PATH}|" etc/config.sh/petsc

# OpenFOAM environmental settings
source etc/bashrc WM_PRECISION_OPTION=${WM_PRECISION_OPTION}

# Build OpenFOAM
./Allwmake -k -j 4
