#!/bin/bash

# Installation prefix and versions
if [ "$#" -ne 5 ]
then
    echo "usage: $0 installation_prefix openfoam_version gcc_version cuda_verion openmpi_cuda_version"
    echo
    echo "usage(eg): $0 ~/opt/local v2406 11.3.0 12.1.1 4.0.5"
    exit 1
fi
prefix=$1
openfoam_version=$2
gcc_version=$3
cuda_version=$4
openmpi_cuda_version=$5

# Set variable of precision
WM_PRECISION_OPTION=DP # DP | SP | SPDP

# Set variable of compile option name
WM_COMPILE_OPTION=Opt

# Set variable of compiler
WM_COMPILER=Gcc

# Set variable of MPI implementation
WM_MPLIB=SYSTEMOPENMPI

# Load modules
module purge
module load gcc/${gcc_version} cuda/${cuda_version} openmpi_cuda/${openmpi_cuda_version}
module load cmake/3.25.2

# Make installation and source directory
dir=${prefix}/x86_64/apps/gcc/${gcc_version}/cuda/${cuda_version}/openmpi_cuda/${openmpi_cuda_version}
openfoam_dir=${dir}/openfoam/${openfoam_version}
platforms_dir=${openfoam_dir}/OpenFOAM-${openfoam_version}/platforms/linux64${WM_COMPILER}${WM_PRECISION_OPTION}Int32${WM_COMPILE_OPTION}
src_dir=${PWD}/src
export CUDA_INC=${CUDA_HOME}/include
export PETSC_ARCH=arch-linux-c-opt
export PETSC_ARCH_PATH=${dir}/petsc/${PETSC_ARCH}
export PETSC_INC_DIR=${PETSC_ARCH_PATH}/include
export PETSC_INC=${PETSC_INC_DIR}
export PETSC_LIB_DIR=${PETSC_ARCH_PATH}/lib
export AMGX_DIR=${dir}/amgx/main
export AMGX_INC=${AMGX_DIR}/include
export AMGX_LIB=${AMGX_DIR}/lib
export SPECTRUM_MPI_HOME=${OPENMPI_DIR}
export CUBROOT=${src_dir}/amgx/thrust/dependencies
export FOAM2CSR_INC=${src_dir}/foam2csr/src
[ ! -d ${openfoam_dir} ] && mkdir -p ${openfoam_dir}
[ ! -d ${src_dir} ] && mkdir ${src_dir}
[ ! -d ${AMGX_DIR} ] && mkdir -p ${AMGX_DIR}
[ ! -d ${PETSC_ARCH_PATH} ] && mkdir -p ${PETSC_ARCH_PATH}

# Add shared library path
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CUDA_DIR}/compat:${AMGX_LIB}:${PETSC_LIB_DIR}

##
## OpenFOAM
##
# Fetch OpenFOAM source files
(cd ${openfoam_dir}
 if [ ! -d OpenFOAM-${openfoam_version} ];then
     wget --no-check-certificate -o /dev/null -O - https://sourceforge.net/projects/openfoam/files/${openfoam_version}/OpenFOAM-${openfoam_version}.tgz | tar zx
     (cd OpenFOAM-${openfoam_version}
      git init
      git add .
     )
 fi
 if   [ ! -d ThirdParty-${openfoam_version} ];then
     wget --no-check-certificate -o /dev/null -O - https://sourceforge.net/projects/openfoam/files/${openfoam_version}/ThirdParty-${openfoam_version}.tgz | tar zx
     (cd ThirdParty-${openfoam_version}
      git init
      git add .
     )
 fi
)

# Adjust hardcoded installation versions
cd ${openfoam_dir}/OpenFOAM-${openfoam_version}
[ -f etc/config.sh/petsc ] && \
sed -i -e s"|^\(petsc_version=\).*$|\1petsc-system|" -e s"|^\(export PETSC_ARCH_PATH=\).*$|\1${PETSC_ARCH_PATH}|" etc/config.sh/petsc
[ -f modules/external-solver/Allwmake ] && mv modules/external-solver/Allwmake modules/external-solver/Allwmake.disabled

# OpenFOAM environmental settings
source etc/bashrc WM_COMPILER=${WM_COMPILER} WM_MPLIB=${WM_MPLIB} WM_PRECISION_OPTION=${WM_PRECISION_OPTION}

# Build OpenFOAM
./Allwmake -k -j 4

##
## AmgX
##
if [ ! -f ${AMGX_LIB}/libamgx.a ]
then
    [ ! -d ${src_dir}/amgx ] && (cd ${src_dir}/;git clone --recursive https://github.com/nvidia/amgx.git)
    [ ! -d ${src_dir}/amgx/build ] && mkdir ${src_dir}/amgx/build
    (
	cd ${src_dir}/amgx/build
	cmake -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=${AMGX_DIR} -DCUDA_ARCH="70" ..
	make -j 4 all
	make install
    )
fi

##
## PETSc
##
if [ ! -f ${PETSC_ARCH_PATH}/lib/libpetsc.so ]
then
    [ ! -d ${src_dir}/petsc ] && (cd ${src_dir};git clone -b release https://gitlab.com/petsc/petsc.git)
    (
	cd ${src_dir}/petsc
	./configure\
	    --prefix=${PETSC_ARCH_PATH}\
	    --PETSC_ARCH=${PETSC_ARCH}\
	    --with-64-bit-indices=0\
	    --with-precision=double\
	    --with-default-arch=0\
	    --with-clanguage=C\
	    --with-fc=0\
	    --with-x=0\
	    --download-f2cblaslapack\
	    --download-hypre\
	    --with-debugging=0\
	    --COPTFLAGS="-O3"\
	    --CXXOPTFLAGS="-O3"\
	    --with-cuda=1\
	    --gpu-architecture=sm_70\
	    --with-openmpi=1\
	    --with-openmpi-dir=${OPENMPI_DIR}\
	    --force  
	make -j 4 all
	make install
    )
fi

##
## FOAM2CSR
##
if [ ! -f ${platforms_dir}/lib/libfoam2csr.so ]
then
    [ ! -d ${src_dir}/foam2csr ] && (cd ${src_dir};git clone https://gitlab.hpc.cineca.it/openfoam/foam2csr.git)
    (
	cd ${src_dir}/foam2csr
	sed -i 's/\(NVARCH =\).*/\1 70/' Make/nvcc
	source ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/bashrc WM_COMPILER=Gcc WM_MPLIB=SYSTEMOPENMPI
	./Allwmake -prefix=${platforms_dir}
    )
fi

##
## petscFoam
##
if [ ! -f ${platforms_dir}/lib/libpetscFoam.so ]
then
    [ ! -d ${src_dir}/external-solver ] && (cd ${src_dir};git clone --branch amgxwrapper https://develop.openfoam.com/modules/external-solver.git)
    (
	cd ${src_dir}/external-solver
	sed -i 's/-I$(FOAM2CSR_INC)$/-I$(FOAM2CSR_INC) -I$(CUDA_INC)/' src/petsc4Foam/Make/options
	source ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/bashrc WM_COMPILER=Gcc WM_MPLIB=SYSTEMOPENMPI
	sed -i -e s"|^\(petsc_version=\).*$|\1petsc-system|" -e s"|^\(export PETSC_ARCH_PATH=\).*$|\1${PETSC_ARCH_PATH}|" ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/config.sh/petsc
	./Allwclean
	./Allwmake -prefix=${platforms_dir}
    )
fi

