#!/bin/bash
#PJM -L rscunit=cx
#PJM -L rscgrp=cx-debug
#PJM -L node=1
#PJM --mpi proc=4
#PJM -L elapse=0:30:00
#PJM -S

# Installation prefix and versions
prefix=/data/group2/others/opt/local
openfoam_version=v2406
gcc_version=11.3.0
cuda_version=12.1.1
openmpi_cuda_version=4.0.5

# Instal directory
dir=${prefix}/x86_64/apps/gcc/${gcc_version}/cuda/${cuda_version}/openmpi_cuda/${openmpi_cuda_version}

# Load modules
module purge
module load gcc/${gcc_version} cuda/${cuda_version} openmpi_cuda/${openmpi_cuda_version}
module load cmake/3.25.2

# Perform environmental settings of OpenFOAM, AmgX, petsc
source ${dir}/openfoam/${openfoam_version}/OpenFOAM-${openfoam_version}/etc/bashrc
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${dir}/amgx/main/lib
eval $(foamEtcFile -sh -config petsc -- -force)

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Log filename
log=log.${application}.${jobid}

# Run solver
ppn=4
if [ ${PJM_MPI_PROC} -eq 1 ]
then
    ${application} &> ${log}
else
    mpiexec\
	-np ${PJM_MPI_PROC}\
	--machinefile ${PJM_O_NODEINF}\
	--display-map\
	--display-devel-map\
	--report-bindings\
	./wrapper.sh\
	${application} -parallel\
	&> ${log}
fi
