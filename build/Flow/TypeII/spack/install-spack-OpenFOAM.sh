#!/bin/sh

# Versions
gcc_versions=gcc@10.2.0
adios_versions=adios2@2.5.0
openmpi_versions="openmpi@3.1.6"
openmpi_options="fabrics=auto"
openfoam_versions_list="openfoam@2006"
openfoam_options="+kahip +metis +mgridgen +paraview +vtk"

# Spack root directory
export SPACK_ROOT=$HOME/spack

# Install spack
WORKDIR=/data/group1/$(id -un)
cd $WORKDIR
[ -d spack ] || git clone https://github.com/spack/spack.git
[ -L $SPACK_ROOT ] || ln -s $WORKDIR/spack $SPACK_ROOT
[ -d $WORKDIR/.spack ] || mkdir $WORKDIR/.spack
[ -L $HOME/.spack ] || ln -s $WORKDIR/.spack $HOME/.spack
source $SPACK_ROOT/share/spack/setup-env.sh
export LC_ALL=ja_JP.utf8
cd $SPACK_ROOT

# Install gcc
spack compiler list | grep $gcc_versions >& /dev/null || \
spack install $gcc_versions
spack load $gcc_versions

# Update compiler list
spack compiler find
spack compiler list

file=$HOME/.spack/linux/packages.yaml
if [ ! -f $file ]
then
    (
    cat <<EOF
packages:
  intel-mpi:
    paths:
EOF
    for dir in /home/center/opt/x86_64/cores/intel/compilers_and_libraries_*.*
    do
	echo "      intel-mpi@${dir##*_}: $dir/linux/mpi/intel64"
    done
    cat <<EOF
  openmpi:
    paths:
EOF
    for dir in /home/center/opt/x86_64/apps/gcc/4.8.5/openmpi/*
    do
	echo "      openmpi@${dir##*/}: $dir"
    done
    cat <<EOF
  all:
    compiler: [ $gcc_versions ]
EOF
    ) > $file
fi

# Install OpenFOAM
for openfoam_versions in $openfoam_versions_list
do
    args="$openfoam_versions $openfoam_options %$gcc_versions ^$openmpi_versions $openmpi_options"
    case "$openfoam_versions" in
	openfoam-org*)
	    spack install $args
	    spack load $args
	    ;;
	*)
	    spack install $args ^$adios_versions
	    spack load $args ^$adios_versions
	    ;;
    esac

    # Unload spack openmpi
    spack unload $openmpi_versions
    
    # build libraries for openmpi
    module purge
    module load gcc
    source $WM_PROJECT_DIR/etc/bashrc WM_MPLIB=SYSTEMOPENMPI
    (cd $FOAM_SRC/Pstream;./Allwmake-mpi)
    (cd $FOAM_SRC/parallel/decompose;./Allwmake-mpi)

    # Add intel-mpi library path for latest versions
    file=$WM_PROJECT_DIR/wmake/rules/linux64Gcc/mplibINTELMPI
    if [ ! -f $file.org ]
    then
	mv $file $file.org
	cat > $file <<'EOF'
PFLAGS     = -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX
PINC       = -isystem $(MPI_ARCH_PATH)/intel64/include
PLIBS      = -L$(MPI_ARCH_PATH)/intel64/lib/release -L$(MPI_ARCH_PATH)/intel64/lib -lmpi
EOF
    fi

    # build libraries for intel-mpi
    module purge
    impi_modules=`module avail 2>&1 -l | awk '/^intel\// {print $1}'`
    for module in $impi_modules 
    do
	module purge
	module load $module
	source $WM_PROJECT_DIR/etc/bashrc WM_MPLIB=INTELMPI
	(cd $FOAM_SRC/Pstream;./Allwmake-mpi)
	(cd $FOAM_SRC/parallel/decompose;./Allwmake-mpi)
    done
done
