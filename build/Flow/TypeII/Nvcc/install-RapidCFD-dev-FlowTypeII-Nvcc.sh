#!/bin/sh

# Versions of CUDA, GCC and CUDA-aware OpenMPI
cuda_version=11.8.0
gcc_version=8.4.0
openmpi_version=4.0.5

# Set variables of precision
WM_PRECISION_OPTION=DP # DP | SP

# Set variables of compile option name
WM_COMPILE_OPTION=Opt

# Set variables of OS architecture and compiler
WM_ARCH=linux64
WM_COMPILER=Nvcc${cuda_version}Gcc${gcc_version}

# Work directory
work=/data/group1/$(id -un)

# Make OpenFOAM direcrory
[ ! -d $work/RapidCFD ] && \
mkdir $work/RapidCFD

# Clone RapidCFD repositry
(cd $work/RapidCFD

[ ! -d RapidCFD-dev ] && \
git clone https://github.com/Atizar/RapidCFD-dev.git
)

# Apply patch
[ ! -f $work/RapidCFD/RapidCFD-dev/PATCHED ] && \
patch -d $work/RapidCFD/RapidCFD-dev -b -p1 < patches/RapidCFD-dev.diff &> $work/RapidCFD/RapidCFD-dev/PATCHED

# Move to the top directory of RapidCFD-dev
cd $work/RapidCFD/RapidCFD-dev

# Adjust compiler options
cp -a wmake/rules/${WM_ARCH}Nvcc wmake/rules/${WM_ARCH}${WM_COMPILER}
sed -i 's/ -arch=sm_[0-9]*//' wmake/rules/${WM_ARCH}${WM_COMPILER}/c
sed -i 's/ -arch=sm_[0-9]*//' wmake/rules/${WM_ARCH}${WM_COMPILER}/c++
sed -i 's/^cOPT .*$/cOPT = -O3 -gencode=arch=compute_70,code=sm_70/' wmake/rules/${WM_ARCH}${WM_COMPILER}/cOpt
sed -i 's/^c++OPT .*$/c++OPT = -O3 -gencode=arch=compute_70,code=sm_70/' wmake/rules/${WM_ARCH}${WM_COMPILER}/c++Opt

# Purge all modules
module purge

# Load CUDA module
module load cuda/${cuda_version}

# Load CUDA-aware OpenMPI
module load openmpi_cuda/${openmpi_version}

# Load gcc compiler
module load gcc/${gcc_version}

# Performe environment settings of RapidCFD
source $work/RapidCFD/RapidCFD-dev/etc/bashrc \
WM_COMPILER=${WM_COMPILER} \
WM_PRECISION_OPTION=${WM_PRECISION_OPTION} \
WM_COMPILE_OPTION=${WM_COMPILE_OPTION}

# Use single processor for build
export WM_NCOMPPROCS=1

# Build RapidCFD
LANG=C nohup ./Allwmake &> log.Allwmake.$$ &
