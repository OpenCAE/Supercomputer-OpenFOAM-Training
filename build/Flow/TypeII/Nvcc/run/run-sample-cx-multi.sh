#!/bin/sh
#PJM -L rscunit=cx
#PJM -L rscgrp=cx-debug
#PJM -L node=1
#PJM -L gpu=4
#PJM -L elapse=0:10:00
#PJM --mpi proc=4
#PJM -S
#PJM -j

# Versions of CUDA, GCC and CUDA-aware OpenMPI
cuda_version=11.8.0
gcc_version=8.4.0
openmpi_version=4.0.5

# Set variables of precision
WM_PRECISION_OPTION=DP # DP | SP

# Set variables of compile option name
WM_COMPILE_OPTION=Opt

# Set variables of OS architecture and compiler
WM_ARCH=linux64
WM_COMPILER=Nvcc${cuda_version}Gcc${gcc_version}

# Work directory
work=/data/group1/$(id -un)

# Purge all modules
module purge

# Load CUDA module
module load cuda/${cuda_version}

# Load CUDA-aware OpenMPI
module load openmpi_cuda/${openmpi_version}

# Perform environmental settings of RapidCFD
source $work/RapidCFD/RapidCFD-dev/etc/bashrc \
WM_COMPILER=${WM_COMPILER} \
WM_PRECISION_OPTION=${WM_PRECISION_OPTION} \
WM_COMPILE_OPTION=${WM_COMPILE_OPTION}

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Log filename
log=log.$application.$jobid

# Run solver
mpiexec \
    -display-map -display-devel-map \
    -n $PJM_MPI_PROC -machinefile $PJM_O_NODEINF -npernode 4 \
    $application -devices '( 0 1 2 3 )' -parallel >& $log
