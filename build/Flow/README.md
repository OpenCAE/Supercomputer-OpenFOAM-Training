[ホーム](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/wikis/home)

[[_TOC_]]

# 名古屋大学情報基盤センターのスーパーコンピュータ「不老」におけるOpenFOAMおよびRapidCFDのビルド

## 「不老」の特徴

### Type Iの特徴

- ログインノードと計算ノードのアーキテクチャが異なる．
- 計算ノードのアーキテクチャ用にOpenFOAM等をビルドするには，ログインノードにおいてクロスコンパイラを用いてビルドするか，計算ノードにおけるバッチジョブまたはインタラクティブジョブでビルドする必要がある．

### Type IIの特徴

- 計算ノードは，Intel社のCPUを2ソケット，NVIDIA社のGPUを4ソケット搭載したハイブリット構成である．
- 計算ノードのGPUを用いずにCPUだけを用いる事も可能だが，CPUだけを使用するのは推奨されていない．
- また，CPUのみを使う場合でも，GPUの演算負荷ポイントが消費されるので，同じIntel社のCPUを有するクラウドに比べて演算負荷ポイントが高くなる．

### その他の特徴

- 「不老」のログインノードでは，大量のメモリを使用する処理や多数の計算コアを使う処理など，負荷の大きな処理は禁止されている．
- 計算ノードは通常インターネットに接続できない．
- Homeファイルシステムよりも，Dataファイルシステムのほうが高速である．

## レポジトリの取得

```bash
cd
mkdir opt
cd opt
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
mkdir local
```

## Fccを用いたType Iノード実行用のOpenFOAMのビルド

以下では，Fujitsu compilerのFccを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Flow/TypeI/Fcc
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeI/Fcc/)

### 自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-FlowTypeI-Fcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeI/Fcc/install-OpenFOAM-FlowTypeI-Fcc.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-FlowTypeI-Fcc.sh installation_prefix openfoam_version tcs_version < /dev/null &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-FlowTypeI-Fcc.sh ~/opt/local v2406 1.2.39 < /dev/null &> log.v2406 &
```

PETSc，HYPREのライブラリもビルドする．

ビルドには2，3時間を要するが，ログファイルの末尾が[以下のような出力](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/FX1000/Fcc/README.md)で終了しており，特にエラーが出ていなければビルド成功である．

ログに以下のようなエラーが出ている場合には，通常ビルドされたbinやlibの数が少なくなる．

```
clang-7: error: unable to execute command: Hangup
clang-7: error: clang frontend command failed due to signal (use -v to see invocation)
```

このような場合には，再度自動ビルドスクリプトを実行する．

### OpenFOAM環境設定用alias

[環境設定用aliasの例(alias-OpenFOAM-FlowTypeI-Fcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeI/Fcc/alias-OpenFOAM-FlowTypeI-Fcc.sh)

### OpenFOAM実行用ジョブファイル

[実行用ジョブファイルの例(run-OpenFOAM-FlowTypeI-Fcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeI/Fcc/run-OpenFOAM-FlowTypeI-Fcc.sh)

## Gccを用いたType IIノード実行用のOpenFOAMのビルド

以下では，Gccを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Flow/TypeII/Gcc
```
[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeII/Gcc/)

### OpenFOAMの自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-FlowTypeII-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeII/Gcc/install-OpenFOAM-FlowTypeII-Gcc.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-FlowTypeII-Gcc.sh installation_prefix openfoam_version gcc_version cuda_verion openmpi_cuda_version &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-FlowTypeII-Gcc.sh ~/opt/local v2406 11.3.0 12.1.1 4.0.5 &> log.v2406 &
```

OpenFOAMをビルドした後，AmgX，PETSc，FOAM2CSRのライブラリをビルドした上で，amgxwrapperブランチのpetscFoamライブラリをビルドする．
なお，amgxwrapperブランチのpetscFoamライブラリをインストールする関係で，標準のpetscFoamライブラリをビルドしないように，modules/external-solverでのAllwmakeは，Allwmake.disabledに変更している．

### OpenFOAM環境設定用alias

[環境設定用aliasの例(alias-OpenFOAM-FlowTypeII-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeII/Gcc/alias-OpenFOAM-FlowTypeII-Gcc.sh)

### OpenFOAM実行用ジョブファイル

[実行用ジョブファイルの例(run-OpenFOAM-FlowTypeII-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeII/Gcc/run-OpenFOAM-FlowTypeII-Gcc.sh)

## Type IIにおけるRapidCFDのビルド

以下では，CUDA 11.2.1を用いてRapidCFD-devを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```shell
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Flow/TypeII/Nvcc
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeII/Nvcc/)

### 自動ビルドスクリプトの実行

以下のようにして自動ビルドスクリプトを実行する．

```shell
./install-RapidCFD-dev-FlowTypeII-Nvcc.sh
```

[install-RapidCFD-dev-FlowTypeII-Nvcc.shの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeII/Nvcc/install-RapidCFD-dev-FlowTypeII-Nvcc.sh)

異なるバージョンのCUDAコンパイラやgccコンパイラ，MPIライブラリのmoduleを用いてRapidCFD-devをビルドする場合には，上記のスクリプトの以下を変更する．

```plaintext
# Versions of CUDA, GCC and CUDA-aware OpenMPI
cuda_version=11.8.0
gcc_version=8.4.0
openmpi_version=4.0.5
```

また，浮動小数点の精度の設定`WM_PRECISION_OPTION`を単精度(SP)に変える場合には，以下を変更する．

```plaintext
# Set variables of precision
WM_PRECISION_OPTION=DP # DP | SP
```

### RapidCFD実行用のジョブファイル

OpenFOAM実行用のジョブファイルの例は，`run`ディレクトリにある．

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeII/Nccc/run/)

RapidCFD実行用の実行ジョブファイルの例を以下に示す．

#### 単一のGPUを使用する場合

[ジョブスクリプトrun-sample-cx-single.shの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeII/Nvcc/run/run-sample-cx-single.sh)

* 1ノードかつ1ソケットで単一のGPUのみを用いる場合，リソースグループはcx-shareとする．
* RapidCFDのアプリケーションは複数のMPIプロセスで動作させないので，アプリケーション実行にmpiexecなどを用いる必要はない．

#### マルチGPUを使用する場合

[ジョブスクリプトrun-sample-cx-multi.shの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Flow/TypeII/Nvcc/run/run-sample-cx-multi.sh)

* マルチGPUを使用する場合には，OpenMPIのmpiexecを用いてRapidCFDのアプリケーションを起動する．
* 通常OpenFOAMのアプリケーションと同様，オプションに`-parallel`を渡すとともに，使用するGPUのデバイス番号のリストを指定する．

## 参考資料

- 今野 雅（株式会社OCAEL・東京大学情報基盤センター客員研究員）: 名古屋大学スーパーコンピュータ不老のOpenFOAMベンチマークテスト, [第1回 スーパーコンピュータ「不老」ユーザ会](https://icts.nagoya-u.ac.jp/ja/sc/event/20200831_user/20200831_user.html), 2020年8月31日, [PDF](https://icts.nagoya-u.ac.jp/ja/sc/event/20200831_user/imano.pdf)

[ホーム](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/wikis/home)
