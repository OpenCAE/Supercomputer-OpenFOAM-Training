#!/bin/sh
(
    numactl -s
    echo
    env
) > ${0##*/}.${PJM_JOBID}.${OMPI_COMM_WORLD_RANK}.out
numactl -l $*
