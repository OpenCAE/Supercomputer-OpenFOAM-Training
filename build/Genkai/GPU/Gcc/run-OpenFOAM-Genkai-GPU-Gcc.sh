#!/bin/bash
#PJM -L rg=b-batch
#PJM -L node=1
#PJM --mpi proc=120
#PJM -L elapse=0:10:00
#PJM -S

# Use Multi-Process Service
MPS=false # true | false

# Number of processes per node
ppn=120

# Number of GPUs per node
gpus_per_node=4

# OpenFOAM settings
prefix=${HOME}/opt/local;openfoam_version=v2406;gcc_version=12;cuda_version=12.2.2;ompi_cuda_version=4.1.6-12.2.2;module purge;module load gcc-toolset/${gcc_version} cuda/${cuda_version} ompi-cuda/${ompi_cuda_version};machine=$(uname -m);dir=${prefix}/${machine}/apps/gcc/${gcc_version}/cuda/${cuda_version}/ompi-cuda/${ompi_cuda_version};openfoam_dir=${dir}/openfoam/${openfoam_version};source ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/bashrc;export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${dir}/amgx/main/lib;eval $(foamEtcFile -sh -config petsc -- -force);export OMP_NUM_THREADS=1;export FOAM_SIGFPE=false

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Log filename
log=log.${application}.${jobid}

# Run solver
if [ ${PJM_MPI_PROC} -eq 1 ]
then
    command="numactl -l ${application} -lib petscFoam"
else
    if ${MPS}
    then
	case ${gpus_per_node} in
	    1)
		export CUDA_VISIBLE_DEVICES=0;;
	    2)
		export CUDA_VISIBLE_DEVICES=0,1;;
	    3)
		export CUDA_VISIBLE_DEVICES=0,1,2;;
	    4)
		export CUDA_VISIBLE_DEVICES=0,1,2,3;;
	esac
	export CUDA_MPS_PIPE_DIRECTORY=/ssd/${jobid}/pipe
	export CUDA_MPS_LOG_DIRECTORY=/ssd/${jobid}/mps.${jobid}
	nvidia-cuda-mps-control -d
	unset CUDA_VISIBLE_DEVICES
	command="mpiexec -np ${PJM_MPI_PROC} --machinefile ${PJM_O_NODEINF} --display-map --display-devel-map --report-bindings ./wrapper.sh ${application} -lib petscFoam -parallel"
    else
	command="mpiexec -np ${PJM_MPI_PROC} --machinefile ${PJM_O_NODEINF} --display-map --display-devel-map --report-bindings ./wrapperGPU.sh ${ppn} ${gpus_per_node} ${application} -lib petscFoam -parallel"
    fi
fi
echo -e "command\n---\n${command}"
${command} &> ${log}

# Copy MPS log files
${MPS} && cp -r ${CUDA_MPS_LOG_DIRECTORY} ./
