#!/bin/bash
#PJM -L rg=a-batch
#PJM -L node=1
#PJM --mpi proc=120
#PJM -L elapse=0:10:00
#PJM -S

# Number of processes per node
ppn=120

# OpenFOAM settings
prefix=${HOME}/opt/local;openfoam_version=v2406;gcc_version=12;ompi_version=4.1.6;module purge;module load gcc-toolset/${gcc_version} ompi/${ompi_version};machine=$(uname -m);dir=${prefix}/${machine}/apps/gcc/${gcc_version}/ompi/${ompi_version};openfoam_dir=${dir}/openfoam/${openfoam_version};source ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/bashrc;eval $(foamEtcFile -sh -config petsc -- -force)

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Log filename
log=log.${application}.${jobid}

# Run solver
if [ ${PJM_MPI_PROC} -eq 1 ]
then
    command="numactl -l ${application} -lib petscFoam"
else
    command="mpiexec -np ${PJM_MPI_PROC} --machinefile ${PJM_O_NODEINF} --display-map --display-devel-map --report-bindings ./wrapper.sh ${application} -lib petscFoam -parallel"
fi
echo -e "command\n---\n${command}"
${command} &> ${log}
