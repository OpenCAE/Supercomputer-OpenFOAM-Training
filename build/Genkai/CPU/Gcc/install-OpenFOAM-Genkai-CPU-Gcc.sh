#!/bin/bash

# Installation prefix and versions
if [ "$#" -ne 4 ]
then
    echo "usage: $0 installation_prefix openfoam_version gcc_version ompi_version"
    echo
    echo "usage(eg): $0 ~/opt/local v2406 12 4.1.6"
    exit 1
fi
prefix=$1
openfoam_version=$2
gcc_version=$3
ompi_version=$4

# [WM_COMPILER] - Compiler:
# = Gcc | Clang | Icc | Icx | Amd | Arm | Cray | Fujitsu | Nvidia |
WM_COMPILER=Gcc

# [WM_PRECISION_OPTION] - Floating-point precision:
# = DP | SP | SPDP
WM_PRECISION_OPTION=DP # DP | SP | SPDP

# [WM_LABEL_SIZE] - Label size in bits:
# = 32 | 64
export WM_LABEL_SIZE=32

# [WM_COMPILE_OPTION] - Optimised(default), debug, profiling, other:
# = Opt | Dbg | Debug | Prof
WM_COMPILE_OPTION=Opt

# [WM_MPLIB] - MPI implementation:
# = SYSTEMOPENMPI | OPENMPI | SYSTEMMPI | MPI | MPICH | MPICH-GM |
WM_MPLIB=SYSTEMOPENMPI

# Set number of processes
WM_NCOMPPROCS=4

# Load dependant modules need for building OpenFOAM
module purge
module load gcc-toolset/${gcc_version} ompi/${ompi_version}
module load fftw

# Make installation and source directory
machine=$(uname -m)
dir=${prefix}/${machine}/apps/gcc/${gcc_version}/ompi/${ompi_version}
openfoam_dir=${dir}/openfoam/${openfoam_version}
src_dir=${PWD}/src
export PETSC_ARCH=arch-linux-c-opt
export PETSC_ARCH_PATH=${dir}/petsc/${PETSC_ARCH}
export MPI_ARCH_PATH=${OMPI_HOME}
[ ! -d ${openfoam_dir} ] && mkdir -p ${openfoam_dir}
[ ! -d ${src_dir} ] && mkdir ${src_dir}

##
## PETSc
##
if [ ! -f ${PETSC_ARCH_PATH}/lib/libpetsc.so ]
then
    [ ! -d ${src_dir}/petsc ] && (cd ${src_dir};git clone -b release https://gitlab.com/petsc/petsc.git)
    (
	cd ${src_dir}/petsc
	./configure\
	    --prefix=${PETSC_ARCH_PATH}\
	    --PETSC_ARCH=${PETSC_ARCH}\
	    --with-64-bit-indices=0\
	    --with-precision=double\
	    --with-default-arch=0\
	    --with-clanguage=C\
	    --with-fc=0\
	    --with-x=0\
	    --download-f2cblaslapack\
	    --download-hypre\
	    --with-debugging=0\
	    --COPTFLAGS="-O3"\
	    --CXXOPTFLAGS="-O3"\
	    --with-openmpi=1\
	    --with-openmpi-dir=${MPI_ARCH_PATH}\
	    --force  
	make -j ${WM_NCOMPPROCS} all
	make install
    )
fi

##
## OpenFOAM
##
# Fetch OpenFOAM source files
(cd ${openfoam_dir}
 if [ ! -d OpenFOAM-${openfoam_version} ];then
     wget --no-check-certificate -o /dev/null -O - https://sourceforge.net/projects/openfoam/files/${openfoam_version}/OpenFOAM-${openfoam_version}.tgz | tar zx
     (cd OpenFOAM-${openfoam_version}
      git init
      git add .
     )
 fi
 if   [ ! -d ThirdParty-${openfoam_version} ];then
     wget --no-check-certificate -o /dev/null -O - https://sourceforge.net/projects/openfoam/files/${openfoam_version}/ThirdParty-${openfoam_version}.tgz | tar zx
     (cd ThirdParty-${openfoam_version}
      git init
      git add .
     )
 fi
)

# Adjust hardcoded installation settings
cd ${openfoam_dir}/OpenFOAM-${openfoam_version}
bin/tools/foamConfigurePaths -system-compiler ${WM_COMPILER}
bin/tools/foamConfigurePaths -mpi ${WM_MPLIB}
bin/tools/foamConfigurePaths -metis metis-none
bin/tools/foamConfigurePaths -fftw fftw-system
sed -i -e s"|^\(ParaView_VERSION=\).*$|\1none|" etc/config.sh/paraview
[ -f etc/config.sh/petsc ] && \
sed -i -e s"|^\(petsc_version=\).*$|\1petsc-system|" -e s"|^\(export PETSC_ARCH_PATH=\).*$|\1${PETSC_ARCH_PATH}|" etc/config.sh/petsc

# OpenFOAM environmental settings
source etc/bashrc WM_COMPILER=${WM_COMPILER} WM_MPLIB=${WM_MPLIB} WM_PRECISION_OPTION=${WM_PRECISION_OPTION}

# Build OpenFOAM
./Allwmake -k -j ${WM_NCOMPPROCS}
