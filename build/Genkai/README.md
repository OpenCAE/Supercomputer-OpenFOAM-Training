[ホーム](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/wikis/home)

[[_TOC_]]

# 九州大学情報基盤研究開発センターのスーパーコンピュータ玄界におけるOpenFOAM

## ビルド方針

- 各グループ毎の共有ディレクトリ`/home/$(id -gn)/share`下の`opt`ディレクトリ以下にインストールする．

## レポジトリの取得

```shell
mkdir -p /home/$(id -gn)/share/opt
cd
ln -s /home/$(id -gn)/share/opt ./
cd opt
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
mkdir local
```

## Gccを用いたCPU(ノードグループA)実行用のOpenFOAMのビルド

以下では，Gccを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Genkai/CPU/Gcc
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/CPU/Gcc/)

### OpenFOAMの自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-Genkai-CPU-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/CPU/Gcc/install-OpenFOAM-Genkai-CPU-Gcc.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-Genkai-CPU-Gcc.sh installation_prefix openfoam_version gcc_version ompi_version &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-Genkai-CPU-Gcc.sh ~/opt/local v2406 12 4.1.6 &> log.v2406 &
```

PETScライブラリをビルドした上で，OpenFOAMをビルドする．

### OpenFOAM環境設定用alias

- [環境設定aliasの例(alias-OpenFOAM-Genkai-CPU-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/CPU/Gcc/alias-OpenFOAM-Genkai-CPU-Gcc.sh)

### OpenFOAM実行用ジョブファイル

- [実行用ジョブファイルの例(run-OpenFOAM-Genkai-CPU-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/CPU/Gcc/run-OpenFOAM-Genkai-CPU-Gcc.sh)

## Gccを用いたGPU(ノードグループB,C)実行用のOpenFOAMのビルド

以下では，Gccを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Genkai/GPU/Gcc
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/GPU/Gcc/)

### OpenFOAMの自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-Genkai-GPU-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/Gcc/GPU/install-OpenFOAM-Genkai-GPU-Gcc.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-Genkai-GPU-Gcc.sh installation_prefix openfoam_version gcc_version cuda_verion ompi-cuda_version &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-Genkai-GPU-Gcc.sh ~/opt/local v2406 12 12.2.2 4.1.6-12.2.2 &> log.v2406 &
```

OpenFOAMをビルドした後，AmgX，PETSc，FOAM2CSRのライブラリをビルドした上で，amgxwrapperブランチのpetscFoamライブラリをビルドする．
なお，amgxwrapperブランチのpetscFoamライブラリをインストールする関係で，標準のpetscFoamライブラリをビルドしないように，modules/external-solverでのAllwmakeは，Allwmake.disabledに変更している．

### OpenFOAM環境設定用alias

- [環境設定aliasの例(alias-OpenFOAM-Genkai-GPU-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/GPU/Gcc/alias-OpenFOAM-Genkai-GPU-Gcc.sh)

### OpenFOAM実行用ジョブファイル

- [実行用ジョブファイルの例(run-OpenFOAM-Genkai-GPU-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/GPU/Gcc/run-OpenFOAM-Genkai-GPU-Gcc.sh)

## NVIDIA HPC SDKを用いたGPU(ノードグループB,C)実行用のOpenFOAMのビルド

以下では，NVIDIA HPC SDKを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Genkai/GPU/Nvidia
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/GPU/Nvidia/)

### OpenFOAMの自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-Genkai-GPU-Nvidia.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/GPU/Nvidia/install-OpenFOAM-Genkai-GPU-Nvidia.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-Genkai-GPU-Nvidia.sh installation_prefix openfoam_version nvidia_hpc_sdk_version cuda_version ompi_cuda_version &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-Genkai-GPU-Nvidia.sh ~/opt/local v2406 23.9 12.2.2 4.1.6-12.2.2 &> log.v2406 &
```

OpenFOAMをビルドした後，AmgX，PETSc，FOAM2CSRのライブラリをビルドした上で，amgxwrapperブランチのpetscFoamライブラリをビルドする．
なお，amgxwrapperブランチのpetscFoamライブラリをインストールする関係で，標準のpetscFoamライブラリをビルドしないように，modules/external-solverでのAllwmakeは，Allwmake.disabledに変更している．

### OpenFOAM環境設定用alias

[環境設定aliasの例(alias-OpenFOAM-Genkai-GPU-Nvidia.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/GPU/Nvidia/alias-OpenFOAM-Genkai-GPU-Nvidia.sh)

### OpenFOAM実行用ジョブファイル

- [実行用ジョブファイルの例(run-OpenFOAM-Genkai-GPU-Nvidia.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/GPU/Nvidia/run-OpenFOAM-Genkai-GPU-Nvidia.sh)

## ラッパーファイル

- [ラッパーファイルの例(wrapperGPU.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/wrapperGPU.sh)
- [GPU用ラッパーファイルの例(wrapperGPU.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Genkai/wrapperGPU.sh)

[ホーム](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/wikis/home)
