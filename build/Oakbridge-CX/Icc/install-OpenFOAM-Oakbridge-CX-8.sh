#!/bin/bash

# OpenFOAM version
openfoam_version=8

# Version of Intel compiler
intel_version=2020.4.304

# Version of Intel MPI
impi_version=2019.9.304

# Set variable of precision
WM_PRECISION_OPTION=DP # DP | SP | SPDP

# Set variable of compile option name
WM_COMPILE_OPTION=Opt  # Opt | Debug | Prof

# Set variable of compiler
WM_COMPILER=Icc

# Set variable of MPI library
WM_MPLIB=INTELMPI

# Project name
project=$(id -gn)

# Work directory
work=/work/$project/$(id -un)

# Make OpenFOAM direcrory
[ ! -d $work/OpenFOAM ] && \
mkdir $work/OpenFOAM

# Fetch OpenFOAM source files
(cd $work/OpenFOAM
[ ! -d OpenFOAM-${openfoam_version} ] && \
wget --no-check-certificate -o /dev/null -O - http://dl.openfoam.org/source/${openfoam_version} | tar zx && mv OpenFOAM-${openfoam_version}-version-${openfoam_version} OpenFOAM-${openfoam_version}

[ ! -d ThirdParty-${openfoam_version} ] && \
wget --no-check-certificate -o /dev/null -O - http://dl.openfoam.org/third-party/${openfoam_version} | tar zx && mv ThirdParty-${openfoam_version}-version-${openfoam_version} ThirdParty-${openfoam_version}
)

# Fetch ThirdParty source files
(cd $work/OpenFOAM/ThirdParty-${openfoam_version}
[ ! -d metis-5.1.0 ] && \
wget --no-check-certificate -o /dev/null -O - http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz | tar zx
)

# Create job file of OpenFOAM build
job=v${openfoam_version}-${WM_COMPILER}${WM_PRECISION_OPTION}${WM_COMPILE_OPTION}${WM_MPLIB}.pjm
if [ "$project" = "gt00" ]
then
    rscgrp=lecture
    elapse=00:15:00
    nstepjobs=4
else
    rscgrp=debug
    elapse=00:30:00
    nstepjobs=2
fi    

cat > $job <<EOF
#!/bin/bash
#PJM -g $project
#PJM -L rscgrp=$rscgrp
#PJM -L elapse=$elapse
#PJM -L node=1
#PJM --mpi proc=56
#PJM -j
#PJM -S
# Purge modules
module purge
# Load dependant modules need for building OpenFOAM
module load intel/${intel_version}
module load impi/${impi_version}
module load cmake/3.14.5
# Build with 56 cores
export WM_NCOMPPROCS=56
# OpenFOAM environmental settings
export MPI_ROOT=/work/opt/local/cores/intel/impi/${impi_version}/intel64
export HOME=$work
source $work/OpenFOAM/OpenFOAM-${openfoam_version}/etc/bashrc \
WM_COMPILER=${WM_COMPILER} \
WM_PRECISION_OPTION=${WM_PRECISION_OPTION} \
WM_COMPILE_OPTION=${WM_COMPILE_OPTION} \
WM_MPLIB=${WM_MPLIB}
# Move to OpenFOAM project directory
cd \$WM_PROJECT_DIR
# Build third party libraries and OpenFOAM libraries and applications
./Allwmake -k
EOF

# Submit jobs
i=0
stepOptions=""
while [ $i -lt $nstepjobs ]
do
  command="pjsub -z jid --step $stepOptions $job"
  echo $command
  jid=$($command)
  jid=${jid%_*}
  echo $ijd
  stepOptions="--sparam jid=$jid"
  i=$(expr $i + 1)
done
