#!/bin/bash
#PJM -g gz00
#PJM -L rscgrp=debug
#PJM -L elapse="0:30:00"
#PJM -L node=2
#PJM --mpi proc=112
#PJM -j
#PJM -S

# OpenFOAM version
openfoam_version=v2206

# Version of Gcc
gcc_version=7.5.0 # 4.8.5 | 7.5.0

# Version of Intel MPI
impi_version=2021.7.1

# Version of CMake
cmake_version=3.18.4 # 3.14.5

# Set variable of precision
WM_PRECISION_OPTION=DP # DP | SP | SPDP

# Set variable of compile option name
WM_COMPILE_OPTION=Opt

# Set variable of compiler
WM_COMPILER=Gcc${gcc_version}

# Set variable of MPI library
WM_MPLIB=INTELMPI${impi_version}

# Project name
project=$(id -gn)

# Work directory
work=/work/$project/$(id -un)

# Purge module
module purge

# OpenFOAM and MPI settings
module load gcc/${gcc_version}
module load impi/${impi_version}
module load cmake/${cmake_version}

# OpenFOAM environmental settings
. $work/OpenFOAM/OpenFOAM-${openfoam_version}/etc/bashrc \
WM_COMPILER=${WM_COMPILER} \
WM_PRECISION_OPTION=${WM_PRECISION_OPTION} \
WM_COMPILE_OPTION=${WM_COMPILE_OPTION} \
WM_MPLIB=${WM_MPLIB}

# Tutorial run functions
. $WM_PROJECT_DIR/bin/tools/RunFunctions

# Application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# MPI run command
mpirun=mpiexec.hydra

# Run solver
$mpirun -np $PJM_MPI_PROC $application -parallel &> log.$application.$jobid
