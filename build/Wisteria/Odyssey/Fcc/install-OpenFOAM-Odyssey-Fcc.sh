#!/bin/bash

# Installation prefix and versions
if [ "$#" -ne 4 ]
then
    echo "usage: $0 installation_prefix openfoam_version fj_version fjmpi_version"
    echo
    echo "usage(eg): $0 ~/opt/local v2406 1.2.39 1.2.39"
    exit 1
fi
prefix=$1
openfoam_version=$2
fj_version=$3
fjmpi_version=$4

# Set variable of precision
WM_PRECISION_OPTION=DP # DP | SP | SPDP

# Set variable of compile option name
WM_COMPILE_OPTION=Opt

# Set variable of compiler
WM_COMPILER=FClang

# Set variable of MPI implementation
WM_MPLIB=FJMPI

# Make installation and source directory
dir=${prefix}/aarch64/apps/fj/${fj_version}/fjmpi/${fjmpi_version}
openfoam_dir=${dir}/openfoam/${openfoam_version}
src_dir=${PWD}/src
PETSC_ARCH=arch-linux-c-opt
PETSC_ARCH_PATH=${dir}/petsc/${PETSC_ARCH}
[ ! -d n${openfoam_dir} ] && mkdir -p ${openfoam_dir}
[ ! -d ${src_dir} ] && mkdir ${src_dir}

# Build PETSc, HYPRE
if [ ! -f ${PETSC_ARCH_PATH}/lib/libpetsc.so ]
then
   [ ! -d ${src_dir}/petsc ] && (cd ${src_dir};git clone -b release https://gitlab.com/petsc/petsc.git)
    group=$(id -gn)
    if [ $group = 'gt00' ]
    then
	rg=lecture-o
    else
	rg=debug-o
    fi
    while [ ! -f ${PETSC_ARCH_PATH}/lib/libpetsc.so ]
    do
	jid=$(pjsub -g ${group} -L rg=${rg},node=1 --mpi proc=48 -s -z jid -x "PETSC_ARCH=${PETSC_ARCH},PETSC_ARCH_PATH=${PETSC_ARCH_PATH}" install-petsc-Odyssey-Fcc.sh)
	pjwait -z ${jid}
    done
fi
    
# Fetch OpenFOAM source files
(cd ${openfoam_dir}
 if [ ! -d OpenFOAM-${openfoam_version} ];then
     wget --no-check-certificate -o /dev/null -O - https://sourceforge.net/projects/openfoam/files/${openfoam_version}/OpenFOAM-${openfoam_version}.tgz | tar zx
     (cd OpenFOAM-${openfoam_version}
      git init
      git add .
     )
 fi
 if   [ ! -d ThirdParty-${openfoam_version} ];then
     wget --no-check-certificate -o /dev/null -O - https://sourceforge.net/projects/openfoam/files/${openfoam_version}/ThirdParty-${openfoam_version}.tgz | tar zx
     (cd ThirdParty-${openfoam_version}
      git init
      git add .
     )
 fi
)

# Apply patches
patch=OpenFOAM-${openfoam_version}.diff
if [ ! -f ${openfoam_dir}/OpenFOAM-${openfoam_version}/${patch} ]
then
    patch -N -b -p1 -d ${openfoam_dir}/OpenFOAM-${openfoam_version} < ../../../FX1000/Fcc/patches/${patch}
    (
	cd ${openfoam_dir}/OpenFOAM-${openfoam_version} 
	git add -N wmake/rules/A64FXFClang/{c,c++,c++Debug,c++Opt,c++Prof,cDebug,cOpt,cProf,general}
	[ -f etc/config.sh/readline ] && git add -N etc/config.sh/readline
	git diff > ${patch}
    )
fi
patch=ThirdParty-${openfoam_version}.diff
if [ ! -f ${openfoam_dir}/ThirdParty-${openfoam_version}/${patch} ]
then
    patch -N -b -p1 -d ${openfoam_dir}/ThirdParty-${openfoam_version} < ../../../FX1000/Fcc/patches/${patch}
    (
	cd ${openfoam_dir}/ThirdParty-${openfoam_version} 
	git diff > ${patch}
    )
fi

# Load dependant modules need for building OpenFOAM
module --no-pager purge
module --no-pager load fj/${fj_version} fjmpi/${fjmpi_version}
module --no-pager load cmake fftw scotch
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$SCOTCH_LIB

# Adjust hardcoded installation versions
cd ${openfoam_dir}/OpenFOAM-${openfoam_version}
bin/tools/foamConfigurePaths -fftw fftw-system -fftw-path ${FFTW_DIR}
bin/tools/foamConfigurePaths -scotch scotch-system -scotch-path ${SCOTCH_DIR}
[ -f etc/config.sh/petsc ] && \
sed -i -e s"|^\(petsc_version=\).*$|\1petsc-system|" -e s"|^\(export PETSC_ARCH_PATH=\).*$|\1${PETSC_ARCH_PATH}|" etc/config.sh/petsc

# OpenFOAM environmental settings
source etc/bashrc WM_PRECISION_OPTION=${WM_PRECISION_OPTION}

# Build OpenFOAM
./Allwmake -k -j 4
