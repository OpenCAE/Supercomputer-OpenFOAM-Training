#!/bin/bash
env
cd src/petsc
./configure\
    --prefix=${PETSC_ARCH_PATH}\
    --PETSC_ARCH=${PETSC_ARCH}\
    --with-64-bit-indices=0\
    --with-precision=double\
    --with-default-arch=0\
    --with-clanguage=C\
    --with-fc=0\
    --with-x=0\
    --download-f2cblaslapack\
    --download-hypre\
    --with-debugging=0\
    --COPTFLAGS="-O3"\
    --CXXOPTFLAGS="-O3"\
    --with-cc=mpifcc\
    --with-cxx=mpiFCC
make -j ${PJM_PROC_BY_NODE:-4} all
make install
