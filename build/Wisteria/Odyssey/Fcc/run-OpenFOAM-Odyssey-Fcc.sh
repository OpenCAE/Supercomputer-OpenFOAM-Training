#!/bin/bash
#PJM -g gt00
#PJM -L rscgrp=lecture-o
#PJM -L elapse=0:15:00
#PJM -j
#PJM -S
#PJM -L node=1
#PJM --mpi proc=48

# Installation prefix and versions
prefix=/work/gt00/share/opt/local
openfoam_version=v2406
fj_version=1.2.39
fjmpi_version=1.2.39

# Set variable of precision
WM_PRECISION_OPTION=DP # DP | SP | SPDP

# Set variable of compile option name
WM_COMPILE_OPTION=Opt

# Installation directory
dir=${prefix}/aarch64/apps/fj/${fj_version}/fjmpi/${fjmpi_version}/openfoam/${openfoam_version}

# Load dependant modules
module load cmake fftw petsc scotch

# Perform environmental settings of OpenFOAM
source ${dir}/OpenFOAM-${openfoam_version}/etc/bashrc \
WM_PRECISION_OPTION=${WM_PRECISION_OPTION} \
WM_COMPILE_OPTION=${WM_COMPILE_OPTION}
eval $(foamEtcFile -sh -config petsc -- -force)

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Log filename
log=log.${application}.${jobid}

# Run solver
mpiexec -of ${log} ${application} -parallel
