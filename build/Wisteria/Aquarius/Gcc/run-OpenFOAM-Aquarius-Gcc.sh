#!/bin/bash
#PJM -L rg=lecture-a
#PJM -L gpu=4
#PJM --mpi proc=4
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S

# Installation prefix and versions
prefix=/work/gt00/share/opt/local
openfoam_version=v2406
gcc_version=12.2.0
cuda_version=12.0
ompi_cuda_version=4.1.6-12.0

# Instal directory
dir=${prefix}/x86_64/apps/gcc/${gcc_version}/cuda/${cuda_version}/ompi-cuda/${ompi_cuda_version}

# Load dependant modules
module --no-pager purge
module --no-pager load gcc/${gcc_version} cuda/${cuda_version} ompi-cuda/${ompi_cuda_version}

# Perform environmental settings of OpenFOAM, AmgX, petsc
source ${dir}/openfoam/${openfoam_version}/OpenFOAM-${openfoam_version}/etc/bashrc
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${dir}/amgx/main/lib
eval $(foamEtcFile -sh -config petsc -- -force)

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Log filename
log=log.${application}.${jobid}

# Run solver
export OMP_NUM_THREADS=1
if [ ${PJM_MPI_PROC} -eq 1 ]
then
    ${application} &> ${log}
else
    mpiexec\
	-np ${PJM_MPI_PROC}\
	--machinefile ${PJM_O_NODEINF}\
	--display-map\
	--display-devel-map\
	--report-bindings\
	./wrapper.sh\
	${application} -parallel\
	&> ${log}
fi
