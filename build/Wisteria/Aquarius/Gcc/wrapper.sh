#!/bin/sh
#ノード内におけるMPIプロセスのローカルランク
export LOCAL_ID=$OMPI_COMM_WORLD_LOCAL_RANK
#プロセスが使うInfiniBandのポート番号を指定
#case $LOCAL_ID in
#    0|4) export UCX_NET_DEVICES=mlx5_0:1 ;;
#    1|5) export UCX_NET_DEVICES=mlx5_1:1 ;;
#    2|6) export UCX_NET_DEVICES=mlx5_2:1 ;;
#    3|7) export UCX_NET_DEVICES=mlx5_3:1 ;;
#esac
#ローカルランクのGPUのみCUDAから見えるようにする
export CUDA_VISIBLE_DEVICES=$LOCAL_ID
#メモリーが常にローカルノードから割り当てられるように指定
numactl -l $*
