#!/bin/bash
#PJM -g ProjectName(e.g. gt00)
#PJM -L rscgrp=ResourceGroup(e.g. regular-a/short-a/debug-a/interactive-a/share/share-debug/share-short/lecture-a)
#PJM -L elapse=ElapseTime(e.g. 0:10:00)
#PJM -S
#PJM -j
#PJM -L node=NumberOfNodes(e.g. 2)
#PJM --mpi proc=NumberOfProcesses(e.g. 16)
#PJM -L gpu=NumberOfGPUs(e.g. 4)

# Installation prefix and versions
prefix=/work/gt00/share/opt/local
gcc_version=8.3.1
cuda_version=12.0
ompi_cuda_version=4.1.5-12.0

# Make install direcrory
dir=${prefix}/x86_64/apps/gcc/${gcc_version}/cuda/${cuda_version}/ompi-cuda/${ompi_cuda_version}
rapidcfd_dir=${dir}/rapidcfd/dev

# Set variables of precision
WM_PRECISION_OPTION=DP # DP | SP

# Set variables of compile option name
WM_COMPILE_OPTION=Opt

# Set variables of OS architecture and compiler
WM_ARCH=linux64

# Load dependant modules
module load gcc/${gcc_version}
module load cuda/${cuda_version}
module load ompi-cuda/${ompi_cuda_version}

# Perform environmental settings of RapidCFD
source ${rapidcfd_dir}/RapidCFD-dev/etc/bashrc \
WM_PRECISION_OPTION=${WM_PRECISION_OPTION} \
WM_COMPILE_OPTION=${WM_COMPILE_OPTION}

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PJM_SUBJOBID:-$PJM_JOBID}

# Log filename
log=log.${application}.${jobid}

# Run solver
if [ ${PJM_MPI_PROC} -eq 1 ]
then
    ${application} &> ${log}
else
    mpiexec\
	-np ${PJM_MPI_PROC}\
	-machinefile ${PJM_O_NODEINF}\
	--display-map\
	--display-devel-map\
	--report-bindings\
	${application} -devices '( 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 )'\
	-parallel >& ${log}
fi
