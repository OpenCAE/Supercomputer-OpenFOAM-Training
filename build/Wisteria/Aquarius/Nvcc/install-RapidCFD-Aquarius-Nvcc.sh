#!/bin/sh

# Installation prefix and versions
if [ "$#" -ne 4 ]
then
    echo "usage: $0 installation_prefix gcc_version cuda_verion ompi-cuda_version"
    echo
    echo "usage(eg): $0 ~/opt/local 8.3.1 12.0 4.1.5-12.0"
    exit 1
fi
prefix=$1
gcc_version=$2
cuda_version=$3
ompi_cuda_version=$4

# Make install direcrory
dir=$prefix/x86_64/apps/gcc/${gcc_version}/cuda/${cuda_version}/ompi-cuda/${ompi_cuda_version}
rapidcfd_dir=${dir}/rapidcfd/dev
[ ! -d ${rapidcfd_dir} ] && mkdir -p ${rapidcfd_dir}

# Set variables of precision
WM_PRECISION_OPTION=DP # DP | SP

# Set variables of compile option name
WM_COMPILE_OPTION=Opt

# Clone RapidCFD repositry
(cd ${rapidcfd_dir}
 [ ! -d RapidCFD-dev ] && git clone https://github.com/Atizar/RapidCFD-dev.git
)

# Apply patch
[ ! -f ${rapidcfd_dir}/RapidCFD-dev/PATCHED ] && \
patch -d ${rapidcfd_dir}/RapidCFD-dev -b -p1 < patches/RapidCFD-dev.diff &> ${rapidcfd_dir}/RapidCFD-dev/PATCHED

# Move to the top directory of RapidCFD-dev
cd ${rapidcfd_dir}/RapidCFD-dev

# Adjust settings
sed -i "s|^\(foamInstall=\).*|\1${rapidcfd_dir}|" etc/bashrc
sed -i 's/ -arch=sm_[0-9]*//' wmake/rules/linux64Nvcc/c
sed -i 's/ -arch=sm_[0-9]*//' wmake/rules/linux64Nvcc/c++
sed -i 's/^cOPT .*$/cOPT = -O3 -gencode=arch=compute_80,code=sm_80/' wmake/rules/linux64Nvcc/c${WM_COMPILE_OPTION}
sed -i 's/^c++OPT .*$/c++OPT = -O3 -gencode=arch=compute_80,code=sm_80/' wmake/rules/linux64Nvcc/c++${WM_COMPILE_OPTION}

# Load dependant modules need for building OpenFOAM
module --no-pager purge
module --no-pager load gcc/${gcc_version} cuda/${cuda_version} ompi-cuda/${ompi_cuda_version}
module --no-pager load cmake/3.24.0

# RapidCFD environmental settings
source etc/bashrc WM_PRECISION_OPTION=${WM_PRECISION_OPTION} WM_COMPILE_OPTION=${WM_COMPILE_OPTION}

# Use single processor for build
export WM_NCOMPPROCS=4

# Build RapidCFD
LANG=C ./Allwmake
