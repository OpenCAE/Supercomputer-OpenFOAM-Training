[ホーム](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/wikis/home)

[[_TOC_]]

# 東京大学情報基盤センターのスーパーコンピュータWisteria/BDEC-01におけるOpenFOAMおよびRapidCFDのビルド

## Wisteria/BDEC-01の特徴

### Odysseyの特徴

- ログインノード・プリポストノードと計算ノードのアーキテクチャが異なる．
- 計算ノードのアーキテクチャ用にOpenFOAM等をビルドするには，ログインノード・プリポストノードにおいてクロスコンパイラを用いてビルドするか，計算ノードにおけるバッチジョブまたはインタラクティブジョブでビルドする必要がある．

### Aquariusの特徴

- 計算ノードは，Intel社のCPUを2ソケット，NVIDIA社のGPU A100を8ソケット搭載したハイブリット構成である．
- 計算ノードのGPUを用いずにCPUだけを用いる事も可能だが，CPUだけを使用するのは推奨されていない．

### Mercuryの特徴

- 計算ノードは，Intel社のCPUを2ソケット，NVIDIA社のGPU H100を4ソケット搭載したハイブリット構成である．
- 計算ノードのGPUを用いずにCPUだけを用いる事も可能だが，CPUだけを使用するのは推奨されていない．

### その他の特徴

- ログインノードでは，大量のメモリを使用する処理や多数の計算コアを使う処理など，負荷の大きな処理は禁止されており，使用メモリ量が合計で24GBを越えた場合，使用メモリ量の大きいプロセスから順に強
制終了される
- ログインノード共有ファイルシステム(`/home/ユーザ名`)は，計算ノードからアクセス不可．また，割り当てサイズはユーザあたり50GBと小さい．
- 共有ファイルシステム(`/work/グループ名/ユーザ名`)は，計算ノードからアクセス可能だあり，また，割り当てサイズは申し込み1セットあたり2TBである．

## ビルド方針

- 各ユーザ毎の共有ファイルシステム`/work/$(id -gn)/$(id -un)`以下にインストールする．
- OpenFOAMのビルドには，様々な依存ライブラリが必要だが，対応するmoduleがある場合には，ビルド時間の短縮のために新規にビルドせず，それらを使うように設定する．

## レポジトリの取得

```shell
mkdir -p /work/$(id -gn)/$(id -un)/opt
cd
ln -s /work/$(id -gn)/$(id -un)/opt ./
cd opt
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
mkdir local
```

## Fccを用いたOdysseyノード実行用のOpenFOAMのビルド

以下では、Fujitsu compilerのFccを用いてOpenFOAMを自動でビルドする方法を示す。

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Wisteria/Odyssey/Fcc
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Odyssey/Fcc/)

### 自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-Odyssey-Fcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Odyssey/Fcc/install-OpenFOAM-Odyssey-Fcc.sh)

以下のようにして自動ビルドスクリプトを実行する。

```bash
nohup time ./install-OpenFOAM-Odyssey-Fcc.sh installation_prefix openfoam_version(eg, v2406) fj_version(eg, 1.2.39) fjmpi_version(eg, 1.2.39) < /dev/null &> log.VER &
# 実行例
# nohup time ./install-OpenFOAM-Odyssey-Fcc.sh ~/opt/local v2406 1.2.39 1.2.39 < /dev/null &> log.v2406 &
```

PETSc，HYPREのライブラリもビルドする．

ビルドには2，3時間を要するが，ログファイルの末尾が[以下のような出力](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/FX1000/Fcc/README.md)で終了しており、特にエラーが出ていなければビルド成功である．

### OpenFOAM環境設定用エイリアス

[OpenFOAM環境設定用エイリアスの例(alias-OpenFOAM-Odyssey-Fcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Odyssey/Fcc/alias-OpenFOAM-Odyssey-Fcc.sh)

### OpenFOAM実行用ジョブファイル

[実行用ジョブファイルの例(run-OpenFOAM-Odyssey-Fcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Odyssey/Fcc/run-OpenFOAM-Odyssey-Fcc.sh)

## Gccを用いたAquariusノード実行用のOpenFOAMのビルド

以下では，Gccを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Wisteria/Aquarius/Gcc
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Aquarius/Gcc/)

### OpenFOAMの自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-Aquarius-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Aquarius/Gcc/install-OpenFOAM-Aquarius-Gcc.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-Aquarius-Gcc.sh installation_prefix openfoam_version gcc_version cuda_verion ompi-cuda_version &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-Aquarius-Gcc.sh ~/opt/local v2406 12.2.0 12.0 4.1.6-12.0 &> log.v2406 &
```

OpenFOAMをビルドした後，AmgX，PETSc，FOAM2CSRのライブラリをビルドした上で，amgxwrapperブランチのpetscFoamライブラリをビルドする．
なお，amgxwrapperブランチのpetscFoamライブラリをインストールする関係で，標準のpetscFoamライブラリをビルドしないように，modules/external-solverでのAllwmakeは，Allwmake.disabledに変更している．

### OpenFOAM環境設定用alias

[環境設定用aliasの例(alias-OpenFOAM-Aquarius-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Aquarius/Gcc/alias-OpenFOAM-Aquarius-Gcc.sh)

### OpenFOAM実行用ジョブファイル

[実行用ジョブファイルの例(run-OpenFOAM-Aquarius-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Aquarius/Gcc/run-OpenFOAM-Aquarius-Gcc.sh)

## Gccを用いたMercuryノード実行用のOpenFOAMのビルド

以下では，Gccを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Wisteria/Mercury/Gcc
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Mercury/Gcc/)

### OpenFOAMの自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-Mercury-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Mercury/Gcc/install-OpenFOAM-Mercury-Gcc.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-Mercury-Gcc.sh installation_prefix openfoam_version nvidia_hpc_sdk_version cuda_version ompi_cuda_version &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-Mercury-Gcc.sh ~/opt/local v2406 11.3.1 12.3 4.1.6-12.3 &> log.v2406 &
```

OpenFOAMをビルドした後，AmgX，PETSc，FOAM2CSRのライブラリをビルドした上で，amgxwrapperブランチのpetscFoamライブラリをビルドする．
なお，amgxwrapperブランチのpetscFoamライブラリをインストールする関係で，標準のpetscFoamライブラリをビルドしないように，modules/external-solverでのAllwmakeは，Allwmake.disabledに変更している．

### OpenFOAM環境設定用alias

[環境設定用aliasの例(alias-OpenFOAM-Mercury-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Mercury/Gcc/alias-OpenFOAM-Mercury-Gcc.sh)

### OpenFOAM実行用ジョブファイル

[実行用ジョブファイルの例(run-OpenFOAM-Mercury-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Mercury/Gcc/run-OpenFOAM-Mercury-Gcc.sh)

## NVIDIA HPC SDKを用いたMercuryノード実行用のOpenFOAMのビルド

以下では，NVIDIA HPC SDKを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Wisteria/Mercury/Nvidia
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Mercury/Nvidia/)

### OpenFOAMの自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-Mercury-Nvidia.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Mercury/Nvidia/install-OpenFOAM-Mercury-Nvidia.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-Mercury-Nvidia.sh installation_prefix openfoam_version nvidia_hpc_sdk_version cuda_version ompi_cuda_version &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-Mercury-Nvidia.sh ~/opt/local v2406 24.3 12.3 4.1.6-12.3 &> log.v2406 &
```

OpenFOAMをビルドした後，AmgX，PETSc，FOAM2CSRのライブラリをビルドした上で，amgxwrapperブランチのpetscFoamライブラリをビルドする．
なお，amgxwrapperブランチのpetscFoamライブラリをインストールする関係で，標準のpetscFoamライブラリをビルドしないように，modules/external-solverでのAllwmakeは，Allwmake.disabledに変更している．

### OpenFOAM環境設定用alias

[環境設定用aliasの例(alias-OpenFOAM-Mercury-Nvidia.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Mercury/Nvidia/alias-OpenFOAM-Mercury-Nvidia.sh)

### OpenFOAM実行用ジョブファイル

[実行用ジョブファイルの例(run-OpenFOAM-Mercury-Nvidia.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Mercury/Nvidia/run-OpenFOAM-Mercury-Nvidia.sh)

## Aquariusノード実行用のRapidCFDのビルド

以下ではRapidCFDをビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```shell
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Wisteria/Aquarius/Nvcc
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Aquarius/Nvcc/)

### 自動ビルドスクリプトの実行

自動ビルドスクリプトを以下のように実行する．

```shell
nohup time ./install-RapidCFD-Aquarius-Nvcc.sh installation_prefix gcc_version(eg, 8.3.1) cuda_verion(eg, 12.0) ompi-cuda_version(eg, 4.1.5-12.0) &> LOG_FILENAME &
# 実行例
# nohup time ./install-RapidCFD-Aquarius-Nvcc.sh ~/opt/local 8.3.1 12.0 4.1.5-12.0 &> log &
```

[install-RapidCFD-Aquarius-Nvcc.shの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Aquarius/Nvcc/install-RapidCFD-Aquarius-Nvcc.sh)

浮動小数点の精度の設定`WM_PRECISION_OPTION`を単精度(SP)に変える場合には，以下を変更する．

```plaintext
# Set variables of precision
WM_PRECISION_OPTION=DP # DP | SP
```

### RapidCFD実行用のジョブファイル

[実行用ジョブファイルの例(run-RapidCFD-Aquarius-Nvcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Wisteria/Aquarius/Nvcc/run-RapidCFD-Aquarius-Nvcc.sh)

* オプション`-npernode`やオプション`-devices`の引数は適宜変更する．
* なお， mpiexecのオプション`-display-map -display-devel-map`は，MPIプロセスの割当て確認用なので必須ではない．
* 通常OpenFOAMのアプリケーションと同様，オプションに`-parallel`を渡すとともに，使用するGPUのデバイス番号のリストを指定する．
* RapidCFD特有のオプションなどは[RapidCFDに関する情報](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/wikis/RapidCFD)を参照．

[ホーム](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/wikis/home)
