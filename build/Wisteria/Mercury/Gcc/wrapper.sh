#!/bin/sh
#ノード内におけるMPIプロセスのローカルランク
export LOCAL_ID=$OMPI_COMM_WORLD_LOCAL_RANK
#ローカルランクのGPUのみCUDAから見えるようにする
export CUDA_VISIBLE_DEVICES=$LOCAL_ID
#メモリーが常にローカルノードから割り当てられるように指定
numactl -l $*
