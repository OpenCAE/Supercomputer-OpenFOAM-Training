#!/bin/bash
#SBATCH -p batch
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4

# Installation prefix and versions
prefix=${HOME}/opt/local
openfoam_version=v2406
nvidia_version=24.3
cuda_version=12.3
ompi_cuda_version=4.1.6-12.3

# Instal directory
machine=$(uname -m)
dir=${prefix}/${machine}/apps/nvidia/${nvidia_version}/cuda/${cuda_version}/ompi-cuda/${ompi_cuda_version}
openfoam_dir=${dir}/openfoam/${openfoam_version}

# Load dependant modules
module --no-pager purge
module --no-pager load nvidia/${nvidia_version} cuda/${cuda_version} ompi-cuda/${ompi_cuda_version}

# Perform environmental settings of OpenFOAM, AmgX, petsc
source ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/bashrc
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${dir}/amgx/main/lib
eval $(foamEtcFile -sh -config petsc -- -force)

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${SLURM_JOBID}

# Log filename
log=log.${application}.${jobid}

# Run solver
export OMP_NUM_THREADS=1
export FOAM_SIGFPE=false
if [ ${SLURM_NTASKS} -eq 1 ]
then
    ${application} &> ${log}
else
    mpiexec\
	-np ${SLURM_NTASKS}\
	--report-bindings\
	--display-map\
	--display-devel-map\
	./wrapper.sh\
	${application} -parallel\
	&> ${log}
fi
