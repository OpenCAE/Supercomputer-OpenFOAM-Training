#!/bin/sh

# Set variables of precision
WM_PRECISION_OPTION=DP # DP | SP

# Set variables of compile option name
WM_COMPILE_OPTION=Opt

# Set variables of OS architecture and compiler
WM_ARCH=linux64
WM_COMPILER=Nvcc

# Work directory
work=/root

# Make OpenFOAM direcrory
[ ! -d $work/RapidCFD ] && \
mkdir $work/RapidCFD

# Clone RapidCFD repositry
(cd $work/RapidCFD

[ ! -d RapidCFD-dev ] && \
git clone https://github.com/Atizar/RapidCFD-dev.git
)

# Apply patch
[ ! -f $work/RapidCFD/RapidCFD-dev/PATCHED ] && \
patch -d $work/RapidCFD/RapidCFD-dev -b -p1 < patches/RapidCFD-dev.diff &> $work/RapidCFD/RapidCFD-dev/PATCHED

# Move to the top directory of RapidCFD-dev
cd $work/RapidCFD/RapidCFD-dev
# Adjust compiler options
sed -i 's/ -arch=sm_[0-9]*/ -arch=sm_75/' wmake/rules/${WM_ARCH}${WM_COMPILER}/c
sed -i 's/ -arch=sm_[0-9]*/ -arch=sm_75/' wmake/rules/${WM_ARCH}${WM_COMPILER}/c++

# Perform environment settings of RapidCFD
source $work/RapidCFD/RapidCFD-dev/etc/bashrc \
WM_COMPILER=${WM_COMPILER} \
WM_PRECISION_OPTION=${WM_PRECISION_OPTION} \
WM_COMPILE_OPTION=${WM_COMPILE_OPTION}

# Use single processor for build
export WM_NCOMPPROCS=2

# Build RapidCFD
LANG=C nohup ./Allwmake &> log.Allwmake.$$

