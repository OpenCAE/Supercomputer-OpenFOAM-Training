#!/bin/bash
#PBS -W group_list=gt01
#PBS -q debug-c
#PBS -l walltime=00:30:00
#PBS -l select=1:mpiprocs=112
#PBS -j oe

# Perform environmental settings of OpenFOAM
prefix=${HOME}/opt/local;openfoam_version=v2406;intel_version=2023.2.0;impi_version=2021.10.0;WM_COMPILER=Icx-${intel_version};WM_MPLIB=INTELMPI-${impi_version};module --no-pager purge;module --no-pager load intel/${intel_version} impi/${impi_version};machine=$(uname -m);dir=${prefix}/${machine}/apps/intel/${intel_version}/impi/${impi_version};openfoam_dir=${dir}/openfoam/${openfoam_version};source ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/bashrc WM_COMPILER=${WM_COMPILER} WM_MPLIB=${WM_MPLIB};eval $(foamEtcFile -sh -config petsc -- -force)
export FOAM_SIGFPE=false
export OMP_NUM_THREADS=1

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PBS_JOBID}
jobid=${jobid%.opbs}

# Log filename
log=log.${application}.${jobid}

# Run solver
MPI_PROC=`wc -l ${PBS_NODEFILE} | awk '{print $1}'`
NODE_NUM=`sort -u ${PBS_NODEFILE} | wc -l`
MPI_PROC_PER_NODE=`expr ${MPI_PROC} / ${NODE_NUM}`
export I_MPI_DEBUG=6

cd ${PBS_O_WORKDIR}
if [ ${MPI_PROC} -eq 1 ]
then
    command="numactl -l ${application} -lib petscFoam"
else
    command="mpiexec.hydra -n ${MPI_PROC} -ppn ${MPI_PROC_PER_NODE} numactl -l ${application} -lib petscFoam -parallel"
fi
echo -e "Execute command\n---\n${command}"
${command} &> ${log}
