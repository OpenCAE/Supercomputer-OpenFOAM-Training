[ホーム](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/wikis/home)

[[_TOC_]]

# JCAHPCのスーパーコンピュータMiyabiにおけるOpenFOAMのビルド

## ビルド方針

- 各ユーザ毎の共有ファイルシステム`/work/$(id -gn)/$(id -un)`以下にインストールする．

## レポジトリの取得

```shell
mkdir -p /work/$(id -gn)/$(id -un)/opt
cd
ln -s /work/$(id -gn)/$(id -un)/opt ./
cd opt
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
mkdir local
```

## NVIDIA HPC SDKを用いたMiyabi-G計算ノード向けのOpenFOAMのビルド

以下では，NVIDIA HPC SDKを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Miyabi/MiyabiG/Nvidia
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiG/Nvidia/)

### OpenFOAMの自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-MiyabiG-Nvidia.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiG/Nvidia/install-OpenFOAM-MiyabiG-Nvidia.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-MiyabiG-Nvidia.sh installation_prefix nvidia_hpc_sdk_version cuda_verion ompi-cuda_version &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-MiyabiG-Nvidia.sh ~/opt/local v2406 24.9 12.6 4.1.6-12.6 &> log.v2406 &
```

OpenFOAMをビルドした後，AmgX，PETSc，FOAM2CSRのライブラリをビルドした上で，amgxwrapperブランチのpetscFoamライブラリをビルドする．
なお，amgxwrapperブランチのpetscFoamライブラリをインストールする関係で，標準のpetscFoamライブラリをビルドしないように，modules/external-solverでのAllwmakeは，Allwmake.disabledに変更している．

### OpenFOAM環境設定用alias

[環境設定用aliasの例(alias-OpenFOAM-MiyabiG-Nvidia.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiG/Nvidia/alias-OpenFOAM-MiyabiG-Nvidia.sh)

### OpenFOAM実行用ジョブファイル

[実行用ジョブファイルの例(run-OpenFOAM-MiyabiG-Nvidia.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiG/Nvidia/run-OpenFOAM-MiyabiG-Nvidia.sh)

[ラッパスクリプトの例(wrapper.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiG/Nvidia/wrapper.sh)

## Gccを用いたMiyabi-G計算ノード向けのOpenFOAMのビルド

以下では，Gccを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Miyabi/MiyabiG/Gcc
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiG/Gcc/)

### OpenFOAMの自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-MiyabiG-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiG/Gcc/install-OpenFOAM-MiyabiG-Gcc.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-MiyabiG-Gcc.sh installation_prefix openfoam_version gcc_version cuda_verion ompi-cuda_version &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-MiyabiG-Gcc.sh ~/opt/local v2406 12.4.0 12.6 4.1.6-12.6 &> log.v2406 &
```

### OpenFOAM環境設定用alias

[環境設定用aliasの例(alias-OpenFOAM-MiyabiG-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiG/Gcc/alias-OpenFOAM-MiyabiG-Gcc.sh)

### OpenFOAM実行用ジョブファイル

[実行用ジョブファイルの例(run-OpenFOAM-MiyabiG-Gcc.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiG/Gcc/run-OpenFOAM-MiyabiG-Gcc.sh)

[ラッパスクリプトの例(wrapper.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiG/Gcc/wrapper.sh)

## Intel oneAPIを用いたMiyabi-C計算ノード向けのOpenFOAMのビルド

以下では，Intel oneAPIを用いてOpenFOAMを自動でビルドする方法を示す．

### 自動ビルドスクリプトがあるディレクトリに移動

```bash
cd ~/opt/Supercomputer-OpenFOAM-Training/build/Miyabi/MiyabiC/Icx
```

[ディレクトリの内容](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiC/Icx/)

### OpenFOAMの自動ビルドスクリプトの実行

[自動ビルドスクリプト(install-OpenFOAM-MiyabiC-Icx.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiC/Icx/install-OpenFOAM-MiyabiC-Icx.sh)

以下のようにして自動ビルドスクリプトを実行する．

```bash
nohup time ./install-OpenFOAM-MiyabiC-Icx.sh installation_prefix openfoam_version intel_version impi_version &> LOG_FILENAME &
# 実行例
# nohup time ./install-OpenFOAM-MiyabiC-Icx.sh ~/opt/local v2406 2023.2.0 2021.10.0 &> log.v2406 &
```

### OpenFOAM環境設定用alias

[環境設定用aliasの例(alias-OpenFOAM-MiyabiC-Icx.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiC/Icx/alias-OpenFOAM-MiyabiC-Icx.sh)

### OpenFOAM実行用ジョブファイル

[実行用ジョブファイルの例(run-OpenFOAM-MiyabiC-Icx.sh)](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/blob/master/build/Miyabi/MiyabiC/Icx/run-OpenFOAM-MiyabiC-Icx.sh)

[ホーム](https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training/-/wikis/home)
