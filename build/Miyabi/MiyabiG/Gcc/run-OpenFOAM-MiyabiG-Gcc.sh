#!/bin/bash
#PBS -W group_list=gt01
#PBS -q debug-g
#PBS -l walltime=00:30:00
#PBS -l select=1:mpiprocs=72
#PBS -j oe

# Perform environmental settings of OpenFOAM
prefix=${HOME}/opt/local;openfoam_version=v2406;gcc_version=11.4.1;cuda_version=12.6;ompi_cuda_version=4.1.6-12.6;module --no-pager purge;module --no-pager load gcc/${gcc_version} cuda/${cuda_version} ompi-cuda/${ompi_cuda_version};machine=$(uname -m);dir=${prefix}/${machine}/apps/gcc/${gcc_version}/cuda/${cuda_version}/ompi-cuda/${ompi_cuda_version};openfoam_dir=${dir}/openfoam/${openfoam_version};source ${openfoam_dir}/OpenFOAM-${openfoam_version}/etc/bashrc;export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${dir}/amgx/main/lib;eval $(foamEtcFile -sh -config petsc -- -force)
export FOAM_SIGFPE=false
export OMP_NUM_THREADS=1

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jobid=${PBS_JOBID%.opbs}

# Log filename
log=log.${application}.${jobid}

# Run solver
MPI_PROC=`wc -l ${PBS_NODEFILE} | awk '{print $1}'`

cd ${PBS_O_WORKDIR}
if [ ${MPI_PROC} -eq 1 ]
then
    command="numactl -l ${application} -lib petscFoam"
else
    unset OMPI_MCA_mca_base_env_list
    report="-display-map -display-devel-map -report-bindings"
    command="mpirun ${report} -x PATH -x LD_LIBRARY_PATH -x WM_PROJECT_DIR numactl -l ${application} -lib petscFoam -parallel"
fi
echo -e "Execute command\n---\n${command}"
${command} &> ${log}
