#!/bin/bash
#PJM -L rg=lecture-o
#PJM -L node=1
#PJM --mpi proc=48
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
module load openfoam/v2112-rist #OpenFOAM-v2112のRIST版moduleをload
source ${WM_PROJECT_DIR}/etc/bashrc #OpenFOAMの環境設定
foamRunTutorials #foamRunTutorialsコマンドでチュートリアルを実行
