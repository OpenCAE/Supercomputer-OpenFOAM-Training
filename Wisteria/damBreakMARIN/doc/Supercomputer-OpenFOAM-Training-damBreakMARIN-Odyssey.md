``` Command
## 講習会作業用ディレクトリとホームディレクトリへのシンボリックリンク作成
# 講習会作業用ディレクトリ作成 (mkdir: make directories)
mkdir /work/$(id -gn)/share/lectures/$(id -un)
# ホームディレクトリへ移動 (cd: change the current directory to dir)
cd
# ディレクトリ名の確認 (pwd: print working directory)
pwd 
# ホームディレクトリ上のファイルやディレクトリの確認(ls: list directory contents)
ls -l
# ホームディレクトリに既にlecturesがあれば，mv lectures lectures.old 等で移動しておく
# シンボリックリンク作成 (ln: make links between files)
ln -s /work/$(id -gn)/share/lectures/$(id -un) lectures
# シンボリックリンクの確認
ls -l
```
``` Command
## 講習会レポジトリの複製
# 講習会作業用ディレクトリへ移動
cd lectures
# 講習会レポジトリの複製   
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
# レポジトリのディレクトリへ移動
cd Supercomputer-OpenFOAM-Training
# 本講習会のブランチへの切り替え
git checkout Information-Technology-Center-The-University-of-Tokyo-20240131
# ブランチ名の表示
git branch
```
``` Command
## 利用可能な全てのmoduleの表示
show_module
# 表示が長いので，パイプ機能(|)でページャーのlessに渡して検索できるようにする
show_module | less
# lessのコマンド例: h:ヘルプ, スペース:次の画面, b:前の画面, /正規表現:正規表現検索, q:終了
```
``` Command
## lessのコマンドでopenfoamを検索
/openfoam
```
``` Command
## lessを終了
q
```
``` Command
## 現環境で利用可能なmoduleの表示
module avail -l
# 自動的にlessにパイプされているので，openfoamを検索する．
/openfoam
# BaseCompilerやMPIのmoduleをloadしていないので，OpenFOAMのmoduleは表示されない
```
``` Command
## BaseCompilerとMPIのmoduleのload
module load gcc/8.3.1 impi/2021.8.0
```
``` Command
## 現環境で利用可能なmoduleの表示
module avail -l
```
``` Command
## moduleのhelp表示
module help openfoam/v2106
```
``` Command
## OpenFOAM v2106のmoduleのload
module load openfoam/v2106
```
``` Command
## OpenFOAMの環境設定
source $WM_PROJECT_DIR/etc/bashrc
```
``` Command
## バージョンの表示
foamVersion
```
``` Command
## 環境設定手順をalias(別名)として登録するスクリプトを表示
cat ~/lectures/Supercomputer-OpenFOAM-Training/Odyssey/etc/.bashrc
```
``` Command
## OpenFOAMの環境設定のalias化
source ~/lectures/Supercomputer-OpenFOAM-Training/Odyssey/etc/.bashrc
```
``` Command
## aliasの確認
alias
```
``` Command
## Aquarius用OpenFOAM v2106の環境設定aliasの実行
OFv2106
```
``` Command
## bashシェル実行時にalias登録を自動的に行うよう~/.bashrcに設定追加
cat ~/lectures/Supercomputer-OpenFOAM-Training/Odyssey/etc/.bashrc >> ~/.bashrc
```
``` Command
## ~/.bashrcの内容確認
cat ~/.bashrc
```
``` Command
## 作成済の解析ケースのコピー
cd ~/lectures/Supercomputer-OpenFOAM-Training/Odyssey/damBreakMARIN/run/ # 実行ディレクトリ
cp -r master/ case-0 # デフォルトケースをコピー．ディレクトリなので-r(recursive)オプションが必要
cd case-0/ # ケースディレクトリに移動
```
``` Command
## 混雑状況の確認
pjstat --rsc -b # 投げられているジョブ数
pjstat --rscuse # リソースグループのノード使用率
```
``` Command
## ベース格子生成・領域分割のジョブ投入
pjsub run1base.sh
```
``` Command
## ジョブの状況確認
pjstat
```
``` Command
## blockMeshのログ確認
less log.blockMesh.*
# *の場所には本来JOB IDを打つ必要があるが，ワイルドカードの*でマッチする
# スペース等で進め，q で終了
```
``` Command
## decomposeParのログの確認(領域分割のジョブ終了後)
less log.decomposePar.*
# [less] Maxの検索
/Max
# [less]一行戻る
Ctrl-P # カーソル↑やvi的なコマンド(j，Ctrl-J, y, Ctrl-Y)でも良い(hによるヘルプ参照)
```
``` Command
## ParaViewによるOpenFOAMデータ可視化用ダミーファイル作成
touch pv.foam # 中身は何でも良いので，touchコマンドで空ファイルを作成する
```
``` Command
## OpenFOAMの環境設定aliasの実行(既に実行している場合には不要)
OFv2106 # OpenFOAM module内のParaViewを使うため
```
``` Command
## paraviewをバックグラウンドジョブで起動
paraview pv.foam & # pv.foamを自動的に読み込むよう引数に書く
```
``` Command
## 格子生成のジョブ投入
pjsub run2mesh.sh
tail -f log.* # ログのモニター(いつでもCtrl+Cで終了して良い)
```
``` Command
## snappyHexMeshのログ確認
less log.snappyHexMesh.*
```
``` Command
## renumberMeshのログ確認
less log.renumberMesh.*
```
``` Command
## checkMeshのログ確認
less log.checkMesh.*
```
``` Command
paraview pv.foam &
```
``` Command
## 初期値設定ジョブの投入
pjsub run3init.sh
```
``` Command
## OpenFOAMの設定ファイル例の表示と検索
tree $FOAM_ETC/caseDicts | less # /probes でprobesを検索する．適宜qで終了
```
``` Command
## 流体解析実行
pjsub run4sol.sh
```
``` Command
## OpenFOAMの環境設定aliasの実行(既に実行している場合には不要)
OFv2106
```
``` Command
## 圧力時系列のモニター
foamMonitor -r 1 postProcessing/probes/0/p & # -r :更新秒(デフォルト10)
```
``` Command
## gnuplotによるプロット
gnuplot pressure.gp
```
``` Command
## firefoxによるプロット結果閲覧(firefoxで閲覧すると図をタブで切り替えられて便利)
firefox pressure*.pdf &
```
``` Command
paraview pv.foam &
```
``` Command
## ParaViewの起動
paraview postProcessing/isoSurface/2/isoSurface.vtp &
```
``` Command
## 全時刻に対する可視化画像の作成
./Allrun.animImage
```
``` Command
## 動画の作成
./Allrun.anim
```
``` Command
## 動画の再生
firefox anim/*.mp4 &
```
``` Command
## 領域分割された格子と計算結果の再構築ジョブの投入
pjsub run5re.sh
```
``` Command
## interIsoFoamを用いた解析ケースの作成
cd ~/lectures/Supercomputer-OpenFOAM-Training/Odyssey/damBreakMARIN/run/ # 実行ディレクトリ
cp -r master/ case-Iso # デフォルトケースをコピー
cd case-0 # case-0ディレクリに移動
cp --parents -r processor*/{0,constant} ../case-Iso/ # 格子と初期値を複製(--parents: ディレクトリ名付き)
cd ../case-Iso # ケースディレクトリに移動
```
``` Command
## 実行アプリケーション名の変更(エディタはvi, emacs, nano等何でも良い)
vi system/controlDict
```
``` Command
## 液相率のVOF解法設定の変更
vi system/fvSolution
```
``` Command
## 流体解析実行
pjsub run4sol.sh
```
``` Command
## 圧力時系列のモニター
foamMonitor -r 1 postProcessing/probes/0/p &
```
``` Command
## 基本ケースとの圧力時系列の比較プロット
gnuplot comparison-pressure.gp
```
``` Command
## firefoxによるプロット結果閲覧
firefox comparison-pressure*.pdf &
```
``` Command
## 解適合格子ケースの作成
cd ~/lectures/Supercomputer-OpenFOAM-Training/Odyssey/damBreakMARIN/run/ # 実行ディレクトリ
cp -r master/ case-AMR
cd case-0 # case-0ディレクリに移動
cp --parents -r processor*/{0,constant} ../case-AMR/ # 格子と初期値を複製(--parents: ディレクトリ名付き)
cd ../case-AMR # ケースディレクトリに移動
```
``` Command
## 動的格子設定の変更
vi constant/dynamicMeshDict
```
``` Command
vi system/controlDict
```
``` Command
## ステップジョブ実行
../../../bin/Allrun.batch run4sol.sh run4sol.sh run4sol.sh run4sol.sh run4sol.sh run4sol.sh
pjstat -E # -E: ステップジョブ内のサブジョブを展開して表示
```
``` Command
## 圧力時系列のモニター
foamMonitor -r 1 postProcessing/probes/0/p &
# ステップジョブ内のサブジョブが終了すると，次のジョブが開始される．
# 次のジョブの開始時刻のディレクトリに圧力時系列のファイルが置かれるので，
# 以下のように開始時刻のディレクトリを指定する．
foamMonitor -r 1 postProcessing/probes/0.7/p &
```
``` Command
## 基本ケースとの圧力時系列の比較プロット
gnuplot comparison-pressure.gp
```
``` Command
## firefoxによるプロット結果閲覧
firefox comparison-pressure*.pdf &
```
``` Command
## 基本ケースのコピー・設定変更
cd ~/lectures/Supercomputer-OpenFOAM-Training/Odyssey/damBreakMARIN/run/
cp -r master/ case-XX #XXは任意
cd case-XX/
#エディタで設定変更
```
``` Command
## ステップジョブの投入・サブジョブの確認
../../../bin/Allrun.batch
pjstat -E # -E: ステップジョブのサブジョブを展開して表示.
# run4sol.sh実行後次に進む
```
``` Command
## 比較プロット
tail -f log.inter* #エラーが出ていない事を確認後，Ctrl+C
foamMonitor -r 1 postProcessing/probes/0/p &
gnuplot comparison-pressure.gp
firefox comparison-pressure*.pdf &
```
``` Command
## interFoamのチュートリアルで使用されている速度Uの移流項スキームを調べる
# grep -r: ディレクトリ検索
grep -r "div(rhoPhi,U)" $FOAM_TUTORIALS/multiphase | less
# 出力略
```
``` Command
## interFoamのチュートリアルで使用されている速度Uの移流項スキームをユニークに表示
# grep -h: ファイル名を非表示
# tr -s "[:blank:]": 空白文字の繰り返しを1文字に置換
# sort | uniq: ソートして重複排除
grep -rh "div(rhoPhi,U)" $FOAM_TUTORIALS/multiphase | tr -s "[:blank:]" | sort | uniq
```
