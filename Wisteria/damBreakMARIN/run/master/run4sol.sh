#!/bin/bash
#PJM -L rg=lecture-o
#PJM -L node=1
#PJM --mpi proc=48
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runShare.sh # 共通設定
application=$(getApplication) # アプリケーション名を取得
mpiexec -of log.$application.$jid $application -parallel # 並列流体解析
