# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [927, 489]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [1.61000001430511, 0.0, 0.499999996873555]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-0.271758686641025, -2.7724400175163, 2.76899733692044]
renderView1.CameraFocalPoint = [2.65338709222251, 1.79815250973452, -1.31912008615726]
renderView1.CameraViewUp = [0.22027131639954403, 0.5681965861895442, 0.7928639143094861]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 1.75843681978879

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(927, 489)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.CaseType = 'Decomposed Case'
pvfoam.MeshRegions = ['internalMesh']
pvfoam.CellArrays = ['U', 'alpha.water', 'alpha.water_0', 'p', 'p_rgh']

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(registrationName='AnnotateTimeFilter1', Input=pvfoam)
annotateTimeFilter1.Format = 'Time: %.2fs'

# create a new 'Contour'
contour1 = Contour(registrationName='Contour1', Input=pvfoam)
contour1.ContourBy = ['POINTS', 'alpha.water']
contour1.Isosurfaces = [0.5]
contour1.PointMergeMethod = 'Uniform Binning'

# create a new 'Glyph'
glyph1 = Glyph(registrationName='Glyph1', Input=contour1,
    GlyphType='Arrow')
glyph1.OrientationArray = ['POINTS', 'U']
glyph1.ScaleArray = ['POINTS', 'U']
glyph1.ScaleFactor = 0.05
glyph1.GlyphTransform = 'Transform2'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'p'
pLUT = GetColorTransferFunction('p')
pLUT.RGBPoints = [-10.698009490966797, 0.231373, 0.298039, 0.752941, 2074.9175968170166, 0.865003, 0.865003, 0.865003, 4160.533203125, 0.705882, 0.0156863, 0.14902]
pLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'p'
pPWF = GetOpacityTransferFunction('p')
pPWF.Points = [-10.698009490966797, 0.0, 0.5, 0.0, 4160.533203125, 1.0, 0.5, 0.0]
pPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Outline'
pvfoamDisplay.ColorArrayName = ['POINTS', '']
pvfoamDisplay.LookupTable = pLUT
pvfoamDisplay.SelectTCoordArray = 'None'
pvfoamDisplay.SelectNormalArray = 'None'
pvfoamDisplay.SelectTangentArray = 'None'
pvfoamDisplay.OSPRayScaleArray = 'p'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'U'
pvfoamDisplay.ScaleFactor = 0.322000002861023
pvfoamDisplay.SelectScaleArray = 'p'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'p'
pvfoamDisplay.GaussianRadius = 0.016100000143051147
pvfoamDisplay.SetScaleArray = ['POINTS', 'p']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = ['POINTS', 'p']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
pvfoamDisplay.ScalarOpacityFunction = pPWF
pvfoamDisplay.ScalarOpacityUnitDistance = 0.09516852503470223
pvfoamDisplay.OpacityArrayName = ['POINTS', 'p']
pvfoamDisplay.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
pvfoamDisplay.ScaleTransferFunction.Points = [-10.698009490966797, 0.0, 0.5, 0.0, 4160.533203125, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
pvfoamDisplay.OpacityTransferFunction.Points = [-10.698009490966797, 0.0, 0.5, 0.0, 4160.533203125, 1.0, 0.5, 0.0]

# show data from contour1
contour1Display = Show(contour1, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')
uLUT.RGBPoints = [0.001314725136998627, 0.231373, 0.298039, 0.752941, 1.6616502371549686, 0.865003, 0.865003, 0.865003, 3.3219857491729385, 0.705882, 0.0156863, 0.14902]
uLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
contour1Display.Representation = 'Surface'
contour1Display.ColorArrayName = ['POINTS', 'U']
contour1Display.LookupTable = uLUT
contour1Display.SelectTCoordArray = 'None'
contour1Display.SelectNormalArray = 'Normals'
contour1Display.SelectTangentArray = 'None'
contour1Display.OSPRayScaleArray = 'alpha.water'
contour1Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour1Display.SelectOrientationVectors = 'U'
contour1Display.ScaleFactor = 0.322000002861023
contour1Display.SelectScaleArray = 'alpha.water'
contour1Display.GlyphType = 'Arrow'
contour1Display.GlyphTableIndexArray = 'alpha.water'
contour1Display.GaussianRadius = 0.016100000143051147
contour1Display.SetScaleArray = ['POINTS', 'alpha.water']
contour1Display.ScaleTransferFunction = 'PiecewiseFunction'
contour1Display.OpacityArray = ['POINTS', 'alpha.water']
contour1Display.OpacityTransferFunction = 'PiecewiseFunction'
contour1Display.DataAxesGrid = 'GridAxesRepresentation'
contour1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour1Display.ScaleTransferFunction.Points = [0.5, 0.0, 0.5, 0.0, 0.5001220703125, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour1Display.OpacityTransferFunction.Points = [0.5, 0.0, 0.5, 0.0, 0.5001220703125, 1.0, 0.5, 0.0]

# show data from glyph1
glyph1Display = Show(glyph1, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'alphawater'
alphawaterLUT = GetColorTransferFunction('alphawater')
alphawaterLUT.RGBPoints = [-1.2076935377767098e-17, 0.231373, 0.298039, 0.752941, 0.5, 0.865003, 0.865003, 0.865003, 1.0, 0.705882, 0.0156863, 0.14902]
alphawaterLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = ['POINTS', '']
glyph1Display.LookupTable = alphawaterLUT
glyph1Display.SelectTCoordArray = 'None'
glyph1Display.SelectNormalArray = 'None'
glyph1Display.SelectTangentArray = 'None'
glyph1Display.OSPRayScaleArray = 'alpha.water'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'U'
glyph1Display.ScaleFactor = 0.3236260856501758
glyph1Display.SelectScaleArray = 'alpha.water'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'alpha.water'
glyph1Display.GaussianRadius = 0.01618130428250879
glyph1Display.SetScaleArray = ['POINTS', 'alpha.water']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = ['POINTS', 'alpha.water']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph1Display.ScaleTransferFunction.Points = [0.5, 0.0, 0.5, 0.0, 0.5001220703125, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph1Display.OpacityTransferFunction.Points = [0.5, 0.0, 0.5, 0.0, 0.5001220703125, 1.0, 0.5, 0.0]

# show data from annotateTimeFilter1
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
annotateTimeFilter1Display.FontSize = 36

# setup the color legend parameters for each legend in this view

# get color legend/bar for uLUT in view renderView1
uLUTColorBar = GetScalarBar(uLUT, renderView1)
uLUTColorBar.Title = 'U'
uLUTColorBar.ComponentTitle = 'Magnitude'

# set color bar visibility
uLUTColorBar.Visibility = 1

# show color legend
contour1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')
uPWF.Points = [0.001314725136998627, 0.0, 0.5, 0.0, 3.3219857491729385, 1.0, 0.5, 0.0]
uPWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'alphawater'
alphawaterPWF = GetOpacityTransferFunction('alphawater')
alphawaterPWF.Points = [-1.2076935377767098e-17, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]
alphawaterPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
