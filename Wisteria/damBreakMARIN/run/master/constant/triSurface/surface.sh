#! /bin/sh -x
surfaceTransformPoints -scale '(3.22 1 1)' cube.stl tmp.stl
surfaceTransformPoints -translate '(0 -0.5 0)' tmp.stl region.stl

surfaceTransformPoints -scale '(0.161 0.403 0.161)' cube.stl tmp.stl
surfaceTransformPoints -translate '(0.6635 -0.2015 0)' tmp.stl box.stl

surfaceTransformPoints -scale '(1.228 1 0.55)' cube.stl tmp.stl
surfaceTransformPoints -translate '(1.992 -0.5 0)' tmp.stl water.stl

rm -f tmp.stl
