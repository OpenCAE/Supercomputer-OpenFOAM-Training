set term pdf font "helvetica,17" linewidth 1.5 #端末,フォント,線幅,画面持続
set key bottom right
set xrange [0.3:2] #X軸のレンジ
set style data line #データのプロットスタイルはline
do for [i=1:8:1] { #実験値とCFD結果(probes関数出力)のプロット
  set yrange [*:*] #X軸のレンジ
  set output sprintf("comparison-pressure-P%d.pdf", i)
  plot '../SPHERIC_Test2/case.txt' u 1:i+1 t sprintf("P%d(Exp.)", i)\
  ,'< cat postProcessing/probes/*/p | sort -g' u 1:i+1 t sprintf("P%d(CFD)", i)\
  ,'< cat ../case-0/postProcessing/probes/*/p | sort -g' u 1:i+1 t sprintf("P%d(CFD case-0)", i)
}
