# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [927, 489]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [1.61000001430511, 0.0, 0.499999996873555]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-0.271758686641025, -2.7724400175163, 2.76899733692044]
renderView1.CameraFocalPoint = [2.65338709222251, 1.79815250973452, -1.31912008615726]
renderView1.CameraViewUp = [0.22027131639954403, 0.5681965861895442, 0.7928639143094861]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 1.75843681978879

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(927, 489)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.CaseType = 'Decomposed Case'
pvfoam.MeshRegions = ['internalMesh']
pvfoam.CellArrays = ['alpha.water']

# create a new 'Extract Block'
extractBlock1 = ExtractBlock(registrationName='ExtractBlock1', Input=pvfoam)
extractBlock1.BlockIndices = [0]

# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=extractBlock1)
threshold1.Scalars = ['CELLS', 'alpha.water']
threshold1.ThresholdRange = [0.5, 1.0]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'alphawater'
alphawaterLUT = GetColorTransferFunction('alphawater')
alphawaterLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.5001220703125, 0.865003, 0.865003, 0.865003, 1.000244140625, 0.705882, 0.0156863, 0.14902]
alphawaterLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'alphawater'
alphawaterPWF = GetOpacityTransferFunction('alphawater')
alphawaterPWF.Points = [0.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]
alphawaterPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Outline'
pvfoamDisplay.ColorArrayName = ['CELLS', 'alpha.water']
pvfoamDisplay.LookupTable = alphawaterLUT
pvfoamDisplay.SelectTCoordArray = 'None'
pvfoamDisplay.SelectNormalArray = 'None'
pvfoamDisplay.SelectTangentArray = 'None'
pvfoamDisplay.OSPRayScaleArray = 'U'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'U'
pvfoamDisplay.ScaleFactor = 0.322000002861023
pvfoamDisplay.SelectScaleArray = 'None'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'None'
pvfoamDisplay.GaussianRadius = 0.016100000143051147
pvfoamDisplay.SetScaleArray = ['POINTS', 'U']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = ['POINTS', 'U']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
pvfoamDisplay.ScalarOpacityFunction = alphawaterPWF
pvfoamDisplay.ScalarOpacityUnitDistance = 0.09516852503470223
pvfoamDisplay.OpacityArrayName = ['POINTS', 'U']
pvfoamDisplay.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
pvfoamDisplay.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
pvfoamDisplay.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# show data from threshold1
threshold1Display = Show(threshold1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold1Display.Representation = 'Surface'
threshold1Display.ColorArrayName = ['CELLS', 'alpha.water']
threshold1Display.LookupTable = alphawaterLUT
threshold1Display.SelectTCoordArray = 'None'
threshold1Display.SelectNormalArray = 'None'
threshold1Display.SelectTangentArray = 'None'
threshold1Display.OSPRayScaleArray = 'alpha.water'
threshold1Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold1Display.SelectOrientationVectors = 'None'
threshold1Display.ScaleFactor = 0.1228000044822693
threshold1Display.SelectScaleArray = 'None'
threshold1Display.GlyphType = 'Arrow'
threshold1Display.GlyphTableIndexArray = 'None'
threshold1Display.GaussianRadius = 0.006140000224113464
threshold1Display.SetScaleArray = ['POINTS', 'alpha.water']
threshold1Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold1Display.OpacityArray = ['POINTS', 'alpha.water']
threshold1Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold1Display.DataAxesGrid = 'GridAxesRepresentation'
threshold1Display.PolarAxes = 'PolarAxesRepresentation'
threshold1Display.ScalarOpacityFunction = alphawaterPWF
threshold1Display.ScalarOpacityUnitDistance = 0.07572640190359824
threshold1Display.OpacityArrayName = ['POINTS', 'alpha.water']
threshold1Display.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold1Display.ScaleTransferFunction.Points = [0.25, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold1Display.OpacityTransferFunction.Points = [0.25, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for alphawaterLUT in view renderView1
alphawaterLUTColorBar = GetScalarBar(alphawaterLUT, renderView1)
alphawaterLUTColorBar.Title = 'alpha.water'
alphawaterLUTColorBar.ComponentTitle = ''

# set color bar visibility
alphawaterLUTColorBar.Visibility = 1

# show color legend
pvfoamDisplay.SetScalarBarVisibility(renderView1, True)

# show color legend
threshold1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(threshold1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
