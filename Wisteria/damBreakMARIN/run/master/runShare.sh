#!/bin/sh
module load openfoam/v2106-rist                # OpenFOAM-v2106のRIST版moduleをload
source $WM_PROJECT_DIR/etc/bashrc              # OpenFOAMの環境設定
source $WM_PROJECT_DIR/bin/tools/RunFunctions  # 実行用関数定義
jid=${PJM_SUBJOBID:-$PJM_JOBID}                # サブジョブIDまたジョブIDをjid変数に代入
