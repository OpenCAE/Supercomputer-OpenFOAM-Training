``` Command
## 講習会作業用ディレクトリとホームディレクトリへのシンボリックリンク作成
# 講習会作業用ディレクトリ作成 (mkdir: make directories)
mkdir /work/$(id -gn)/share/lectures/$(id -un)
# ホームディレクトリへ移動 (cd: change the current directory to dir)
cd
# ディレクトリ名の確認 (pwd: print working directory)
pwd 
# ホームディレクトリ上のファイルやディレクトリの確認(ls: list directory contents)
ls -l
# ホームディレクトリに既にlecturesがあれば，mv lectures lectures.old 等で移動しておく
# シンボリックリンク作成 (ln: make links between files)
ln -s /work/$(id -gn)/share/lectures/$(id -un) lectures
# シンボリックリンクの確認
ls -l
```
``` Command
## 講習会レポジトリの複製
# 講習会作業用ディレクトリへ移動
cd lectures
# 講習会レポジトリの複製   
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
# レポジトリのディレクトリへ移動
cd Supercomputer-OpenFOAM-Training
# 本講習会のブランチへの切り替え
git checkout Information-Technology-Center-The-University-of-Tokyo-20240918
# ブランチ名の表示
git branch
```
``` CommandOutput
* Information-Technology-Center-The-University-of-Tokyo-20240918
  master
```
``` Command
## 実行ディレクトリに移動
cd ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/AhmedBody/run
```
``` Command
## 作成済の解析ケースのコピー
# デフォルトの実行ケースを作成
cp -r master/ case-0
# ケースディレクトリに移動 
cd case-0/
```
``` Command
## 混雑状況の確認
# 投げられているジョブ数
pjstat --rsc -b
# リソースグループのノード使用率
pjstat --rscuse
```
``` Command
## ベース格子生成ジョブの投入
pjsub run1base.sh
```
``` Command
## ジョブの状況確認
pjstat
```
``` Command
## ログファイルのトレース(ジョブの実行中，または，実行後)
tail -f log.*
```
``` Command
## ジョブのエラー出力の確認
less run1base.sh.*.err
# 本来はJOBIDが付くが，*のワイルドカードで全ての文字にマッチする
# ls -al で上記のファイルのサイズが0である事を確認するのでも良い
```
``` Command
## ジョブの標準出力ファイルの確認
less run1base.sh.*.out
```
``` Command
## ログ確認
less log.surfaceFeatureExtract.*
less log.blockMesh.*
less log.decomposePar.*
```
``` Command
## OpenFOAMデータ可視化用ダミーファイル作成
# ダミーファイルのファイル名は任意．ただし，拡張子は.foam
touch pv.foam
```
``` Command
## paraviewの起動
# OpenFOAMの環境設定(ParaView-5.9.1も使えるようになる)
module load gcc/8.3.1 impi/2021.8.0 openfoam/v2106;source $WM_PROJECT_DIR/etc/bashrc
# paraviewをバックグラウンドジョブで起動
paraview &
```
``` Command
## 格子生成ジョブの投入
pjsub run2mesh.sh
```
``` Command
## ジョブの状況確認
pjstat
```
``` Command
## ログファイルのトレース
tail -f log.*
```
``` Command
## ジョブのエラー出力と標準出力ファイルの確認
less run2mesh.sh.*.err
less run2mesh.sh.*.out
```
``` Command
## snappyHexMeshのログ確認
less log.snappyHexMesh.*
```
``` Command
## checkMeshのログ確認
less log.checkMesh.*
```
``` Command
## コピー元の初期値場の確認
ls 0.orig/
# 0.orig/にコピーする初期値場を置いておく
```
``` Command
## 初期値場コピー
pjsub run3init.sh
pjstat # ジョブが終了したら次に進む
```
``` Command
## 並列計算用ディレクリにおける初期値場の確認
ls processor*/0/
# 0.orig/にあった初期値場が，全並列計算用ディレクリ(processor0-575)にコピーされていることを確認する
```
``` Command
## 流体解析実行
pjsub run4solve.sh
```
``` Command
## 関数出力のモニター
foamMonitor -y "[-1:1]" -r 1 postProcessing/forceCoeffs/0/coefficient.dat
# -y : 縦軸のレンジ指定． -r :更新秒
# "File postProcessing/forceCoeffs/0/coefficient.dat does not exit" → 暫く待って再実行
# "foamMonitor: command not found" → 以下のOpenFOAMの環境設定を再度行う
module load gcc/8.3.1 impi/2021.8.0 openfoam/v2106;source $WM_PROJECT_DIR/etc/bashrc
```
``` Command
## 空力係数の関数出力の表示
less postProcessing/forceCoeffs/0/coefficient.dat
#末尾に飛ぶにはG(大文字のg)を押す
```
``` Command
## プロット実行
gnuplot plot/forceCoeffs.gp
gnuplot plot/forces.gp
gnuplot plot/profileUx.gp
gnuplot plot/residual.gp
```
``` Command
## プロットファイル閲覧
evince *.pdf &
# 他のPDFファイルが閲覧できるコマンド例: firefox, gs
```
``` Command
## 空力解析演習実行例(12ノード/576並列 → 4ノード/192並列))
cd ..                            # 実行ディレクトリrunに移動
cp -r master/ case-node4         # コピー先のディレクトリ名は任意
cd case-node4/                   # ケースディレクトリに移動
git add .                        # 変更箇所確認を容易にするためにgitで管理する
nano system/decomposeParDict     # 設定変更(numberOfSubdomains 192)
nano run2mesh.sh                 # 設定変更(node=4, proc=192)
nano run4solve.sh                # 設定変更(node=4, proc=192)
git diff                         # 変更箇所を確認する
../../../bin/Allrun.batch        # 全ジョブを実行
pjstat -E                        # -E: ステップジョブ内のサブジョブを展開して表示
tail -f log.*                    # 実行中のジョブのログトレース
```
``` Command
## プロット
./Allrun.plot  
evince *.pdf
```
``` Command
## 解析時間の集計結果表示
cd .. # 実行ディレクトリrunに移動
~/lectures/Supercomputer-OpenFOAM-Training/bin/averageExecutionTime.sh */log.simpleFoam.*
```
``` CommandOutput
#Filename,TimeSteps[-],InitTime[s],LastTime[s],Time[s],AveTime[s]
case-0/log.simpleFoam.4863609,500,2.48,87.47,84.99,0.170321
case-node4/log.simpleFoam.4863877_3,459,2.25,205.49,203.24,0.443755
#4ノードを基準とした場合の12ノードのスピードアップと並列化効率
#スピードアップ=0.443755/0.170321=2.61
#並列化効率[%]=0.443755/0.170321/(12/4)*100=86.8
```
``` Command
## ケースの複製
cd ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/AhmedBody/run #実行ディレクトリ移動
cp -r master case-gpu4  #既存ケースのコピー
cd case-gpu4            #新ケースのディレクトリに移動
cp system/gpu/* system/ #線形ソルバの設定変更
less system/fvSolution  #線形ソルバの設定確認
```
``` Command
## 圧力線形ソルバに関するAmgXのオプション設定確認
less system/amgxpOptions
```
``` Command
## GPUオフロード実行用ジョブスクリプトの確認
less gpuShare.sh
```
``` Command
## ジョブ投入
../../../bin/Allrun.batch gpu[0-9]*.sh
pjstat -E     # -E: ステップジョブ内のサブジョブを展開して表示
tail -f log.* # ログのトレース．表示が止まったらC-cで中止(log.simpleFoamが表示されるまで繰り返す)
foamMonitor -y "[-1:1]" -r 1 postProcessing/forceCoeffs/0/coefficient.dat & # モニター
./Allrun.plot # 計算終了後プロット
evince *.pdf  # プロット閲覧
cd ..         # 実行ディレクトリrunに移動して，解析時間の比較
~/lectures/Supercomputer-OpenFOAM-Training/bin/averageExecutionTime.sh */log.simpleFoam*
```
``` CommandOutput
#Filename,TimeSteps[-],InitTime[s],LastTime[s],Time[s],AveTime[s]
case-0/log.simpleFoam.4863609,500,2.48,87.47,84.99,0.170321
case-gpu4/log.simpleFoam-gpu.4863769_3,500,5.22,670.31,665.09,1.33285
case-node4/log.simpleFoam.4863877_3,459,2.25,205.49,203.24,0.443755
```
