#!/bin/bash
#PJM -L rg=lecture-o
#PJM -L node=1
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runShare.sh # 共通設定
reconstructParMesh -constant &> log.reconstructParMesh.$jid # 格子の再構築
reconstructPar &> log.reconstructPar.$jid                   # 解析結果の再構築
