set terminal pdf enhanced color solid font "Times,17"
set output "forceCoeffs.pdf"
set xlabel "Time"
set ylabel "Force coefficients [-]"
set grid
set style data line
set key outside box width 2
set yrange [0.2:0.5]
plot \
0.299 title "C_d(Exp.)" with l lc 1 dt "-" lw 2,\
0.345 title "C_l(Exp.)" with l lc 3 dt "-" lw 2,\
"postProcessing/forceCoeffs/0/coefficient.dat" using 1:2  title "C_d(CFD)" with l lc 1,\
"" using 1:4  title "C_l(CFD)" with l lc 3
#    EOF
