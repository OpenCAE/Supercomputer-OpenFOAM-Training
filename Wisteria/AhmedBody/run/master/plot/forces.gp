set terminal pdf enhanced color solid font "Times,17"
set output "forces.pdf"
set xlabel "Time"
set ylabel "Drag force [N]"
set grid
set style data line
set key outside box
set yrange [0:50]
plot \
"< tr '()' '  ' < postProcessing/forces/0/force.dat" using 1:($2*2) title "Total",\
"" using 1:($5*2) title "Pressure",\
"" using 1:($8*2) title "Viscous"
#    EOF

