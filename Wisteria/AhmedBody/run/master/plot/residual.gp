set terminal pdf enhanced color solid font "Times,16"
set output "residual.pdf"
set logscale y
set xlabel "Time"
set ylabel "Residual"
set grid
set style data line
set key outside box width 1
plot \
"postProcessing/solverInfo/0/solverInfo.dat" using 1:3  title "Ux",\
"" using 1:6  title "Uy",\
"" using 1:9  title "Uz",\
"" using 1:14 title "p"
