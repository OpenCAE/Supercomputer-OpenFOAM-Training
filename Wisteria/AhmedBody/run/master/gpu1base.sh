#!/bin/bash
#PJM -L rg=lecture-a
#PJM -L gpu=1
#PJM --mpi proc=1
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./gpuShare.sh # 共通設定
surfaceFeatureExtract &> log.surfaceFeatureExtract.$jid # 特徴辺抽出
blockMesh &> log.blockMesh.$jid # ベース格子生成
decomposePar -cellDist &> log.decomposePar.$jid # 領域分割
# -cellDist : 格子の領域番号を場cellDistに出力．可視化用なので必須ではない．
