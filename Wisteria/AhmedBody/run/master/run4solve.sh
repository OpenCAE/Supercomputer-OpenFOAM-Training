#!/bin/bash
#PJM -L rg=lecture-o
#PJM -L node=12
#PJM --mpi proc=576
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runShare.sh # 共通設定
mpiexec -of log.simpleFoam.$jid simpleFoam -parallel # 並列流体解析
