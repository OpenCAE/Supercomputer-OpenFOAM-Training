#!/bin/bash
#PJM -L rg=lecture-a
#PJM -L gpu=4
#PJM --mpi proc=36
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./gpuShare.sh # 共通設定
mpiexec -n $PJM_MPI_PROC -hostfile $PJM_O_NODEINF -npernode $PJM_PROC_BY_NODE \
snappyHexMesh -overwrite -parallel &> log.snappyHexMesh.$jid # 並列格子生成

mpiexec -n $PJM_MPI_PROC -hostfile $PJM_O_NODEINF -npernode $PJM_PROC_BY_NODE \
renumberMesh -overwrite -parallel &> log.renumberMesh.$jid # 格子順変更による行列バンド幅縮小

mpiexec -n $PJM_MPI_PROC -hostfile $PJM_O_NODEINF -npernode $PJM_PROC_BY_NODE \
checkMesh -constant -parallel &> log.checkMesh.$jid # 格子の品質チェック
