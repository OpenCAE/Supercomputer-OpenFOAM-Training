#!/bin/bash
#PJM -L rg=lecture-o
#PJM -L node=1
#PJM --mpi proc=1
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runShare.sh # 共通設定
surfaceFeatureExtract &> log.surfaceFeatureExtract.$jid # 特徴辺抽出
blockMesh &> log.blockMesh.$jid # ベース格子生成
snappyHexMesh -overwrite &> log.snappyHexMesh.$jid # 並列格子生成
renumberMesh -overwrite &> log.renumberMesh.$jid # 格子順変更による行列バンド幅縮小
checkMesh -constant &> log.checkMesh.$jid # 格子の品質チェック
