#!/bin/bash
#PJM -L rg=lecture-a
#PJM -L gpu=4
#PJM --mpi proc=36
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./gpuShare.sh # 共通設定
mpiexec -n $PJM_MPI_PROC -hostfile $PJM_O_NODEINF -npernode $PJM_PROC_BY_NODE \
numactl --localalloc \
$application -lib petscFoam -parallel &> log.$application-gpu.$jid  # 並列流体解析
