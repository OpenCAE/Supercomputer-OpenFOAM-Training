#!/usr/bin/env python

import numpy as np
import csv
import math

Uref=40.0
nu=1.5e-5
Cmu=0.09
l=1.4*0.07

for y in ['000', '200', '400']:
   filename="case82/inlet-y"+y+".dat"
   data=np.genfromtxt(filename,skip_header=7,names=True)
   
   z=data['zm']
   U=data['Ums']
   uu=data['uum2s2']
   vv=data['vvm2s2']
   ww=data['wwm2s2']
   
   n=len(z)
   
   k=0.5*(uu+vv+ww)
   I=2./3./Uref*k**0.5
   epsilon=Cmu**0.75*k**1.5/l
   omega=epsilon/k/Cmu
   nut=Cmu*k**2/epsilon
   
   dz=z.copy()
   for i in range(n-1):
      dz[i]-=z[i+1]
   
   result=np.hstack((
      z.reshape(n,1),
      dz.reshape(n,1),
      U.reshape(n,1),
      I.reshape(n,1),
      k.reshape(n,1),
      epsilon.reshape(n,1),
      omega.reshape(n,1),
      nut.reshape(n,1)
   ))
   
   header="z[m],dz[m],U[m/s],I[-],k[m2/s2],epsilon[m2/s3],omega[1/s],nut[m2/s]"
   footer="Average,weight,%.4g,%.4g,%.4g,%.4g,%.4g,%.4g" % (
      np.average(U,weights=dz),
      np.average(I,weights=dz),
      np.average(k,weights=dz),
      np.average(epsilon,weights=dz),
      np.average(omega,weights=dz),
      np.average(nut,weights=dz)
    )
   
   np.savetxt('inflow-'+y+'.csv'
              , result
              , header=header
              , footer=footer
              , delimiter=','
              , fmt='%.4g'
              , comments='#')
