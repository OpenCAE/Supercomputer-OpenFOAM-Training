#!/bin/bash
#PJM -L rg=tutorial-o
#PJM -L node=1
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
#PJM -j
# OpenFOAM-v2106のRIST版moduleをload
module load openfoam/v2106-rist
# OpenFOAMの環境設定
source $WM_PROJECT_DIR/etc/bashrc
# 各種環境変数の設定
export HOME=/work/gt00/$USER
export PATH="$HOME/.local/bin:$PATH"
export TERM=dumb
# Optunaによる最適化を実行
python -u single_objective_optimizaion.py &> log.single_objective_optimizaion.py.$PJM_JOBID
# -u : 標準入力，標準出力，標準エラーをバッファリングしない
# 本オプションは必須ではなく，動作確認用に標準出力と標準エラーの出力順序が変わらないようにしている
