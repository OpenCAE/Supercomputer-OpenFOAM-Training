#!/bin/bash
#PJM -L rg=tutorial-o
#PJM -L node=1
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -j
#PJM -S
# 計算ノードからアクセス可能な共有ファイルシステムにHOMEを設定
export HOME=/work/gt00/$USER
# 実行ファイルがインストールされるパスをPATHに加える
export PATH="$HOME/.local/bin:$PATH"
# Pythonのライブラリを$HOME/.local以下にインストール
python3 -m pip install --user scipy optuna PyFoam &> log.install_Odyssey.sh.$PJM_JOBID
