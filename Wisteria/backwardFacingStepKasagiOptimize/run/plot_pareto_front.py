#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# optunaをインポート
import optuna

# Studyオブジェクトの作成
study = optuna.load_study(
    study_name="multi_objective_optimization",
    storage="sqlite:///optuna.db",
)

# すべてのトライアルをプロット（デフォルト挙動）
optuna.visualization.plot_pareto_front(
	study,
	include_dominated_trials=True
).show()

# Study.best_trials だけをプロット
optuna.visualization.plot_pareto_front(
	study,
	include_dominated_trials=False
).show()
