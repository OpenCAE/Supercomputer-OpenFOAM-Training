#!/bin/bash
# plotディレクトリにある全.gpファイルをgnuplotで処理してプロット
for file in ../share/plot/*.gp
do
    gnuplot $file
done

convert -density 300 residual.eps profileU.eps profileV.eps profilek.eps ${PWD##*/}.pdf
