#!/bin/bash
#PJM -L rg=tutorial-o
#PJM -L node=1
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
#PJM -j
# OpenFOAM-v2106のRIST版moduleをload
module load openfoam/v2106-rist
# OpenFOAMの環境設定
source $WM_PROJECT_DIR/etc/bashrc
# 各種環境変数の設定
export HOME=/work/gt00/$USER
export PATH="$HOME/.local/bin:$PATH"
export TERM=dumb
# 並列分散実行数
nParallels=10
# 並列分散実行のループ
for ((i=1; i <= $nParallels; i++))
do
    # Optunaによる最適化をバックグランドで実行
    python -u single_objective_optimizaion.py &> log.single_objective_optimizaion.py.$PJM_JOBID.p$i &

    # 最初のOptunaプロセスのみ実行
    if [ $i -eq 1 ]
    then
	# 待機ループ
	while :
	do
	    # SQLite3のデータベースのStudyリスト表示が正常終了したら待機終了
	    optuna studies -q --storage sqlite:///optuna.db && break
	    sleep 1
	done
    fi
done
# Optunaによる最適化プロセスは全てバックグランドで実行されているので，
# バッチジョブが終了してしまわないよう，全プロセスが終了するまで待つ
wait
