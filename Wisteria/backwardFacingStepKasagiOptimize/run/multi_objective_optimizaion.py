#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# optunaをインポート
import optuna
# OpenFOAMのファイルをPythonのdictとして読み書き可能なPyFoamのParsedParameterFileをインポート
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
# その他，必要なライブラリをimport
import hashlib
import numpy as np
import os
import pathlib
import shutil
import subprocess
import time


def objective(trial):
    ### 目的関数を定義
    
    ## OpenFOAMの設定を提案

    # 壁関数を提案
    trial.suggest_categorical(
        "0/nut:typeWall",
        [ "nutkWallFunction", "nutUWallFunction", "nutUSpaldingWallFunction" ]
    )
    # 乱流モデルを提案
    trial.suggest_categorical(
        "constant/turbulenceProperties:RAS/RASModel",
        [ "RNGkEpsilon", "kEpsilon", "kOmegaSST", "LienCubicKE", "ShihQuadraticKE" ]
    )
    # 速度場の移流項スキームを提案
    trial.suggest_categorical(
        "system/fvSchemes:velocity",
        [ "Gauss upwind", "Gauss linearUpwindV unlimited", "Gauss linearUpwindV limited", "Gauss limitedLinearV 1", "Gauss limitedLinearV 0.5", "Gauss limitedLinearV 0", "Gauss QUICKV", "Gauss SuperBeeV", "Gauss MUSCLV" ]
    )
    # 乱流場の移流項スキームを提案
    trial.suggest_categorical(
        "system/fvSchemes:turbulence",
        [ "Gauss upwind", "Gauss linearUpwind unlimited", "Gauss linearUpwind limited", "Gauss limitedLinear 1", "Gauss limitedLinear 0.5", "Gauss limitedLinear 0", "Gauss QUICK", "Gauss SuperBee", "Gauss MUSCL" ]
    )

    ## 提案されたパラメータからケースディレクトリ名を決定

    # 提案されたパラメータをキーの順にソート
    sortedParames = {key: trial.params[key] for key in sorted(trial.params.keys())}

    # ソートしたパラメータを文字列化
    paramsStr = f"{sortedParames}"

    # 文字列化したパラメータをSHA1でダイジェスト化
    case = "_"+hashlib.sha1(paramsStr.encode("utf-8")).hexdigest()

    # ケースディレクトリ名
    casePath = pathlib.Path("cases") / case
    print(f"Case directory: {casePath}")

    ## ケースディレクトリ上でソルバを実行

    # ソルバ実行の結果，目的関数の評価値が書き込まれたログファイル
    logFilename1=casePath / "log.velocityRMSE"
    logFilename2=casePath / "log.kineticEnergyRMSE"

    # ケースディレクトリの存在確認
    if casePath.exists():
        # ケースディレクトリが存在したら，計算中，または，既に計算済み
        while True:
            # 目的関数のファイルが無ければ計算中なので，存在するまで待つ
            if logFilename1.exists() and logFilename2.exists():
                break
            time.sleep(1)
        time.sleep(3)
    else:
        # 雛型をコピーしてケースディレクトリを作成
        shutil.copytree("cases/main", casePath, symlinks=True)

        # パラメータ文字列をファイルに出力
        with open(casePath / "BOOF.params", "w") as f:
            f.write(paramsStr)

        # OpenFOAMの設定ファイルの内容をパラメータに応じて修正
        with ParsedParameterFile(casePath / "0/nut" ) as f:
            f[ "typeWall" ] = trial.params[ "0/nut:typeWall" ]
            f.writeFile()

        with ParsedParameterFile(casePath / "constant/turbulenceProperties" ) as f:
            f[ "RAS" ][ "RASModel" ] = trial.params[ "constant/turbulenceProperties:RAS/RASModel" ]
            f.writeFile()

        with ParsedParameterFile(casePath / "system/fvSchemes" ) as f:
            f[ "velocity" ] = trial.params[ "system/fvSchemes:velocity" ]
            f[ "turbulence" ] = trial.params[ "system/fvSchemes:turbulence" ]
            f.writeFile()

        # ケースディレトリ上でOpenFOAMのソルバを実行
        command = ["../share/run.sh"]
        try:
            command_output = subprocess.check_output(
                command,
                cwd=casePath,
                universal_newlines=True
            )
        except subprocess.CalledProcessError as e:
            print(f"Error in job {command} at {casePath}")
            return np.nan, np.nan

    ## 目的関数の評価値を返す

    if logFilename1.exists() and logFilename2.exists():
        # 目的関数評価値のログファイルが存在した場合は，ソルバが正常終了
        # ログファイルの読み出し
        with open(logFilename1) as logf:
            line1=logf.readline()
        with open(logFilename2) as logf:
            line2=logf.readline()

        # 数値化して目的関数の評価値を返す
        return float(line1), float(line2)
    else:
        # 目的関数評価値のログファイルが存在しない場合は，ソルバが異常終了(発散)
        # Optunaが失敗試行とみなす評価値であるnanをログファイルに書き込む
        if not logFilename1.exists():
            with open(logFilename1, mode = "w") as logf:
                logf.write("nan")
        if not logFilename2.exists():
            with open(logFilename2, mode = "w") as logf:
                logf.write("nan")

        # 目的関数の評価値としてnanを返す
        return np.nan, np.nan


def enqueDefaultParameters(study):
    ### デフォルトのパラメータを初期の探索点に追加

    ## 標準的な解析条件を初期の探索点に追加
    study.enqueue_trial({
        "0/nut:typeWall": "nutkWallFunction",
        "constant/turbulenceProperties:RAS/RASModel": "kEpsilon",
        "system/fvSchemes:velocity": "Gauss upwind",
        "system/fvSchemes:turbulence": "Gauss upwind"
    })
    # 試すべき他の解析条件を探索点に追加しても良い

    
def printBestTrials(trials):
    ### 最適化結果の表示
    print("\nBest Trial(s):")
    for trial in trials:
        # ケースディレクトリ名
        sortedParames = {key: trial.params[key] for key in sorted(trial.params.keys())}
        paramsStr = f"{sortedParames}"
        case = "_"+hashlib.sha1(paramsStr.encode("utf-8")).hexdigest()
        casePath = pathlib.Path("cases") / case

        # 試行番号，パス，パラメータ，評価値の表示
        print(f"- [{trial.number}] path={casePath}, params={trial.params}, values={trial.values}")
    

if __name__ == "__main__":
    ### 最適化の実行

    ## 履歴を保存するストレージの指定
    storage = optuna.storages.RDBStorage(
        # SQLite3の関係データベースを用いoptuna.dbに保存
        url = "sqlite:///optuna.db",

        # タイムアウトに100sを指定
        engine_kwargs = {"connect_args": {"timeout": 100}}
    )

    ## サンプラーの指定
    # 探索点選択アルゴリズムとしてTPE(tree-structured Parzen estimator)を用いるサンプラーを指定
    # Ref.:「Optunaによるブラックボックス最適化」6.4 探索点選択アルゴリズムの使い分け
    sampler = optuna.samplers.TPESampler()

    # Random sampler
    #sampler = optuna.samplers.RandomSampler()

    # NSGAII sampler
    #sampler = optuna.samplers.NSGAIISampler()

    # QMC sampler
    #sampler = optuna.samplers.QMCSampler()
    # Note: QMCSampler is not supported for Python 3.6.8.

    # CmaEs sampler
    #sampler = optuna.CmaEsSampler()
    # Note: CmaEsSampler does not support dynamic search space or `CategoricalDistribution`.

    # Grid sampler
    #sampler = optuna.samplers.GridSampler(search_space)
    # search_spaceの指定が必須
    # Ref.: https://optuna.readthedocs.io/en/latest/reference/samplers/generated/optuna.samplers.GridSampler.html

    # BoTorch sampler
    #sampler = optuna.samplers.BoTorchSampler()
    # Note: BoTorchのインストールが必要
    
    ## Studyオブジェクトの作成
    study = optuna.create_study(
        # スタディが存在すれば読み出す(スタディの再開や分散並列最適化用)
        load_if_exists = True,

        # ストレージを指定
        storage = storage,

        # サンプラーを指定
        sampler = sampler,

        # Study名を指定
        study_name = "multi_objective_optimization",

        # 最適化の方向として目的関数の最小化を指定
        directions=[ "minimize", "minimize" ],
    )

    ## 最初にデフォルトのパラメータを試す
    if len(study.trials) == 0:
        enqueDefaultParameters(study)

    ## 最適化

    # 最適化ループの回数
    n_trials = 10

    # 最適化の実行
    study.optimize(objective, n_trials=n_trials)

    # Study statistics
    timeout_trials = [t for t in study.trials
                      if t.state == optuna.trial.TrialState.FAIL]
    complete_trials = [t for t in study.trials
                       if t.state == optuna.trial.TrialState.COMPLETE]

    # 終了・タイムアウト・正常終了の試行数の表示
    print("\nStudy statistics:")
    print(f"  Number of finished trials: {len(study.trials)}")
    print(f"  Number of timeout  trials: {len(timeout_trials)}")
    print(f"  Number of complete trials: {len(complete_trials)}")

    # 最適化結果の表示
    printBestTrials(study.best_trials)

