#!/bin/bash
# 計算ノード用にインストールされたPythonライブラリを共有するために.localをリンク
[ ! -L /home/$USER/.local ] && ln -s /work/gt00/$USER/.local /home/$USER/.local
# Pythonのライブラリを~/.local以下にインストール
python -m pip install --user scipy optuna-dashboard plotly &> log.install_login.sh.$$
