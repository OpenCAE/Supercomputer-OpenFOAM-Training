``` Command
## 講習会作業用ディレクトリとホームディレクトリへのシンボリックリンク作成
# 講習会作業用ディレクトリ作成 (mkdir: make directories)
mkdir /work/$(id -gn)/share/lectures/$(id -un)
# ホームディレクトリへ移動 (cd: change the current directory to dir)
cd
# ディレクトリ名の確認 (pwd: print working directory)
pwd 
# ホームディレクトリ上のファイルやディレクトリの確認(ls: list directory contents)
ls -l
# ホームディレクトリに既にlecturesがあれば，mv lectures lectures.old 等で移動しておく
# シンボリックリンク作成 (ln: make links between files)
ln -s /work/$(id -gn)/share/lectures/$(id -un) lectures
# シンボリックリンクの確認
ls -l
```
``` Command
## 講習会レポジトリの複製
# 講習会作業用ディレクトリへ移動
cd lectures
# 講習会レポジトリの複製   
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
# レポジトリのディレクトリへ移動
cd Supercomputer-OpenFOAM-Training
# 本講習会のブランチへの切り替え
git checkout Information-Technology-Center-The-University-of-Tokyo-20240522
# ブランチ名の表示
git branch
```
``` Command
## 利用可能な全てのmoduleの表示
show_module # 当センター独自のコマンド
# 表示が長いので，パイプ機能(|)でページャーのlessに渡して検索できるようにする
show_module | less
```
``` Command
## lessのコマンドでopenfoamを検索
# コマンド例: h:ヘルプ(help), スペース:次の画面, b:前の画面(back), /正規表現:検索, q:終了(quit)
/openfoam
```
``` Command
## lessを終了
q
```
``` Command
## 現環境で利用可能なmoduleの表示
# -lオプションで更新日などの詳細が表示される
module avail -l
# 自動的にlessにパイプされているので，openfoamを検索する．
/openfoam
# BaseCompilerやMPIのmoduleをloadしていないので，OpenFOAMのmoduleは表示されない
q
# lessを終了
```
``` Command
## BaseCompilerとMPIのmoduleのload
module load gcc/8.3.1 impi/2021.8.0
# loadするmodule名は複数指定できる
```
``` Command
## 現環境で利用可能なopenfoam moduleの表示
module avail -l openfoam
```
``` Command
## moduleのhelp表示
module help openfoam/v2106
```
``` Command
## OpenFOAM v2106のmoduleのload
module load openfoam/v2106
```
``` Command
## OpenFOAMの環境設定
source $WM_PROJECT_DIR/etc/bashrc
```
``` Command
## バージョンの表示
foamVersion
```
``` Command
## 環境設定手順をalias(別名，ショートカットコマンド)として登録するスクリプトを表示
# cat file : fileの内容を表示する．catはCATenateの略
# ~はホームディレクトリ
cat ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/etc/.bashrc
```
``` Command
## OpenFOAMの環境設定のalias化
source ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/etc/.bashrc
```
``` Command
## aliasの確認
alias
```
``` Command
## Aquarius用OpenFOAM v2106の環境設定aliasの実行 
OFv2106
```
``` Command
## bashシェル実行時にalias登録を自動的に行うよう~/.bashrcに設定追加
# >> : ファイルに追記モードでリダイレクト
cat ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/etc/.bashrc >> ~/.bashrc
```
``` Command
## ~/.bashrcの確認
less ~/.bashrc
```
``` Command
## 作業ディレクトリへの移動
cd ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/cavity/run/
```
``` Command
## 演習ケースのコピー
# cp : コピー(copy)コマンド
#   -r : 再帰的コピー
cp -r master cavity
```
``` Command
## コピーの確認(カレントディレトリの内容確認)
ls -l
```
``` Command
## ケースディレクトリへの移動
# cd の後にTABを押すと，cd cavity/ まで補完される．なお，cd cavtity でも良い．
cd cavity/
```
``` Command
## ファイル構成の確認
# tree : ディレクトリ構造を樹木状に表示(標準コマンドではないが，本スパコンにはインストール済み)
tree
# 主なファイルのみを示す
```
``` Command
less system/blockMeshDict
```
``` Command
## ジョブスクリプトの確認
less run0mesh.sh
```
``` Command
## 共通設定スクリプトの確認
less runShare.sh
```
``` Command
## pjstatのマニュアル閲覧
man pjstat
```
``` Command
## ノードの利用状況の表示(混雑確認．必須ではない)
pjstat --rscuse # さらに-vをつけるとより詳細に表示される
```
``` Command
## 投げられているジョブ数を見る(混雑確認．必須ではない)
pjstat --rsc -b
```
``` Command
## 格子生成のジョブ投入
pjsub run0mesh.sh
# 講習会時lecture-oが混んでいる場合には，以下のようにしてtutorial-oに投入(rgはrscgrpでも可)
pjsub -L rg=tutorial-o run0mesh.sh
# run0mesh.sh でのrgの指定を書き変えても良い
```
``` Command
## ジョブの状況確認
pjstat
# 以降の演習ではジョブ状態の確認を適宜行うこと
```
``` Command
tree
```
``` Command
## ジョブの出力ファイルを確認
less run0mesh.sh.* # *のワイルドカードで全ての文字にマッチする
# 特にジョブの標準エラー出力 *.err にエラーが出ていないことを確認
```
``` Command
less log.blockMesh.*
```
``` Command
## ParaViewによるOpenFOAMデータ可視化用ダミーファイル作成
touch pv.foam # 中身は何でも良いので，touchコマンドで空ファイルを作成する
```
``` Command
## OpenFOAMの環境設定aliasの実行(既に実行している場合には不要)
OFv2106 # OpenFOAM module内のParaViewを使うため
```
``` Command
## paraviewをバックグラウンドジョブで起動
paraview &
```
``` Command
## ファイル構成の確認
tree
```
``` Command
## ファイルの確認
less 0/U
```
``` Command
## ファイルの確認
less 0/p
```
``` Command
## ファイルの確認
less constant/transportProperties
```
``` Command
less system/fvSchemes
```
``` Command
less system/fvSolution
```
``` Command
less system/controlDict
```
``` Command
## 実行制御の設定ファイルの編集
emacs -nw system/controlDict
# もちろん，以下の例のように，各自の好みのエディタで編集して構わない
# vi system/controlDict
# nano system/controlDict
```
``` Command
## ジョブスクリプトの確認
less run1sol.sh
```
``` Command
## ソルバ実行のジョブ投入
pjsub run1sol.sh
```
``` Command
## ジョブ確認
pjstat
```
``` Command
## ファイルの確認
tree
```
``` Command
## ログの確認
less log.icoFoam.*
```
``` Command
##初期残差のプロット
foamMonitor -l -r 1 postProcessing/solverInfo/0/solverInfo.dat &
# -l : 縦軸ログスケール． -r 1 : 更新間隔1秒(デフォルト10秒)
```
``` Command
## 既往計算ケースの複製
cd ..                              #親ディレクトリ(run)に移動
cp -r cavity cavity100             #ケースのコピー
cd cavity100/                      #新ケースのディレクトリに移動
tree                               #適宜ケース内のファイルを確認
rm log.* *.err *.out *.stats       #実行結果ファイルを消去
rm -r 0.*/ postProcessing/         #実行結果ディレクトリを消去
tree                               #適宜ケース内のファイルを確認
```
``` Command
## 格子生成設定ファイルの編集
emacs -nw system/blockMeshDict
```
``` Command
## 実行制御の設定ファイルの編集
emacs -nw system/controlDict
```
``` Command
## ジョブ投入
pjsub run0mesh.sh # pjstatでジョブの終了を確認してから次に進む
pjsub run1sol.sh
```
``` Command
## 初期残差のプロット
foamMonitor -l -r 1 -i 5 postProcessing/solverInfo/0/solverInfo.dat &
```
``` Command
## サンプリング設定ファイルの内容確認
less system/sample
```
``` Command
## ジョブスクリプトの確認
less runpost.sh
```
``` Command
## サンプリングの実行
pjsub runpost.sh
pjstat #ジョブの終了を確認してから次に進む
```
``` Command
## サンプリング結果確認
less postProcessing/sample/15/lineX1_U.xy
```
``` Command
## プロットファイルの確認
less plot/profiles.gp
```
``` Command
## gnuplot実行(キャラクターでの表示もされる)
gnuplot plot/profiles.gp
```
``` Command
## プロットファイル表示
evince profiles.pdf #gvの代わりに，firefox, gv, gsでも良い
```
``` Command
## 領域分割設定ファイルの編集
emacs -nw system/decomposeParDict
```
``` Command
less run2de.sh
```
``` Command
## 領域分割ジョブの投入
pjsub run2de.sh
pjstat # ジョブの完了を確認して，次に進む
```
``` Command
less log.decomposePar.*
```
``` Command
## 領域分割で生成されたファイルの確認
tree
```
``` Command
## paraviewを起動
paraview &
```
``` Command
less run3sol-par.sh
```
``` Command
pjsub run3sol-par.sh
pjstat #ジョブ開始後，次に進む
less log.icoFoam-par.*
```
``` Command
## ソルバーのログから計算時間を算出するスクリプトを使用して算出
~/lectures/Supercomputer-OpenFOAM-Training/bin/averageExecutionTime.sh log.icoFoam*
```
``` Command
less run4re.sh
```
``` Command
## 領域再構築ジョブの投入
pjsub run4re.sh
pjstat # ジョブの完了を確認して，次に進む
```
``` Command
## 並列計算結果と再構築結果の確認
tree
```
``` Command
## PETScケースの作成
cd ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/cavity/run #実行用ディレクトリに移動
cp -r cavity100 cavity100-PETSc                               #既存ケースのコピー
cd cavity100-PETSc/                                           #新ケースのディレクトリに移動
rm log.icoFoam*                                               #解析ソルバーのログの消去
rm -r postProcessing/                                         #後処理ディレクトリを消去
cp system/fvSolution.PETSc system/fvSolution                  #線形ソルバの設定変更
less system/fvSolution                                        #線形ソルバの設定確認
```
``` Command
## GPUオフロード実行用共通設定の確認
less runSharePETSc.sh
```
``` Command
## GPUオフロード実行用ジョブスクリプトの確認
less runPETSc.sh
```
``` Command
## PETScのソルバ実行のジョブ投入
pjsub runPETSc.sh
```
``` Command
## ログの確認
less log.icoFoam-PETSc.*
```
``` Command
## PETScの並列実行用ジョブスクリプトの確認
less runPETSc-par.sh
```
``` Command
## ラッパースクリプトの確認
less wrapper.sh
```
``` Command
## PETScのソルバ並列実行のジョブ投入
pjsub runPETSc-par.sh
```
``` Command
## ログの確認
less log.icoFoam-PETSc-par.*
```
``` Command
## AmgXケースの作成
cd ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/cavity/run #実行用ディレクトリに移動
cp -r cavity100 cavity100-AmgX                                #既存ケースのコピー
cd cavity100-AmgX/                                            #新ケースのディレクトリに移動
rm log.icoFoam*                                               #解析ソルバーのログの消去
rm -r postProcessing/                                         #後処理ディレクトリを消去
cp system/fvSolution.AmgX system/fvSolution                   #線形ソルバの設定変更
less system/fvSolution                                        #線形ソルバの設定確認
```
``` Command
## 圧力線形ソルバに関するAmgXのオプション設定確認
less system/amgxpOptions
```
``` Command
## ソルバ実行のジョブ投入
pjsub runPETSc.sh
```
``` Command
## ログの確認
less log.icoFoam-PETSc.*
```
``` Command
## ソルバ並列実行のジョブ投入
pjsub runPETSc-par.sh
```
``` Command
## ログの確認
less log.icoFoam-PETSc-par*
```
``` Command
## ソルバーのログから計算時間を算出するスクリプトを使用して算出
cd ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/cavity/run
~/lectures/Supercomputer-OpenFOAM-Training/bin/averageExecutionTime.sh */log.icoFoam*
```
``` Command
## 手動での解析ジョブ実行(ここでは実行しない)
pjsub run0mesh.sh
pjstat            #ジョブの完了を確認したら次に進む，以下同様
pjsub run1sol.sh
pjstat
pjsub run2de.sh
```
``` Command
## ステップジョブ実行(ここでは実行しない)
pjsub --step run0mesh.sh 
```
``` Command
## ステップジョブ実行(ここでは実行しない)
pjsub --step --sparam jid=123456 run1sol.sh  #jid=ジョブID．_0(サブジョブID)は不要
pjsub --step --sparam jid=123456 run2de.sh
```
``` Command
## ステップジョブの自動投入
~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/bin/Allrun.batch
# 引数を与えない場合，run[0-9]*.shをステップジョブで投入
# 引数を与えた場合，引数のファイルをステップジョブで投入
```
``` Command
## ステップジョブを展開して表示
pjstat -E
```
``` Command
## チュートリアルケースのコピーとケースディレクトリ移動
cd ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/cavity/run #実行ディレクトリに移動
OFv2106                                                           #OpenFOAMの環境設定
cp -r $FOAM_TUTORIALS/incompressible/simpleFoam/motorBike ./      #motorBikeケースのコピー
cd motorBike/                                                     #ケースディレクトリに移動
```
``` Command
## 領域分割設定ファイルの編集
emacs -nw system/decomposeParDict.6
```
``` Command
## チュートリアル実行用ジョブスクリプトのコピー
cp ~/lectures/Supercomputer-OpenFOAM-Training/Wisteria/bin/foamRunTutorials.sh ./
less foamRunTutorials.sh #内容確認
```
``` Command
## ジョブの投入・ログ確認
pjsub foamRunTutorials.sh
pjstat                     #ジョブが開始されたら次に進む
tail -f *.out log.*        #ログのトレース (*.outはジョブの出力ファイル)
#Endが出てログの更新が止まったら，Ctrl+Cを押して，また上記を実行
#ソルバのログ log.simpleFoam が出てくるまで何度か繰り返す
```
``` Command
## paraviewを起動
paraview &
```
``` Command
## 再実行する場合には以下のように初期化してから再実行を行う
foamCleanTutorials
```
