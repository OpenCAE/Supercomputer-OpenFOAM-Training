#!/bin/sh
#ノード内におけるMPIプロセスのローカルランク
export LOCAL_ID=$OMPI_COMM_WORLD_LOCAL_RANK
#プロセスが使うInfiniBandのポート番号を指定
case $LOCAL_ID in
    0|1) export UCX_NET_DEVICES=mlx5_0:1 ;;
    2|3) export UCX_NET_DEVICES=mlx5_1:1 ;;
    4|5) export UCX_NET_DEVICES=mlx5_2:1 ;;
    6|7) export UCX_NET_DEVICES=mlx5_3:1 ;;
#ローカルランクのGPUのみCUDAから見えるようにする
export CUDA_VISIBLE_DEVICES=$LOCAL_ID
#メモリーが常にローカルノードから割り当てられるように指定
numactl -l $*
