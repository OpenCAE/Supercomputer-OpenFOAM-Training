#!/bin/bash
module load gcc/12.2.0 #GCCのmoduleをload
module load cuda/12.0 #CUDAのmoduleをload
module load ompi-cuda/4.1.6-12.0 #CUDA awareのOpenMPIのmoduleをload
dir=/work/gt00/share/opt/local/x86_64/apps/gcc/12.2.0/cuda/12.0/ompi-cuda/4.1.6-12.0 #インストール先
export AMGX_LIB=$dir/amgx/main/lib #AmgXのライブラリパス
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$AMGX_LIB #ライブラリパスの更新
openfoam_version=v2312 #OpenFOAMのバージョン
openfoam_dir=$dir/openfoam/$openfoam_version #OpenFOAMのインストール先
source $openfoam_dir/OpenFOAM-$openfoam_version/etc/bashrc #OpenFOAMの環境設定
eval $(foamEtcFile -sh -config petsc -- -force) #PETScのライブラリ設定
source $WM_PROJECT_DIR/bin/tools/RunFunctions #実行用関数定義
application=$(getApplication) #アプリケーション名を取得
jid=${PJM_SUBJOBID:-$PJM_JOBID} #サブジョブIDまたジョブIDをjid変数に代入
