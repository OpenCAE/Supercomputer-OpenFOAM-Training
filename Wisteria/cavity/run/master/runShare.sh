#!/bin/bash
module load openfoam/v2112-rist #OpenFOAM-v2112のRIST版moduleをload
source $WM_PROJECT_DIR/etc/bashrc #OpenFOAMの環境設定
source $WM_PROJECT_DIR/bin/tools/RunFunctions #実行用関数定義
application=$(getApplication) #getApplication: controlDictでのapplicationの定義を取得する関数(RunFunctionsで定義される)
jid=${PJM_SUBJOBID:-$PJM_JOBID} #サブジョブIDまたジョブIDをjid変数に代入
