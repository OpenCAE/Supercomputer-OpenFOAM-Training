#!/bin/bash
#PJM -L rg=lecture-o
#PJM -L node=1
#PJM --mpi proc=4
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runShare.sh #共通設定
fipp -C -d./fipp.$jid mpiexec -of log.icoFoam-par.$jid icoFoam -parallel #並列ソルバ実行のプロファイリング
fipp -A -d./fipp.$jid &> fipp.$jid.txt #プロファイリングデータの結果出力
