#!/bin/bash
#PJM -L rg=lecture-a
#PJM -L gpu=4
#PJM --mpi proc=4
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runSharePETSc.sh #共通設定
mpiexec \
--report-bindings --display-map -display-devel-map \
-machinefile $PJM_O_NODEINF -n $PJM_MPI_PROC -npernode 4 \
./wrapper.sh \
icoFoam -lib petscFoam -parallel &> log.icoFoam-PETSc-par.$jid #ソルバ並列実行
#mpiexecに対するオプション
#    --report-bindings --display-map -display-devel-map : プロセス配置などの確認用(必須ではない)
#    -machinefile $PJM_O_NODEINF : 使用するマシンの設定 
#    -n $PJM_MPI_PROC : MPIプロセス数
#    -npernode 4 : ノード毎のMPIプロセス数
#./wrapper.sh : ラッパースクリプト
