#!/bin/bash
#PJM -L rg=lecture-a
#PJM -L gpu=1
#PJM --mpi proc=1
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runSharePETSc.sh #共通設定
icoFoam -lib petscFoam &> log.icoFoam-PETSc.$jid #ソルバ実行
#オプション
#    -lib petscFoam : petscFoamライブラリを使用する
#        なお，system/controlDictに以下の定義があれば上記オプションは不要
#        libs (petscFoam);  
