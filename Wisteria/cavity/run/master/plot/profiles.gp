#!/bin/env gnuplot

set xlabel 'x'
set ylabel 'y'
set x2label 'u'
set y2label 'v'

set xrange [ 0 : 1 ]
set yrange [ 0 : 1 ]
set x2range [ -1 : 1 ]
set y2range [ -1 : 1 ]

set xtics 0.1 nomirror
set mxtics 2
set ytics 0.1 nomirror
set mytics 2
set x2tics 0.2 nomirror
set y2tics 0.2 nomirror

set grid xtics mxtics ytics mytics 

set key outside box height 0.5
set pointsize 0.7

set arrow from 0,0.5 to 1,0.5 nohead
set arrow from 0.5,0 to 0.5,1 nohead

set style data line

set terminal dumb 
plot \
 'plot/u-vel.dat' using 3:2 axes x2y1 title 'Ghia et al., u' with point pt 4\
,'plot/v-vel.dat' using 2:3 axes x1y2 title 'Ghia et al., v' with point pt 6\
,'< cat postProcessing/sample/*/lineY1_U.xy' using 4:2 axes x2y1 title 'case 0, u'\
,'< cat postProcessing/sample/*/lineX1_U.xy' using 1:5 axes x1y2 title 'case 0, v'

set size square
set output "profiles.pdf"
set terminal pdf enhanced color solid font "Times Italic,14" linewidth 1
replot
