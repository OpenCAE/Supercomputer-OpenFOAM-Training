#!/bin/bash
module load gcc/12.2.0 #GCCのmoduleをload
module load cuda/12.0 #CUDAのmoduleをload
module load ompi-cuda/4.1.6-12.0 #CUDA awareのOpenMPIのmoduleをload
dir=/work/gt00/share/opt/local/x86_64/apps/gcc/12.2.0/cuda/12.0/ompi-cuda/4.1.6-12.0 #インストール先
source $dir/rapidcfd/dev/RapidCFD-dev/etc/bashrc #RapidCFDの環境設定
source $WM_PROJECT_DIR/bin/tools/RunFunctions #実行用関数定義
application=$(getApplication) #アプリケーション名を取得
jid=${PJM_SUBJOBID:-$PJM_JOBID} #サブジョブIDまたジョブIDをjid変数に代入
