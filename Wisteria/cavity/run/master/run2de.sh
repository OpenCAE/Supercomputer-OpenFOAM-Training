#!/bin/bash
#PJM -L rg=lecture-o
#PJM -L node=1
#PJM --mpi proc=1
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runShare.sh #共通設定
decomposePar -cellDist &> log.decomposePar.$jid #領域分割
#-cellDist : 格子の領域番号を場cellDistに出力(可視化用．必須ではない)
