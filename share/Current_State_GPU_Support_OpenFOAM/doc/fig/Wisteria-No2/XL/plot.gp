set terminal pdf solid color lw 2 font "Helvetica,14"
set datafile separator ","
set style data linespoints
set xrange [0:*] 
set logscale xy
set grid

set ylabel "Exectution time per step [h] (<Lower is better)" 
set format y "10^{%T}"

set output "consumption.pdf"
set pointsize 0.75
set label "Linear" center at 8,0.55 rotate by 0 font "Courier-Bold,10"
set key bottom left font "Courier-Bold,10" maxrows 5
set xlabel "Token consumption rate [Token/h]" offset 0,-0.5
set xrange [3:192] 
set xtics ("3\n(1GPU)" 3 ,4, "6\n(2GPU)" 6 ,8, "12\n(4GPU)" 12,"24\n(8GPU)" 24,"48\n(16GPU)" 48,"96\n(32GPU)" 96,"192\n(64GPU)" 192)  font "Helvetica,12" 
set yrange [1e-4:1]
plot \
 "< grep -h 'Aquarius.*,.*,PETSc-ICC-CG.aijcusparse.fixedNORM,' time.csv" using ($4*3):($10/3600) \
title "A100-PETSc-ICC"\
,"< grep -h 'Aquarius.*,.*,PETSc-GAMG-CG.aijcusparse.fixedNORM,' time.csv" using ($4*3):($10/3600) \
title "A100-PETSc-GAMG"\
,"< grep -h 'Aquarius.*,.*,PETSc-JACOBI-CG.aijcusparse.fixedNORM,' time.csv" using ($4*3):($10/3600) \
title "A100-PETSc-JACOBI"\
,"< grep -h 'AmgX-PCG_CLASSICAL_V_JACOBI,' time.csv" using ($4*3):($10/3600) \
title "A100-AmgX-AMG"\
,"< grep -h ',RapidCFD-AINV-PCG.fixedNORM.gpuDirectTransfer_0,' time.csv" using ($4*3):($10/3600) \
title "A100-RC-AINV"\
,"< grep -h 'Odyssey,.*,FOAM-DIC-PCG.fixedNORM,' time.csv" using 3:($10/3600) \
title "A64FX-OF-DIC"\
,"< grep -h 'Odyssey,.*,FOAM-GAMG-PCG.fixedNORM,' time.csv" using 3:($10/3600) \
title "A64FX-OF-GAMG"\
,"< grep -h 'Odyssey,.*,PETSc-ICC-CG.mpiaij.fixedNORM,' time.csv" using 3:($10/3600) \
title "A64FX-PETSc-ICC"\
,"< grep -h 'Odyssey,.*,PETSc-AMG-CG.mpiaij.fixedNORM,' time.csv" using 3:($10/3600) \
title "A64FX-PETSc-AMG"\
,"< grep -h 'Odyssey,.*,PETSc-AMG-CG.mpiaij.fixedNORM.caching,' time.csv" using 3:($10/3600) \
title "A64FX-PETSc-AMGc "\
,1/(x/3) title "" with l lt 1 lc 0 lw 0.5\
