#!/bin/bash
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM -S
source ./runShare.sh # 共通設定
surfaceFeatureExtract >& log.surfaceFeatureExtract.$jid # 特徴辺抽出
blockMesh >& log.blockMesh.$jid # ベース格子生成
snappyHexMesh -overwrite >& log.snappyHexMesh.$jid # 並列格子生成
renumberMesh -overwrite >& log.renumberMesh.$jid # 格子順変更による行列バンド幅縮小
checkMesh -constant >& log.checkMesh.$jid # 格子の品質チェック
