# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [997, 924]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [-0.5220000147819519, 0.0, 0.16899999976158142]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-1.5884501432353884, -1.0868768824737896, 1.2944493399631396]
renderView1.CameraFocalPoint = [-0.38794761047096527, 0.1802148786654225, -0.12402553271193649]
renderView1.CameraViewUp = [0.4721586105569554, 0.4201717221459621, 0.7749335264303784]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.5821299383071945
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(997, 924)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'Wavefront OBJ Reader'
ahmedBody_edgeMeshobj = WavefrontOBJReader(registrationName='AhmedBody_edgeMesh.obj', FileName='/work/01/gt00/share/lectures/z30106/Supercomputer-OpenFOAM-Training/AhmedBody/run/case-0/constant/extendedFeatureEdgeMesh/AhmedBody_edgeMesh.obj')

# create a new 'STL Reader'
ahmedBodystl = STLReader(registrationName='AhmedBody.stl', FileNames=['/work/01/gt00/share/lectures/z30106/Supercomputer-OpenFOAM-Training/AhmedBody/run/case-0/constant/triSurface/AhmedBody.stl'])

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from ahmedBodystl
ahmedBodystlDisplay = Show(ahmedBodystl, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'STLSolidLabeling'
sTLSolidLabelingLUT = GetColorTransferFunction('STLSolidLabeling')
sTLSolidLabelingLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 1.5, 0.865003, 0.865003, 0.865003, 3.0, 0.705882, 0.0156863, 0.14902]
sTLSolidLabelingLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
ahmedBodystlDisplay.Representation = 'Surface'
ahmedBodystlDisplay.ColorArrayName = ['CELLS', 'STLSolidLabeling']
ahmedBodystlDisplay.LookupTable = sTLSolidLabelingLUT
ahmedBodystlDisplay.SelectTCoordArray = 'None'
ahmedBodystlDisplay.SelectNormalArray = 'None'
ahmedBodystlDisplay.SelectTangentArray = 'None'
ahmedBodystlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
ahmedBodystlDisplay.SelectOrientationVectors = 'None'
ahmedBodystlDisplay.ScaleFactor = 0.10440000295639039
ahmedBodystlDisplay.SelectScaleArray = 'STLSolidLabeling'
ahmedBodystlDisplay.GlyphType = 'Arrow'
ahmedBodystlDisplay.GlyphTableIndexArray = 'STLSolidLabeling'
ahmedBodystlDisplay.GaussianRadius = 0.0052200001478195195
ahmedBodystlDisplay.SetScaleArray = [None, '']
ahmedBodystlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
ahmedBodystlDisplay.OpacityArray = [None, '']
ahmedBodystlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
ahmedBodystlDisplay.DataAxesGrid = 'GridAxesRepresentation'
ahmedBodystlDisplay.PolarAxes = 'PolarAxesRepresentation'

# show data from ahmedBody_edgeMeshobj
ahmedBody_edgeMeshobjDisplay = Show(ahmedBody_edgeMeshobj, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
ahmedBody_edgeMeshobjDisplay.Representation = 'Surface'
ahmedBody_edgeMeshobjDisplay.ColorArrayName = [None, '']
ahmedBody_edgeMeshobjDisplay.SelectTCoordArray = 'None'
ahmedBody_edgeMeshobjDisplay.SelectNormalArray = 'None'
ahmedBody_edgeMeshobjDisplay.SelectTangentArray = 'None'
ahmedBody_edgeMeshobjDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
ahmedBody_edgeMeshobjDisplay.SelectOrientationVectors = 'None'
ahmedBody_edgeMeshobjDisplay.ScaleFactor = 0.10440000295639039
ahmedBody_edgeMeshobjDisplay.SelectScaleArray = 'None'
ahmedBody_edgeMeshobjDisplay.GlyphType = 'Arrow'
ahmedBody_edgeMeshobjDisplay.GlyphTableIndexArray = 'None'
ahmedBody_edgeMeshobjDisplay.GaussianRadius = 0.0052200001478195195
ahmedBody_edgeMeshobjDisplay.SetScaleArray = [None, '']
ahmedBody_edgeMeshobjDisplay.ScaleTransferFunction = 'PiecewiseFunction'
ahmedBody_edgeMeshobjDisplay.OpacityArray = [None, '']
ahmedBody_edgeMeshobjDisplay.OpacityTransferFunction = 'PiecewiseFunction'
ahmedBody_edgeMeshobjDisplay.DataAxesGrid = 'GridAxesRepresentation'
ahmedBody_edgeMeshobjDisplay.PolarAxes = 'PolarAxesRepresentation'

# setup the color legend parameters for each legend in this view

# get color legend/bar for sTLSolidLabelingLUT in view renderView1
sTLSolidLabelingLUTColorBar = GetScalarBar(sTLSolidLabelingLUT, renderView1)
sTLSolidLabelingLUTColorBar.Title = 'STLSolidLabeling'
sTLSolidLabelingLUTColorBar.ComponentTitle = ''

# set color bar visibility
sTLSolidLabelingLUTColorBar.Visibility = 1

# show color legend
ahmedBodystlDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'STLSolidLabeling'
sTLSolidLabelingPWF = GetOpacityTransferFunction('STLSolidLabeling')
sTLSolidLabelingPWF.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]
sTLSolidLabelingPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(ahmedBody_edgeMeshobj)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')