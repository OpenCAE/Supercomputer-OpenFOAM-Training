set terminal pdf enhanced color solid font "Times,16" size 10cm,10cm
set output "profileUx.pdf"

r=0.03
Uref=40
UxExp(x,U)=x/1000+(U/Uref)*r
UxCFD(x,U)=x+(U/Uref)*r
Z(z)=z/1000

set size square
set pointsize 0.35
set xlabel "{/Times-Italic x} [m]" offset 0,0.5
set ylabel "{/Times-Italic z} [m]" offset 2,0
set key box at -0.1,0.2
set xrange [-0.3:0.2]
set yrange [0.0:0.5]

deg=25
theta=deg*pi/180
set arrow from -0.3,0.05 to 0.0,0.05 nohead lc rgbcolor "blue" lw 2
set arrow from  0.0,0.05 to 0.0,0.338-0.222*sin(theta) nohead lc rgbcolor "blue" lw 2
set arrow from -0.3,0.338 to -0.222*cos(theta),0.338 nohead lc rgbcolor "blue" lw 2
set arrow from -0.222*cos(theta),0.338 to 0.0,0.338-0.222*sin(theta) nohead lc rgbcolor "blue" lw 2

plot \
  "< cat ../exptData/case82/Ahmed-25-yp000-bl.dat ../exptData/case82/Ahmed-25-yp000-xz.dat" \
    using (UxExp($1,$4)):(Z($3)) \
    with points pt 7 lc 1 \
    title "Exp.",\
  "< for file in postProcessing/sample/*/*_U.xy;do cat $file;echo;echo;done" \
    using (UxCFD($1,$4)):3 \
    with line lt 1 lc 0 \
    title "CFD"
