#!/bin/bash
# Installation prefix and versions, directory
prefix=/data/group2/others/opt/local
openfoam_version=v2406
gcc_version=11.3.0
cuda_version=12.1.1
openmpi_version=4.0.5
dir=${prefix}/x86_64/apps/gcc/${gcc_version}/cuda/${cuda_version}/openmpi_cuda/${openmpi_version}

# Load modules
module purge
module load gcc/${gcc_version} cuda/${cuda_version} openmpi_cuda/${openmpi_version}

# Perform environmental settings of OpenFOAM, AmgX, PETSc
source ${dir}/openfoam/${openfoam_version}/OpenFOAM-${openfoam_version}/etc/bashrc # OpenFOAM
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${dir}/amgx/main/lib                     # AmgX  
eval $(foamEtcFile -sh -config petsc -- -force)                                    # PETSc

# Run functions
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

# Get application name
application=$(getApplication)

# Job ID
jid=${PJM_SUBJOBID:-$PJM_JOBID}
