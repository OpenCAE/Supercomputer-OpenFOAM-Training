#!/bin/bash
#PJM -L rscgrp=cx-workshop
#PJM -L node=1
#PJM --mpi proc=40
#PJM -L elapse=0:30:00
#PJM -S
source ./cxShare.sh # 共通設定
mpiexec -n $PJM_MPI_PROC -hostfile $PJM_O_NODEINF -npernode $PJM_PROC_BY_NODE \
snappyHexMesh -overwrite -parallel &> log.snappyHexMesh.$jid # 並列格子生成

mpiexec -n $PJM_MPI_PROC -hostfile $PJM_O_NODEINF -npernode $PJM_PROC_BY_NODE \
renumberMesh -overwrite -parallel &> log.renumberMesh.$jid # 格子順変更による行列バンド幅縮小

mpiexec -n $PJM_MPI_PROC -hostfile $PJM_O_NODEINF -npernode $PJM_PROC_BY_NODE \
checkMesh -constant -parallel &> log.checkMesh.$jid # 格子の品質チェック
