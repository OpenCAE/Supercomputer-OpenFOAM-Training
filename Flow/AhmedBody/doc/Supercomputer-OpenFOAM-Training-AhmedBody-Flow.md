``` CommandLocal
ssh username@flow-fx.cc.nagoya-u.ac.jp
# username は各自のログイン名に置き換える．「-i SSH秘密鍵」の指定は通常不要
```
``` Command
## 講習会レポジトリの複製
# 以下，##と記載の実線枠内のコマンドはType I(flow-fx)のログインノードで実行する
# プロンプトが [username名@flow-fx* directory名]$ である事を確認してから実行する
# 講習会作業用ディレクトリを作成 (mkdir: make directories)
mkdir lectures
# 講習会作業用ディレクトリへ移動 (cd: change the current directory to dir)
cd lectures/
# 講習会レポジトリの複製
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
# レポジトリのディレクトリへ移動
cd Supercomputer-OpenFOAM-Training/
# 本講習会のブランチへの切り替え
git checkout Nagoya-University-Information-Technology-Center-20240911
# ブランチ名の表示
git branch
```
``` CommandOutput
* Nagoya-University-Information-Technology-Center-20240911
  master
```
``` Command
## pjstatのテスト実行
pjstat
pjstat -E
pjstat -H
pjstat --limit
```
``` Command
## カスタマイズ・コマンドpjstat2のテスト実行
pjstat2 --rsc
```
``` CommandOutput
#リソースグループ [有効・無効,起動・停止] 割り当てノード数:ノード3次元構成
RSCGRP               STATUS                       NODE
fx-interactive       [ENABLE,START]        768:4x12x16
fx-small             [ENABLE,START]        768:4x12x16
fx-workshop          [ENABLE,START]        768:4x12x16 #講習会用リソースグループ
fx-debug             [ENABLE,START]        768:4x12x16
fx-middle            [ENABLE,START]       1536:8x12x16
fx-middle2           [ENABLE,START]       1536:8x12x16
fx-large             [ENABLE,START]       1536:8x12x16
fx-xlarge            [ENABLE,START]       1536:8x12x16
fx-special           [ENABLE,START]      2304:12x12x16
```
``` Command
## バッチキューの詳細構成を見る
pjstat2 --rsc -x
```
``` CommandOutput
#リソースグループ [有効・無効,起動・停止] 最小ノード数 最大ノード数 経過時間制限値 ノード毎最大メモリ
RSCGRP               STATUS             MIN_NODE       MAX_NODE MAX_ELAPSE MEM(GB)
fx-interactive       [ENABLE,START]            1              4   24:00:00      28
fx-small             [ENABLE,START]            1             24  168:00:00      28
fx-workshop          [ENABLE,START]           12             12   00:30:00      28
fx-debug             [ENABLE,START]            1             36   01:00:00      28
fx-middle            [ENABLE,START]           12             96   72:00:00      28
fx-middle2           [ENABLE,START]            1             96   72:00:00      28
fx-large             [ENABLE,START]           96            192   72:00:00      28
fx-xlarge            [ENABLE,START]           96            768   24:00:00      28
fx-special           [ENABLE,START]            1           2304  unlimited      28
```
``` Command
## 投げられているジョブ数を見る
pjstat2 --rsc -b
```
``` CommandOutput
#リソースグループ [有効・無効,起動・停止] ジョブ数              割り当てノード数:ノード3次元構成
RSCGRP               STATUS           TOTAL RUNNING QUEUED  HOLD OTHER           NODE
fx-interactive       [ENABLE,START]       0       0      0     0     0    768:4x12x16
fx-small             [ENABLE,START]      60      60      0     0     0    768:4x12x16
fx-workshop          [ENABLE,START]       0       0      0     0     0    768:4x12x16
fx-debug             [ENABLE,START]       0       0      0     0     0    768:4x12x16
fx-middle            [ENABLE,START]       4       4      0     0     0   1536:8x12x16
fx-middle2           [ENABLE,START]       0       0      0     0     0   1536:8x12x16
fx-large             [ENABLE,START]       2       2      0     0     0   1536:8x12x16
fx-xlarge            [ENABLE,START]       0       0      0     0     0   1536:8x12x16
fx-special           [ENABLE,START]       0       0      0     0     0  2304:12x12x16
```
``` Command
## リソースグループのノード使用率
pjstat2 --use
```
``` CommandOutput
#リソースグループ                        ノード使用率    使用ノード数  割り当てノード数
RSCGRP                                                  Used nodes/  Total nodes
fx-debug groups (total 768 nodes)                               91/          768
   fx-interactive    -------------------------    0%             0
   fx-small          ***----------------------   12%            91
   fx-workshop       -------------------------    0%             0
   fx-debug          -------------------------    0%             0
fx groups (total 1536 nodes)                                   348/         1536
   fx-middle         **-----------------------    6%            92
   fx-middle2        -------------------------    0%             0
   fx-large          *****--------------------   17%           256
   fx-xlarge         -------------------------    0%             0
   fx-special           -------------------------    0%             0/         2304
```
``` Command
## 実行ディレクトリに移動
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/AhmedBody/run
```
``` Command
## 作成済の解析ケースのコピー
# デフォルトの実行ケースを作成
cp -r master/ case-0
# ケースディレクトリに移動 
cd case-0/
```
``` Command
## ベース格子生成ジョブの投入
pjsub run1base.sh
```
``` Command
## ジョブの状況確認
pjstat
```
``` Command
## ログファイルのトレース(ジョブの実行中，または，実行後)
tail -f log.*
```
``` Command
## ジョブのエラー出力の確認
less run1base.sh.*.err
# 本来はJOBIDが付くが，*のワイルドカードで全ての文字にマッチする
# ls -al で上記のファイルのサイズが0である事を確認するのでも良い
```
``` Command
## ジョブの標準出力ファイルの確認
less run1base.sh.*.out
```
``` Command
## ログ確認
less log.surfaceFeatureExtract.*
less log.blockMesh.*
less log.decomposePar.*
```
``` Command
##@lm01 ParaViewによるOpenFOAMデータ可視化用ダミーファイル作成
# ##@lm01と記載の実線枠内のコマンドはNICE DCV系由でログインした可視化用ノードlm01で実行
# ケースディレクトリに移動
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/AhmedBody/run/case-0/
# ダミーファイル(拡張子は.foam．ファイル名は任意)を作成
touch pv.foam
```
``` Command
##@lm01 paraviewをバックグラウンドジョブで起動
/opt/ParaView/5.11.1-python3.9/bin/paraview &
```
``` Command
## 格子生成ジョブの投入
pjsub run2mesh.sh
```
``` Command
## ジョブの状況確認
pjstat
```
``` Command
## ログファイルのトレース
tail -f log.*
```
``` Command
## ジョブのエラー出力と標準出力ファイルの確認
less run2mesh.sh.*.err
less run2mesh.sh.*.out
```
``` Command
## snappyHexMeshのログ確認
less log.snappyHexMesh.*
```
``` Command
## checkMeshのログ確認
less log.checkMesh.*
```
``` Command
## コピー元の初期値場の確認
ls 0.orig/
# 0.orig/にコピーする初期値場を置いておく
```
``` Command
## 初期値場コピー
pjsub run3init.sh
pjstat # ジョブが終了したら次に進む
```
``` Command
## 並列計算用ディレクリにおける初期値場の確認
ls processor*/0/
# 0.orig/にあった初期値場が，全並列計算用ディレクリ(processor0-575)にコピーされていることを確認する
```
``` Command
## 流体解析実行
pjsub run4solve.sh
```
``` Command
##@lm01 関数出力のモニター
module load gcc/4.8.5 impi/2019.7.217 openfoam/v2006 # OpenFOAM-v2006等のmoduleをload
source $WM_PROJECT_DIR/etc/bashrc                    # OpenFOAMの環境設定
foamMonitor -y "[-1:1]" -r 1 postProcessing/forceCoeffs/0/coefficient.dat
# -y : 縦軸のレンジ指定． -r :更新秒
# "File postProcessing/forceCoeffs/0/coefficient.dat does not exit" → 暫く待って再実行
```
``` Command
## 空力係数の関数出力の表示
less postProcessing/forceCoeffs/0/coefficient.dat
#末尾に飛ぶにはG(大文字のg)を押す
```
``` Command
##@lm01 プロット実行
gnuplot plot/forceCoeffs.gp
gnuplot plot/forces.gp
gnuplot plot/profileUx.gp
gnuplot plot/residual.gp
```
``` Command
##@lm01 プロットファイル閲覧
evince *.pdf &
# 他のPDFファイルが閲覧できるコマンド例: firefox, gs
```
``` Command
## 空力解析演習実行例
cd ..                              # 実行ディレクトリrunに移動
cp -r master/ case-1               # コピー先のディレクトリ名は任意
cd case-1/                         # ケースディレクトリに移動
git add .                          # 変更箇所確認を容易にするためにgitで管理する
nano system/snappyHexMeshDict      # 設定変更(エディタは任意．例: AhmedBodyのnSurfaceLayersの値を5から4に変更)
nano constant/turbulenceProperties # 設定変更(エディタは任意．例: RASModelをkOmegaSSTに変更)
git diff                           # 変更箇所を確認する
../../../bin/Allrun.batch          # 全ジョブを実行
pjstat -E                          # -E: ステップジョブ内のサブジョブを展開して表示
tail -f log.*                      # 実行中のジョブのログトレース
./Allrun.plot                      # plotディレクトリ以下のgnuplotファイルを全てプロット
```
``` Command
##@lm01 プロット閲覧
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/AhmedBody/run/case-1/  
evince *.pdf
```
``` Command
## 解析時間の集計結果表示
cd .. # 実行ディレクトリrunに移動
~/lectures/Supercomputer-OpenFOAM-Training/bin/averageExecutionTime.sh */log.simpleFoam*
```
``` CommandOutput
#Filename,TimeSteps[-],InitTime[s],LastTime[s],Time[s],AveTime[s]
case-0/log.simpleFoam.xxxxxx,500,xxx,xx.xx,xxx,AveTime0
case-1/log.simpleFoam.xxxxxx,500,xxx,xx.xx,xxx,AveTime1
case-X/log.simpleFoam.xxxxx,500,xxx,xx.xx,xxx,AveTimeX
```
``` CommandLocal
##@新規端末 Type IIサブシステムのログインノード(flow-cx)にログイン
ssh username@flow-cx.cc.nagoya-u.ac.jp
# username は各自のログイン名に置き換える
```
``` Command
##@flow-cx RapidCFD moduleのロードとヘルプ表示
# 以下，##@flow-cxと記載の実線枠内のコマンドはType II(flow-cx)のログインノードで実行する
# プロンプトが [username名@flow-cx* directory名]$ である事を確認してから実行する
# 依存moduleのロード
module load gcc/8.4.0
module load cuda/11.8.0
module load openmpi_cuda/4.0.5
# RapidCFD moduleのロード
module load rapidcfd/dev
# RapidCFD moduleのヘルプ表示
module help rapidcfd/dev
```
``` CommandOutput
(1) Make a job script
    [username@flow-node ~]$ vi run.sh
    #!/bin/sh

    #PJM -L rscgrp=cx-debug
    #PJM -L node=1
    #PJM -L elapse=0:10:00
    #PJM --mpi proc=4
    #PJM -j

    module load gcc/8.4.0
    module load cuda/11.x.x
    module load openmpi_cuda/4.x.x
    module load rapidcfd/dev

    source ${WM_PROJECT_DIR}/etc/bashrc
    source ${WM_PROJECT_DIR}/bin/tools/RunFunctions

    application=$(getApplication)

    mpiexec \
        -display-map -display-devel-map \
        -n $PJM_MPI_PROC -machinefile $PJM_O_NODEINF -npernode 4 \
        $application -devices '( 0 1 2 3 )' -parallel >& log.$application
```
``` Command
##@flow-cx ケースの複製
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/AhmedBody/run #実行用ディレクトリに移動
cp -r master case-cx0  #既存ケースのコピー
cd case-cx0            #新ケースのディレクトリに移動
cp system/cx/* system/ #線形ソルバの設定変更
less system/fvSolution #線形ソルバの設定確認
```
``` Command
##@flow-cx 圧力線形ソルバに関するAmgXのオプション設定確認
less system/amgxpOptions
```
``` Command
##@flow-cx GPUオフロード実行用ジョブスクリプトの確認
less cxShare.sh
```
``` Command
##@flow-cx ジョブ投入
../../../bin/Allrun.batch cx[0-9]*.sh
```
``` Command
##@flow-cx ジョブ確認やログのトレース(cx4solve.shのジョブが開始されるまで繰り返す)
pjstat -E     # -E: ステップジョブ内のサブジョブを展開して表示
tail -f log.* # ログのトレース．表示が止まったらCtrl-Cで中止
```
``` Command
##@lm01 関数出力のモニターとプロット
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/AhmedBody/run/case-cx0/
foamMonitor -y "[-1:1]" -r 1 postProcessing/forceCoeffs/0/coefficient.dat &
# 計算終了後(反復回数500回)に以下を実行
./Allrun.plot # プロット
evince *.pdf  # プロット閲覧
```
``` Command
##@flow-cx 解析時間の比較
cd ..         # 実行ディレクトリrunに移動
~/lectures/Supercomputer-OpenFOAM-Training/bin/averageExecutionTime.sh */log.simpleFoam*
```
``` CommandOutput
#Filename,TimeSteps[-],InitTime[s],LastTime[s],Time[s],AveTime[s]
case-0/log.simpleFoam.*,500,2.6,89.84,87.24,0.17483
case-1/log.simpleFoam.*,500,2.5,95.98,93.48,0.187335
case-cx0/log.simpleFoam-cx.*,500,8.52,633.97,625.45,1.25341
```
