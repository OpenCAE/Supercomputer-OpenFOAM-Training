#!/bin/bash
module purge                                      # moduleの初期化
module load gcc/11.3.0                            # GCCのmoduleをload
module load cuda/12.1.1                           # CUDAのmoduleをload
module load openmpi_cuda/4.0.5                    # CUDA awareのOpenMPIのmoduleをload
dir=/data/group2/others/opt/local/x86_64/apps/gcc/11.3.0/cuda/12.1.1/openmpi_cuda/4.0.5 #インストール先
amgx_dir=$dir/amgx/main                           # AmgXのインストール先
export AMGX_LIB=$amgx_dir/lib                     # AmgXのライブラリパス
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$AMGX_LIB # ライブラリパスの更新
openfoam_version=v2312                            # OpenFOAMのバージョン
openfoam_dir=$dir/openfoam/$openfoam_version      # OpenFOAMのインストール先
source $openfoam_dir/OpenFOAM-$openfoam_version/etc/bashrc # OpenFOAMの環境設定
eval $(foamEtcFile -sh -config petsc -- -force)   # PETScのライブラリ設定
source $WM_PROJECT_DIR/bin/tools/RunFunctions     # 実行用関数定義
application=$(getApplication)                     # アプリケーション名を取得
jid=${PJM_SUBJOBID:-$PJM_JOBID}                   # サブジョブIDまたジョブIDをjid変数に代入
