#!/bin/bash
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM --mpi proc=4
#PJM -L elapse=0:30:00
#PJM -S
source ./runShare.sh              # 共通設定
# 並列ソルバ実行のプロファイリング
fipp -C -d./fipp.$jid \
mpiexec -of log.icoFoam-par.$jid icoFoam -parallel
fipp -A -d./fipp.$jid &> fipp.$jid.txt
