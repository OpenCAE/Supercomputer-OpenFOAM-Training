#!/bin/bash
#PJM -L rscunit=cx
#PJM -L rscgrp=cx-debug
#PJM -L node=1
#PJM --mpi proc=4
#PJM -L elapse=0:30:00
#PJM -S
source ./runShareRapidCFD.sh # 共通設定
mpiexec \
--report-bindings --display-map -display-devel-map \
-machinefile $PJM_O_NODEINF -n $PJM_MPI_PROC -npernode 4 \
icoFoam -devices "(0 1 2 3)" -parallel &> log.icoFoam-RapidCFD-par.$jid # ソルバ並列実行
# --report-bindings --display-map -display-devel-mapのオプションは必須ではない
