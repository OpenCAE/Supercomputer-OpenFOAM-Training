#!/bin/bash
module purge                                   # moduleの初期化
module load gcc/8.4.0                          # GCCのmoduleをload
module load cuda/11.8.0                        # CUDAのmoduleをload
module load openmpi_cuda/4.0.5                 # CUDA awareのOpenMPIのmoduleをload
module load rapidcfd/dev                       # RapidCFD/devのmoduleをload
source $WM_PROJECT_DIR/etc/bashrc              # RapidCFDの環境設定
source $WM_PROJECT_DIR/bin/tools/RunFunctions  # 実行用関数定義
application=$(getApplication)                  # アプリケーション名を取得
jid=${PJM_SUBJOBID:-$PJM_JOBID}                # サブジョブIDまたジョブIDをjid変数に代入
