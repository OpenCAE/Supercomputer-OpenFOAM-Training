#!/bin/bash
#PJM -L rscunit=cx
#PJM -L rscgrp=cx-debug
#PJM -L node=1
#PJM --mpi proc=4
#PJM -L elapse=0:30:00
#PJM -S
source ./runSharePETSc.sh # 共通設定
mpiexec \
--report-bindings --display-map -display-devel-map \
-machinefile $PJM_O_NODEINF -n $PJM_MPI_PROC -npernode 4 \
./wrapper.sh \
icoFoam -lib petscFoam -parallel &> log.icoFoam-PETSc-par.$jid # ソルバ並列実行
#
# '--report-bindings --display-map -display-devel-map'のオプションは必須ではない
#
# ./wrapper.sh
#     ノード内におけるMPIプロセスのローカルランクをGPUのIDとするラッパースクリプト
#
# system/controlDictに以下の定義があれば，'-lib petscFoam'のオプションは不要
#     libs (petscFoam);  
