#!/bin/bash
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM -S
source ./runShare.sh              # 共通設定
# 領域分割
# -cellDist : 格子の領域番号を場cellDistに出力(可視化用．必須ではない)
decomposePar -cellDist &> log.decomposePar.$jid
