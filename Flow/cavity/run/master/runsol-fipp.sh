#!/bin/bash
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM -S
source ./runShare.sh              # 共通設定
# ソルバ実行のプロファイリング
fipp -C -d./fipp.$jid \
icoFoam &> log.icoFoam.$jid
fipp -A -d./fipp.$jid &> fipp.$jid.txt
