#!/bin/bash
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM --mpi proc=4
#PJM -L elapse=0:30:00
#PJM -S
source ./runShare.sh                                 # 共通設定
mpiexec -of log.icoFoam-par.$jid icoFoam -parallel   # ソルバ並列実行
# mpiexec : FJ MPIのMPI並列実行コマンド
# mpiexecに対するオプション
#   -of ファイル : 標準出力と標準エラー出力をファイルに追加出力(上書き保存ではない)
# OpenFOAMのアプリケーション(ここではicoFoam)に対するオプション
#   -parallel : 並列実行
