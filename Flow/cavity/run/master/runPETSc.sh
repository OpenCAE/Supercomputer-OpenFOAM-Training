#!/bin/bash
#PJM -L rscunit=cx
#PJM -L rscgrp=cx-share
#PJM -L elapse=0:30:00
#PJM -S
source ./runSharePETSc.sh # 共通設定
icoFoam -lib petscFoam &> log.icoFoam-PETSc.$jid # ソルバ実行
# system/controlDictに以下の定義があれば，'-lib petscFoam'のオプションは不要
#   libs (petscFoam);  
