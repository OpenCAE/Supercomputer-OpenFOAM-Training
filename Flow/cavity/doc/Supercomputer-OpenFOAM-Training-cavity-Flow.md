``` CommandLocal
ssh username@flow-fx.cc.nagoya-u.ac.jp
# username は各自のログイン名に置き換える．「-i SSH秘密鍵」の指定は通常不要
```
``` Command
## 講習会レポジトリの複製
# 以下，##と記載の実線枠内のコマンドはType I(flow-fx)のログインノードで実行する
# プロンプトが [username名@flow-fx* directory名]$ である事を確認してから実行する
# 講習会作業用ディレクトリを作成 (mkdir: make directories)
mkdir lectures
# 講習会作業用ディレクトリへ移動 (cd: change the current directory to dir)
cd lectures/
# 講習会レポジトリの複製
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
# レポジトリのディレクトリへ移動
cd Supercomputer-OpenFOAM-Training/
# 本講習会のブランチへの切り替え
git checkout Nagoya-University-Information-Technology-Center-20240515
# ブランチ名の表示
git branch
```
``` Command
## pjstatのテスト実行
pjstat
pjstat -E
pjstat -H
pjstat --limit
```
``` Command
## カスタマイズ・コマンドpjstat2のテスト実行
pjstat2 --rsc
```
``` Command
## バッチキューの詳細構成を見る
pjstat2 --rsc -x
```
``` Command
## 投げられているジョブ数を見る
pjstat2 --rsc -b
```
``` Command
## リソースグループのノード使用率
pjstat2 --use
```
``` Command
## 作業ディレクトリへの移動
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/cavity/run
```
``` Command
## 演習ケースのコピー
# cp : コピー(copy)コマンド
#   -r : 再帰的コピー
cp -r master cavity
```
``` Command
## コピーの確認(カレントディレトリの内容確認)
ls -l
```
``` Command
## ケースディレクトリへの移動
# cd の後にTABを押すと，cd cavity/ まで補完される．なお，cd cavtity でも良い．
cd cavity/
```
``` Command
## ファイル構成の確認
# tree : ディレクトリ構造を樹木状に表示(標準コマンドではないが，本スパコンにはインストール済み)
tree
# 主なファイルのみを示す
```
``` Command
less system/blockMeshDict
```
``` Command
## ジョブスクリプトの確認
less run0mesh.sh
```
``` Command
## 共通設定スクリプトの確認
less runShare.sh
```
``` Command
## 投げられているジョブ数を見る(混雑確認．必須ではない)
pjstat2 --rsc -b
```
``` Command
## ノードの利用状況の表示(混雑確認．必須ではない)
pjstat2 --use
```
``` Command
## 格子生成のジョブ投入
pjsub run0mesh.sh
```
``` Command
## ジョブの状況確認
pjstat
# 以降の演習ではジョブ状態の確認を適宜行うこと
```
``` Command
tree | less
```
``` Command
## ジョブの出力ファイルを確認
less run0mesh.sh.* # *のワイルドカードで全ての文字にマッチする
# 特にジョブの標準エラー出力 *.err にエラーが出ていないことを確認
```
``` Command
less log.blockMesh.*
```
``` Command
##@lm01 ParaViewの起動
# 以下，##@lm01と記載の実線枠内のコマンドはNICE DCV系由でログインした可視化用ノードlm01で実行する
# 実行ディレクトリへの移動
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/cavity/run/cavity/
# ParaViewによるOpenFOAMデータ可視化用ダミーファイル作成
# 中身は何でも良いので，touchコマンドで空ファイルを作成する
touch pv.foam
# paraviewをバックグラウンドジョブで起動
/opt/ParaView/5.9.1-python3.8/bin/paraview &
```
``` Command
## ファイル構成の確認
tree | less
```
``` Command
## ファイルの確認
less 0/U
```
``` Command
## ファイルの確認
less 0/p
```
``` Command
## ファイルの確認
less constant/transportProperties
```
``` Command
less system/fvSchemes
```
``` Command
less system/fvSolution
```
``` Command
less system/controlDict
```
``` Command
## 実行制御の設定ファイルの編集
emacs -nw system/controlDict
# もちろん，以下の例のように，各自の好みのエディタで編集して構わない
# vi system/controlDict
# nano system/controlDict
```
``` Command
## ジョブスクリプトの確認
less run1sol.sh
```
``` Command
## ソルバ実行のジョブ投入
pjsub run1sol.sh
```
``` Command
## ジョブ確認
pjstat
```
``` Command
## ファイルの確認
tree | less
```
``` Command
## ログの確認
less log.icoFoam.*
```
``` Command
##@lm01初期残差のプロット
/home/center/opt/aarch64/apps/tcsds/1.2.31/openfoam/v2106/OpenFOAM-v2106/bin/foamMonitor -l -r 1 postProcessing/solverInfo/0/solverInfo.dat & # 1行コマンド
# -l : 縦軸ログスケール． -r 1 : 更新間隔1秒(デフォルト10秒)．foamMonitorのパスを直接指定
```
``` Command
## 既往計算ケースの複製
cd ..                              #親ディレクトリ(run)に移動
cp -r cavity cavity100             #ケースのコピー
cd cavity100/                      #新ケースのディレクトリに移動
tree                               #適宜ケース内のファイルを確認
rm log.* *.err *.out *.stats       #実行結果ファイルを消去
rm -r 0.*/ postProcessing/         #実行結果ディレクトリを消去
tree                               #適宜ケース内のファイルを確認
```
``` Command
## 格子生成設定ファイルの編集
emacs -nw system/blockMeshDict
```
``` Command
## 実行制御の設定ファイルの編集
emacs -nw system/controlDict
```
``` Command
## ジョブ投入
pjsub run0mesh.sh # pjstatでジョブの終了を確認してから次に進む
pjsub run1sol.sh
```
``` Command
##@lm01 初期残差のプロット
cd ../cavity100
/home/center/opt/aarch64/apps/tcsds/1.2.31/openfoam/v2106/OpenFOAM-v2106/bin/foamMonitor -l -r 1 postProcessing/solverInfo/0/solverInfo.dat & # 1行コマンド
```
``` Command
## サンプリング設定ファイルの内容確認
less system/sample
```
``` Command
## ジョブスクリプトの確認
less runpost.sh
```
``` Command
## サンプリングの実行
pjsub runpost.sh
pjstat #ジョブの終了を確認してから次に進む
```
``` Command
## サンプリング結果確認
less postProcessing/sample/15/lineX1_U.xy
```
``` Command
## プロットファイルの確認
less plot/profiles.gp
```
``` Command
## gnuplot実行(キャラクターでの表示もされる)
gnuplot plot/profiles.gp
```
``` Command
##@lm01 プロットファイル表示
evince profiles.pdf # evinceの代わりにfirefoxやgsでも良い
```
``` Command
## 領域分割設定ファイルの編集
emacs -nw system/decomposeParDict
```
``` Command
less run2de.sh
```
``` Command
## 領域分割ジョブの投入
pjsub run2de.sh
pjstat # ジョブの完了を確認して，次に進む
```
``` Command
less log.decomposePar.*
```
``` Command
## 領域分割で生成されたファイルの確認
tree | less
```
``` Command
##@lm01 paraviewを起動
/opt/ParaView/5.9.1-python3.8/bin/paraview &
```
``` Command
less run3sol-par.sh
```
``` Command
pjsub run3sol-par.sh
pjstat #ジョブ開始後，次に進む
less log.icoFoam-par.*
```
``` Command
less run4re.sh
```
``` Command
## 領域再構築ジョブの投入
pjsub run4re.sh
pjstat # ジョブの完了を確認して，次に進む
```
``` Command
## 並列計算結果と再構築結果の確認
tree | less
```
``` CommandLocal
## Type IIサブシステムのログインノード(flow-cx)にログイン
ssh username@flow-cx.cc.nagoya-u.ac.jp
# username は各自のログイン名に置き換える
```
``` Command
##@flow-cx RapidCFD moduleのロードとヘルプ表示
# 以下，##@flow-cxと記載の実線枠内のコマンドはType II(flow-cx)のログインノードで実行する
# プロンプトが [username名@flow-cx* directory名]$ である事を確認してから実行する
# 依存moduleのロード
module load gcc/8.4.0
module load cuda/11.8.0
module load openmpi_cuda/4.0.5
# RapidCFD moduleのロード
module load rapidcfd/dev
# RapidCFD moduleのヘルプ表示
module help rapidcfd/dev
```
``` Command
##@flow-cx ケースの複製
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/cavity/run #実行用ディレクトリに移動
cp -r cavity100 cavity100-PETSc                               #既存ケースのコピー
cd cavity100-PETSc/                                           #新ケースのディレクトリに移動
rm -r postProcessing/                                         #後処理ディレクトリを消去
cp system/fvSolution.PETSc system/fvSolution                  #線形ソルバの設定変更
less system/fvSolution                                        #線形ソルバの設定確認
```
``` Command
##@flow-cx GPUオフロード実行用共通設定の確認
less runSharePETSc.sh
```
``` Command
##@flow-cx GPUオフロード実行用ジョブスクリプトの確認
less runPETSc.sh
```
``` Command
##@flow-cx PETScのソルバ実行のジョブ投入
pjsub runPETSc.sh
```
``` Command
##@flow-cx ログのトレース
tail -f log.icoFoam-PETSc.*
```
``` Command
##@flow-cx PETScの並列実行用ジョブスクリプトの確認
less runPETSc-par.sh
```
``` Command
##@flow-cx ラッパースクリプトの確認
less wrapper.sh
```
``` Command
##@flow-cx PETScのソルバ並列実行のジョブ投入
pjsub runPETSc-par.sh
```
``` Command
##@flow-cx ログのトレース
tail -f log.icoFoam-PETSc-par.*
```
``` Command
##@flow-cx ケースの複製
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/cavity/run #実行用ディレクトリに移動
cp -r cavity100 cavity100-AmgX                                #既存ケースのコピー
cd cavity100-AmgX/                                            #新ケースのディレクトリに移動
rm -r postProcessing/                                         #後処理ディレクトリを消去
cp system/fvSolution.AmgX system/fvSolution                   #線形ソルバの設定変更
less system/fvSolution                                        #線形ソルバの設定確認
```
``` Command
##@flow-cx 圧力線形ソルバに関するAmgXのオプション設定確認
less system/amgxpOptions
```
``` Command
##@flow-cx ソルバ実行のジョブ投入
pjsub runPETSc.sh
```
``` Command
##@flow-cx ログのトレース
tail -f log.icoFoam-PETSc.*
```
``` Command
##@flow-cx ソルバ並列実行のジョブ投入
pjsub runPETSc-par.sh
```
``` Command
##@flow-cx ログのトレース
tail -f log.icoFoam-PETSc-par*
```
``` Command
##@flow-cx ソルバーのログからt(n)を算出するスクリプトを使用して算出
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/cavity/run
~/lectures/Supercomputer-OpenFOAM-Training/bin/averageExecutionTime.sh */log.icoFoam*
```
``` Command
## ソルバーのログからt(n)を算出するスクリプトを使用して算出
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/cavity/run/cavity100/
~/lectures/Supercomputer-OpenFOAM-Training/bin/averageExecutionTime.sh log.icoFoam*
```
``` Command
## 手動での解析ジョブ実行(ここでは実行しない)
pjsub run0mesh.sh
pjstat            #ジョブの完了を確認したら次に進む，以下同様
pjsub run1sol.sh
pjstat
pjsub run2de.sh
```
``` Command
## ステップジョブ実行(ここでは実行しない)
pjsub --step run0mesh.sh 
```
``` Command
## ステップジョブ実行(ここでは実行しない)
pjsub --step --sparam jid=123456 run1sol.sh  #jid=ジョブID．_0(サブジョブID)は不要
pjsub --step --sparam jid=123456 run2de.sh
```
``` Command
## ステップジョブの自動投入
~/lectures/Supercomputer-OpenFOAM-Training/Flow/bin/Allrun.batch
# 引数を与えない場合，run[0-9]*.shをステップジョブで投入
# 引数を与えた場合，引数のファイルをステップジョブで投入
```
``` Command
## ステップジョブを展開して表示
pjstat -E
```
``` Command
## チュートリアルケースのコピーとケース移動
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/cavity/run #実行ディレクトリに移動
cp -r /home/center/opt/aarch64/apps/tcsds/1.2.31/openfoam/v2106/OpenFOAM-v2106/tutorials/incompressible/simpleFoam/motorBike ./ #一行コマンド
cd motorBike/
```
``` Command
## 領域分割設定ファイルの編集
emacs -nw system/decomposeParDict.6
```
``` Command
## チュートリアル実行用ジョブスクリプトのコピー
cp ~/lectures/Supercomputer-OpenFOAM-Training/Flow/bin/foamRunTutorials.sh ./
```
``` Command
## ジョブの投入・ログ確認
pjsub foamRunTutorials.sh
pjstat                     #ジョブが開始されたら次に進む
tail -f log.* *.out        #ログのトレース (*.outはジョブの出力ファイル)
#Endが出てログの更新が止まったら，Ctrl+Cを押して，また上記を実行
#ソルバのログ log.simpleFoam が出てくるまで何度か繰り返す
```
``` Command
##[可視化用ノードlm01の端末で実行] paraviewを起動
cd ../motorBike/
paraview &
```
``` Command
## 再実行する場合には以下のように初期化してから再実行を行う
foamCleanTutorials
```
