## TeX engine
TEX = lualatex

## BiBTeX
BIBTEX = pbibtex

## Pandoc
PANDOC = pandoc

## awk
AWK = awk

## Ghostscript
GS = gs

## Remove command
RM= \rm -f

## Copy command
CP= \cp

##  Filename
TITLE = Supercomputer-OpenFOAM-Training-cavity-Flow

## Style file
STY = \
../../../share/doc/OFmacros.sty \
../../../share/doc/LatexMacros.sty

SRC = $(TITLE).tex
BBL = $(TITLE).bbl
AUX = $(TITLE).aux
LOG = $(TITLE).log
BLG = $(TITLE).blg
OUT = $(TITLE).out
TOC = $(TITLE).toc
MD = $(TITLE).md
PDF = $(TITLE).pdf
CPDF =Compressed-$(PDF)
BIB = ../../../share/doc/Supercomputer-OpenFOAM-Training.bib
INC = \
../../../share/doc/copyright.tex \
*.tex \
../../share/doc/*.tex \
../../share/doc/fig/*/* \
../../../share/doc/*.tex \
../../../share/doc/fig/*/* \
../../../share/cavity/doc/*.tex \
../../../share/cavity/doc/fig/*/* \
../../../share/cavity/doc/fig/*/*/* \
../../../share/cavity/doc/fig/*/*/*/* \
../run/*/*

CHANGED = grep '^LaTeX Warning: Label(s) may have changed.' $(LOG)

all: $(PDF) $(MD)

compress: $(CPDF)

$(CPDF): $(PDF)
	$(GS) -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$(CPDF) $(PDF)

$(PDF): $(SRC) $(BBL) $(STY) $(INC)
	$(TEX) $(SRC)
	(while $(CHANGED); do $(TEX) $(SRC); done)

$(BBL): $(SRC) $(BIB)
	$(TEX) $(SRC)
	$(BIBTEX) $(TITLE)

$(MD): $(SRC) $(INC)

distclean:
	$(RM) $(LOG) $(AUX) $(LOG) $(BLG) $(OUT) $(TOC) $(BBL)

clean: distclean
	$(RM) $(PDF)

%.md: %.tex
	$(PANDOC) $< -t commonmark 2> /dev/null | $(AWK) '/^``` Command(()|Local)$$/,/^```$$/' > $@
