# OpenFOAM
alias OFv2106='module load openfoam/v2106;source $WM_PROJECT_DIR/etc/bashrc'
alias OFv2106p='source /home/center/opt/aarch64/apps/tcsds/1.2.31/openfoam/v2106/OpenFOAM-v2106/etc/bashrc'
# POV-Ray
alias Povray='module unload gcc;module load intel/2019.5.281;module load povray/3.7.0.8'
# FFmpeg
alias Ffmpeg='module unload intel;module load gcc/4.8.5;module load ffmpeg/4.2.3'
# ParaView
export PATH=$PATH:/opt/ParaView/5.9.1-python3.8/bin
# Supercomputer OpenFOAM Training
export PATH=$PATH:$HOME/lectures/Supercomputer-OpenFOAM-Training/bin
