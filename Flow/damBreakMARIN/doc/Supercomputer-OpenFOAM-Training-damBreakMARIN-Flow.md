``` Command
## 講習会レポジトリの複製
# 以下，##と記載の実線枠内のコマンドはType I(flow-fx)のログインノードで実行する
# プロンプトが [username名@flow-fx* directory名]$ である事を確認してから実行する
# 講習会作業用ディレクトリを作成 (mkdir: make directories)
mkdir opt
# 講習会作業用ディレクトリへ移動 (cd: change the current directory to dir)
cd opt/
# 講習会レポジトリの複製   
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
# レポジトリのディレクトリへ移動
cd Supercomputer-OpenFOAM-Training/
# 本講習会のブランチへの切り替え
git checkout Nagoya-University-Information-Technology-Center-20250122
# ブランチ名の表示
git branch
```
``` Command
## pjstatのテスト実行
pjstat
pjstat -E
pjstat -H
pjstat --limit
```
``` Command
## カスタマイズ・コマンドpjstat2のテスト実行
pjstat2 --rsc
```
``` Command
## バッチキューの詳細構成を見る
pjstat2 --rsc -x
```
``` Command
## 投げられているジョブ数を見る
pjstat2 --rsc -b
```
``` Command
## リソースグループのノード使用率
pjstat2 --use
```
``` Command
## 作成済の解析ケースのコピー
cd ~/opt/Supercomputer-OpenFOAM-Training/Flow/damBreak-MARIN/run #実行用ディレクトリ移動
cp -r master case-0 # デフォルトの実行ケースを作成
cd case-0 # ケースディレクトリに移動 
```
``` Command
## ベース格子生成・領域分割のジョブ投入
pjsub run1base.sh
```
``` Command
## ジョブの状況確認
pjstat
```
``` Command
## blockMeshのログ確認
less log.blockMesh.*
# *の場所には本来JOB IDを打つ必要があるが，ワイルドカードの*でマッチする
# スペース等で進め，q で終了
```
``` Command
## decomposeParのログの確認(領域分割のジョブ終了後)
less log.decomposePar.*
# [less] Maxの検索
/Max
# [less]一行戻る
C-p # カーソル↑やvi的なコマンド(j，C-j, y, C-y)でも良い(hによるヘルプ参照)
```
``` Command
##@cx ParaViewによるOpenFOAMデータ可視化用ダミーファイル作成
# ケースディレクトリに移動
cd ~/opt/Supercomputer-OpenFOAM-Training/Flow/damBreakMARIN/run/case-0/
# ダミーファイル(拡張子は.foam．ファイル名は任意)を作成
touch pv.foam
```
``` Command
##@cx paraviewをバックグラウンドジョブで起動
paraview &
```
``` Command
## 格子生成のジョブ投入
pjsub run2mesh.sh
tail -f log.* # ログのモニター(いつでもCtrl+Cで終了して良い)
```
``` Command
## snappyHexMeshのログ確認
less log.snappyHexMesh.*
```
``` Command
## renumberMeshのログ確認
less log.renumberMesh.*
```
``` Command
## checkMeshのログ確認
less log.checkMesh.*
```
``` Command
paraview pv.foam &
```
``` Command
## 初期値設定ジョブの投入
pjsub run3init.sh
```
``` Command
## OpenFOAMの設定ファイル例の表示と検索
tree $FOAM_ETC/caseDicts | less # /probes でprobesを検索する．適宜qで終了
```
``` Command
## 流体解析実行
pjsub run4sol.sh
```
``` Command
##@cx OpenFOAMの環境設定(既に実行している場合には不要)
module load gcc/11.3.0 openmpi/4.1.5 openfoam/v2212;source ${WM_PROJECT_DIR}/etc/bashrc
```
``` Command
##@cx 圧力時系列のモニター
foamMonitor -r 1 postProcessing/probes/0/p & # -r :更新秒(デフォルト10)
```
``` Command
##@cx gnuplotによるプロット
gnuplot pressure.gp
```
``` Command
##@cx firefoxによるプロット結果閲覧(firefoxで閲覧すると図をタブで切り替えられて便利)
firefox *.pdf &
```
``` Command
##@cx
paraview pv.foam &
```
``` Command
##@cx ParaViewの起動
paraview postProcessing/isoSurface/2/isoSurface.vtp &
```
``` Command
##@cx 全時刻に対する可視化画像の作成
./Allrun.animImage
```
``` Command
##@cx 動画の作成
./Allrun.anim
```
``` Command
##@cx 動画の再生
firefox anim/*.gif &
```
``` Command
## 領域分割された格子と計算結果の再構築ジョブの投入
pjsub run5re.sh
```
``` Command
## interIsoFoamを用いた解析ケースの作成
cd ~/opt/Supercomputer-OpenFOAM-Training/Flow/damBreak-MARIN/run #実行用ディレクトリ移動
cp -r master case-Iso
cd case-Iso # ケースディレクトリに移動
```
``` Command
## 実行アプリケーション名の変更(エディタはvi, emacs, nano等何でも良い)
vi system/controlDict
```
``` Command
## 液相率のVOF解法設定の変更
vi system/fvSolution
```
``` Command
## ステップジョブ実行
../../../bin/Allrun.batch # 全ジョブを実行
pjstat -E # -E: ステップジョブ内のサブジョブを展開して表示
```
``` Command
##@cx 圧力時系列のモニター
cd ~/opt/Supercomputer-OpenFOAM-Training/Flow/damBreak-MARIN/run/case-Iso
foamMonitor -r 1 postProcessing/probes/0/p &
```
``` Command
##@cx 基本ケースとの圧力時系列の比較プロット
gnuplot comparison-pressure.gp
```
``` Command
##@cx firefoxによるプロット結果閲覧
firefox comparison-pressure*.pdf &
```
``` Command
## 解適合格子ケースの作成
cd ~/opt/Supercomputer-OpenFOAM-Training/Flow/damBreak-MARIN/run #実行用ディレクトリ移動
cp -r master case-AMR
cd case-AMR # ケースディレクトリに移動
```
``` Command
## 動的格子設定の変更
vi constant/dynamicMeshDict
```
``` Command
## ステップジョブ実行
../../../bin/Allrun.batch
```
``` Command
##@cx 圧力時系列のモニター
cd ~/opt/Supercomputer-OpenFOAM-Training/Flow/damBreak-MARIN/run/case-AMR
foamMonitor -r 1 postProcessing/probes/0/p &
```
``` Command
##@cx 基本ケースとの圧力時系列の比較プロット
gnuplot comparison-pressure.gp
```
``` Command
##@cx firefoxによるプロット結果閲覧
firefox comparison-pressure*.pdf &
```
``` Command
## 基本ケースのコピー・設定変更
cd ~/opt/Supercomputer-OpenFOAM-Training/Flow/damBreakMARIN/run/
cp -r master/ case-XX #XXは任意
cd case-XX/ #その後，エディタで設定変更
```
``` Command
## ステップジョブの投入・サブジョブの確認
../../../bin/Allrun.batch
pjstat -E # -E: ステップジョブのサブジョブを展開して表示．run4sol.sh実行後次に進む
```
``` Command
##@cx 比較プロット
cd ~/opt/Supercomputer-OpenFOAM-Training/Flow/damBreakMARIN/run/case-XX
foamMonitor -r 1 postProcessing/probes/0/p &
gnuplot comparison-pressure.gp
firefox comparison-pressure*.pdf &
```
``` Command
## interFoamのチュートリアルで使用されている速度Uの移流項スキームを調べる
# grep -r: ディレクトリ検索
grep -r "div(rhoPhi,U)" $FOAM_TUTORIALS/multiphase | less
# 出力略
```
``` Command
## interFoamのチュートリアルで使用されている速度Uの移流項スキームをユニークに表示
# grep -h: ファイル名を非表示
# tr -s "[:blank:]": 空白文字の繰り返しを1文字に置換
# sort | uniq: ソートして重複排除
grep -rh "div(rhoPhi,U)" $FOAM_TUTORIALS/multiphase | tr -s "[:blank:]" | sort | uniq
```
``` Command
##@cx RapidCFD moduleのロードとヘルプ表示
# moduleの初期化(既にロードしているopenfoamの依存moduleとの衝突を防ぐ)
module purge
# 依存moduleのロード
module load gcc/8.4.0
module load cuda/11.8.0
module load openmpi_cuda/4.0.5
# RapidCFD moduleのロード
module load rapidcfd/dev
# RapidCFD moduleのヘルプ表示
module help rapidcfd/dev
```
``` Command
##@cx ケースの複製
cd ~/opt/Supercomputer-OpenFOAM-Training/Flow/damBreakMARIN/run #実行用ディレクトリ移動
cp -r master case-cx0  #既存ケースのコピー
cd case-cx0            #新ケースのディレクトリに移動
cp system/cx/* system/ #線形ソルバの設定変更
less system/fvSolution #線形ソルバの設定確認
```
``` Command
##@cx 圧力線形ソルバに関するAmgXのオプション設定確認
less system/amgxp_rghOptions
```
``` Command
##@cx GPUオフロード実行用ジョブスクリプトの確認
less cxShare.sh
```
``` Command
##@cx ジョブ投入
../../../bin/Allrun.batch cx[0-9]*.sh
```
``` Command
##@cx ジョブ確認やログのトレース(cx4solve.shのジョブが開始されるまで繰り返す)
pjstat -E     # -E: ステップジョブ内のサブジョブを展開して表示
tail -f log.* # ログのトレース．表示が止まったらCtrl-Cで中止
```
``` Command
##@cx ジョブ統計情報ファイルにおけるGPU情報の表示
less cx4sol.sh.*.stats #
# [less] gpu文字列の検索
/gpu
```
``` Command
##@cx 関数出力のモニターとプロット
foamMonitor -r 1 postProcessing/probes/0/p &
# 計算終了後に以下を実行
gnuplot pressure.gp # プロット
firefox *.pdf  # プロット閲覧
```
``` Command
##@cx 解析時間の比較
cd ..         # 実行ディレクトリrunに移動
~/opt/Supercomputer-OpenFOAM-Training/bin/averageExecutionTime.sh */log.interFoam*
```
