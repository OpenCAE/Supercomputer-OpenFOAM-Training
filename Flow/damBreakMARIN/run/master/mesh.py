# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [927, 489]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [1.61000001430511, 0.0, 0.499999996873555]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-0.271758686641025, -2.7724400175163, 2.76899733692044]
renderView1.CameraFocalPoint = [2.65338709222251, 1.79815250973452, -1.31912008615726]
renderView1.CameraViewUp = [0.22027131639954403, 0.5681965861895442, 0.7928639143094861]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 1.75843681978879

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(927, 489)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.CaseType = 'Decomposed Case'
pvfoam.MeshRegions = ['box_maxX', 'box_maxY', 'box_maxZ', 'box_minX', 'box_minY', 'defaultFaces', 'internalMesh', 'region_maxX', 'region_maxY', 'region_maxZ', 'region_minX', 'region_minY', 'region_minZ']
pvfoam.Readzones = 1

# create a new 'Extract Block'
extractBlock2 = ExtractBlock(registrationName='ExtractBlock2', Input=pvfoam)
extractBlock2.BlockIndices = [15]

# create a new 'Extract Block'
extractBlock1 = ExtractBlock(registrationName='ExtractBlock1', Input=pvfoam)
extractBlock1.BlockIndices = [12, 11, 10, 9, 7, 6, 4, 13]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from extractBlock1
extractBlock1Display = Show(extractBlock1, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'vtkBlockColors'
vtkBlockColorsLUT = GetColorTransferFunction('vtkBlockColors')
vtkBlockColorsLUT.InterpretValuesAsCategories = 1
vtkBlockColorsLUT.AnnotationsInitialized = 1
vtkBlockColorsLUT.Annotations = ['0', '0', '1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '7', '7', '8', '8', '9', '9', '10', '10', '11', '11']
vtkBlockColorsLUT.ActiveAnnotatedValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11']
vtkBlockColorsLUT.IndexedColors = [1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.63, 0.63, 1.0, 0.67, 0.5, 0.33, 1.0, 0.5, 0.75, 0.53, 0.35, 0.7, 1.0, 0.75, 0.5]

# trace defaults for the display properties.
extractBlock1Display.Representation = 'Surface With Edges'
extractBlock1Display.ColorArrayName = ['FIELD', 'vtkBlockColors']
extractBlock1Display.LookupTable = vtkBlockColorsLUT
extractBlock1Display.SelectTCoordArray = 'None'
extractBlock1Display.SelectNormalArray = 'None'
extractBlock1Display.SelectTangentArray = 'None'
extractBlock1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractBlock1Display.SelectOrientationVectors = 'None'
extractBlock1Display.ScaleFactor = 0.322000002861023
extractBlock1Display.SelectScaleArray = 'None'
extractBlock1Display.GlyphType = 'Arrow'
extractBlock1Display.GlyphTableIndexArray = 'None'
extractBlock1Display.GaussianRadius = 0.016100000143051147
extractBlock1Display.SetScaleArray = [None, '']
extractBlock1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractBlock1Display.OpacityArray = [None, '']
extractBlock1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractBlock1Display.DataAxesGrid = 'GridAxesRepresentation'
extractBlock1Display.PolarAxes = 'PolarAxesRepresentation'

# show data from extractBlock2
extractBlock2Display = Show(extractBlock2, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
extractBlock2Display.Representation = 'Wireframe'
extractBlock2Display.ColorArrayName = [None, '']
extractBlock2Display.SelectTCoordArray = 'None'
extractBlock2Display.SelectNormalArray = 'None'
extractBlock2Display.SelectTangentArray = 'None'
extractBlock2Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractBlock2Display.SelectOrientationVectors = 'None'
extractBlock2Display.ScaleFactor = 0.322000002861023
extractBlock2Display.SelectScaleArray = 'None'
extractBlock2Display.GlyphType = 'Arrow'
extractBlock2Display.GlyphTableIndexArray = 'None'
extractBlock2Display.GaussianRadius = 0.016100000143051147
extractBlock2Display.SetScaleArray = [None, '']
extractBlock2Display.ScaleTransferFunction = 'PiecewiseFunction'
extractBlock2Display.OpacityArray = [None, '']
extractBlock2Display.OpacityTransferFunction = 'PiecewiseFunction'
extractBlock2Display.DataAxesGrid = 'GridAxesRepresentation'
extractBlock2Display.PolarAxes = 'PolarAxesRepresentation'
extractBlock2Display.ScalarOpacityUnitDistance = 0.1588596196012523
extractBlock2Display.OpacityArrayName = ['CELLS', 'CellId']
extractBlock2Display.ExtractedBlockIndex = 1

# setup the color legend parameters for each legend in this view

# get color legend/bar for vtkBlockColorsLUT in view renderView1
vtkBlockColorsLUTColorBar = GetScalarBar(vtkBlockColorsLUT, renderView1)
vtkBlockColorsLUTColorBar.Title = 'vtkBlockColors'
vtkBlockColorsLUTColorBar.ComponentTitle = ''

# set color bar visibility
vtkBlockColorsLUTColorBar.Visibility = 1

# show color legend
extractBlock1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'vtkBlockColors'
vtkBlockColorsPWF = GetOpacityTransferFunction('vtkBlockColors')

# ----------------------------------------------------------------
# restore active source
SetActiveSource(extractBlock2)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
