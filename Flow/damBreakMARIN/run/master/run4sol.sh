#!/bin/bash
#PJM -L rg=fx-workshop
#PJM -L node=1
#PJM --mpi proc=48
#PJM -L elapse=0:30:00
#PJM -S
source ./runShare.sh # 共通設定
mpiexec -of log.$application.$jid $application -parallel # 並列流体解析
