#!/bin/bash
#PJM -L rg=fx-workshop
#PJM -L node=1
#PJM --mpi proc=1
#PJM -L elapse=0:30:00
#PJM -S
source ./runShare.sh # 共通設定
reconstructParMesh -constant &> log.reconstructParMesh.$jid # 格子の再構築
reconstructPar &> log.reconstructPar.$jid # 解析結果の再構築
