# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1280, 720]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [1.61000001430511, 0.0, 0.499999996873555]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-0.271758686641025, -2.7724400175163, 2.76899733692044]
renderView1.CameraFocalPoint = [2.65338709222251, 1.79815250973452, -1.31912008615726]
renderView1.CameraViewUp = [0.22027131639954403, 0.5681965861895442, 0.7928639143094861]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 1.75843681978879

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(927, 489)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'XML PolyData Reader'
isoSurfacevtp = XMLPolyDataReader(registrationName='isoSurface.vtp', FileName=['postProcessing/isoSurface/0.01/isoSurface.vtp'])
isoSurfacevtp.PointArrayStatus = ['U']

# create a new 'STL Reader'
regionstl = STLReader(registrationName='region.stl', FileNames=['constant/triSurface/region.stl'])

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(registrationName='AnnotateTimeFilter1', Input=isoSurfacevtp)
annotateTimeFilter1.Format = "Time: %.2fs"

# create a new 'STL Reader'
boxstl = STLReader(registrationName='box.stl', FileNames=['constant/triSurface/box.stl'])

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from isoSurfacevtp
isoSurfacevtpDisplay = Show(isoSurfacevtp, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
isoSurfacevtpDisplay.Representation = 'Surface'
isoSurfacevtpDisplay.AmbientColor = [0.6666666666666666, 1.0, 1.0]
isoSurfacevtpDisplay.ColorArrayName = ['POINTS', '']
isoSurfacevtpDisplay.DiffuseColor = [0.6666666666666666, 1.0, 1.0]
isoSurfacevtpDisplay.SelectTCoordArray = 'None'
isoSurfacevtpDisplay.SelectNormalArray = 'None'
isoSurfacevtpDisplay.SelectTangentArray = 'None'
isoSurfacevtpDisplay.OSPRayScaleArray = 'U'
isoSurfacevtpDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
isoSurfacevtpDisplay.SelectOrientationVectors = 'None'
isoSurfacevtpDisplay.ScaleFactor = 0.12288299798965455
isoSurfacevtpDisplay.SelectScaleArray = 'None'
isoSurfacevtpDisplay.GlyphType = 'Arrow'
isoSurfacevtpDisplay.GlyphTableIndexArray = 'None'
isoSurfacevtpDisplay.GaussianRadius = 0.006144149899482727
isoSurfacevtpDisplay.SetScaleArray = ['POINTS', 'U']
isoSurfacevtpDisplay.ScaleTransferFunction = 'PiecewiseFunction'
isoSurfacevtpDisplay.OpacityArray = ['POINTS', 'U']
isoSurfacevtpDisplay.OpacityTransferFunction = 'PiecewiseFunction'
isoSurfacevtpDisplay.DataAxesGrid = 'GridAxesRepresentation'
isoSurfacevtpDisplay.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
isoSurfacevtpDisplay.ScaleTransferFunction.Points = [-0.6935991644859314, 0.0, 0.5, 0.0, 0.5435415506362915, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
isoSurfacevtpDisplay.OpacityTransferFunction.Points = [-0.6935991644859314, 0.0, 0.5, 0.0, 0.5435415506362915, 1.0, 0.5, 0.0]

# show data from boxstl
boxstlDisplay = Show(boxstl, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
boxstlDisplay.Representation = 'Feature Edges'
boxstlDisplay.ColorArrayName = ['POINTS', '']
boxstlDisplay.SelectTCoordArray = 'None'
boxstlDisplay.SelectNormalArray = 'None'
boxstlDisplay.SelectTangentArray = 'None'
boxstlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
boxstlDisplay.SelectOrientationVectors = 'None'
boxstlDisplay.ScaleFactor = 0.04029999971389771
boxstlDisplay.SelectScaleArray = 'STLSolidLabeling'
boxstlDisplay.GlyphType = 'Arrow'
boxstlDisplay.GlyphTableIndexArray = 'STLSolidLabeling'
boxstlDisplay.GaussianRadius = 0.0020149999856948855
boxstlDisplay.SetScaleArray = [None, '']
boxstlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
boxstlDisplay.OpacityArray = [None, '']
boxstlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
boxstlDisplay.DataAxesGrid = 'GridAxesRepresentation'
boxstlDisplay.PolarAxes = 'PolarAxesRepresentation'

# show data from regionstl
regionstlDisplay = Show(regionstl, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
regionstlDisplay.Representation = 'Feature Edges'
regionstlDisplay.ColorArrayName = ['POINTS', '']
regionstlDisplay.SelectTCoordArray = 'None'
regionstlDisplay.SelectNormalArray = 'None'
regionstlDisplay.SelectTangentArray = 'None'
regionstlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
regionstlDisplay.SelectOrientationVectors = 'None'
regionstlDisplay.ScaleFactor = 0.322000002861023
regionstlDisplay.SelectScaleArray = 'STLSolidLabeling'
regionstlDisplay.GlyphType = 'Arrow'
regionstlDisplay.GlyphTableIndexArray = 'STLSolidLabeling'
regionstlDisplay.GaussianRadius = 0.016100000143051147
regionstlDisplay.SetScaleArray = [None, '']
regionstlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
regionstlDisplay.OpacityArray = [None, '']
regionstlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
regionstlDisplay.DataAxesGrid = 'GridAxesRepresentation'
regionstlDisplay.PolarAxes = 'PolarAxesRepresentation'

# show data from annotateTimeFilter1
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
annotateTimeFilter1Display.FontSize = 36

# ----------------------------------------------------------------
# restore active source
SetActiveSource(isoSurfacevtp)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
