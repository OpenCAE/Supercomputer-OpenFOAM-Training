#!/bin/sh
module load openfoam/v2106                     # OpenFOAM-v2106のmoduleをload
source $WM_PROJECT_DIR/etc/bashrc              # OpenFOAMの環境設定
source $WM_PROJECT_DIR/bin/tools/RunFunctions  # 実行用関数定義
application=$(getApplication)                  # アプリケーション名を取得
jid=${PJM_SUBJOBID:-$PJM_JOBID}                # サブジョブIDまたジョブIDをjid変数に代入
