#!/bin/bash
#PJM -L rg=cx-workshop
#PJM -L node=1
#PJM --mpi proc=40
#PJM -L elapse=0:30:00
#PJM -S
source ./cxShare.sh # 共通設定
mpiexec -n $PJM_MPI_PROC -hostfile $PJM_O_NODEINF -npernode $PJM_PROC_BY_NODE \
numactl -l $application -lib petscFoam -parallel &> log.$application-cx.$jid  # 並列流体解析
