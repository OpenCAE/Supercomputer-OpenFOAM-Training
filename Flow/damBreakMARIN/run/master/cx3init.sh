#!/bin/bash
#PJM -L rg=cx-workshop
#PJM -L node=1
#PJM --mpi proc=40
#PJM -L elapse=0:30:00
#PJM -S
source ./cxShare.sh # 共通設定
restore0Dir -processor &> log.restore0Dir.$jid # 初期値場を並列計算用ディレクトリにコピー
mpiexec -n $PJM_MPI_PROC -hostfile $PJM_O_NODEINF -npernode $PJM_PROC_BY_NODE \
setFields -parallel &> log.setFields.$jid # 初期分布作成
