#!/bin/bash
#PJM -L rg=cx-workshop
#PJM -L node=1
#PJM --mpi proc=1
#PJM -L elapse=0:30:00
#PJM -S
source ./cxShare.sh # 共通設定
blockMesh &> log.blockMesh.$jid # ベース格子生成
decomposePar -cellDist &> log.decomposePar.$jid # 領域分割
# -cellDist: 領域分割の可視化の場cellDistを出力する(必須ではない)
