#!/bin/bash
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM --mpi proc=48
#PJM -S
module load openfoam/v2106               # OpenFOAMのmoduleをload
source ${WM_PROJECT_DIR}/etc/bashrc      # OpenFOAMの環境設定
# foamRunTutorialsコマンドでチュートリアルを実行
foamRunTutorials
