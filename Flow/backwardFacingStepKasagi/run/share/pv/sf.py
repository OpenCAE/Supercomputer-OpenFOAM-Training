# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1035, 509]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [6.5, 1.5, 1.0]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [4.495862858931384, 1.9719182104713096, 34.34893012759968]
renderView1.CameraFocalPoint = [4.495862858931384, 1.9719182104713096, 1.0]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 2.4657085563147842
renderView1.CameraParallelProjection = 1
renderView1.OSPRayMaterialLibrary = materialLibrary1

# init the 'GridAxes3DActor' selected for 'AxesGrid'
renderView1.AxesGrid.Visibility = 1
renderView1.AxesGrid.XTitle = 'x/H'
renderView1.AxesGrid.YTitle = 'y/H'
renderView1.AxesGrid.ZTitle = 'z/H'

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1035, 509)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.MeshRegions = ['internalMesh']
pvfoam.CellArrays = ['U', 'epsilon', 'k', 'nut', 'p', 'yPlus']
pvfoam.PointArrays = ['streamFunction']

# create a new 'Slice'
slice1 = Slice(registrationName='Slice1', Input=pvfoam)
slice1.SliceType = 'Plane'
slice1.HyperTreeGridSlicer = 'Plane'
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [6.5, 1.5, 1.0]
slice1.SliceType.Normal = [0.0, 0.0, 1.0]

# init the 'Plane' selected for 'HyperTreeGridSlicer'
slice1.HyperTreeGridSlicer.Origin = [6.5, 1.5, 1.0]

# create a new 'Contour'
contour2 = Contour(registrationName='Contour2', Input=slice1)
contour2.ContourBy = ['POINTS', 'streamFunction']
contour2.Isosurfaces = [0.0, 0.09999999999999999, 0.19999999999999998, 0.3, 0.39999999999999997, 0.49999999999999994, 0.6, 0.7, 0.7999999999999999, 0.8999999999999999, 0.9999999999999999, 1.0999999999999999, 1.2, 1.2999999999999998, 1.4, 1.4999999999999998, 1.5999999999999999, 1.7]
contour2.PointMergeMethod = 'Uniform Binning'

# create a new 'Contour'
contour1 = Contour(registrationName='Contour1', Input=slice1)
contour1.ContourBy = ['POINTS', 'streamFunction']
contour1.Isosurfaces = [-0.07, -0.065, -0.060000000000000005, -0.05500000000000001, -0.05, -0.045000000000000005, -0.04000000000000001, -0.035, -0.030000000000000006, -0.02500000000000001, -0.020000000000000004, -0.015000000000000006, -0.010000000000000009, -0.0050000000000000044]
contour1.PointMergeMethod = 'Uniform Binning'

# create a new 'Feature Edges'
featureEdges1 = FeatureEdges(registrationName='FeatureEdges1', Input=slice1)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from slice1
slice1Display = Show(slice1, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')
uLUT.AutomaticRescaleRangeMode = 'Never'
uLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.5000000000000001, 0.865003, 0.865003, 0.865003, 1.0, 0.705882, 0.0156863, 0.14902]
uLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'
slice1Display.ColorArrayName = ['POINTS', 'U']
slice1Display.LookupTable = uLUT
slice1Display.SelectTCoordArray = 'None'
slice1Display.SelectNormalArray = 'None'
slice1Display.SelectTangentArray = 'None'
slice1Display.OSPRayScaleArray = 'p'
slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice1Display.SelectOrientationVectors = 'U'
slice1Display.ScaleFactor = 1.7000000000000002
slice1Display.SelectScaleArray = 'p'
slice1Display.GlyphType = 'Arrow'
slice1Display.GlyphTableIndexArray = 'p'
slice1Display.GaussianRadius = 0.085
slice1Display.SetScaleArray = ['POINTS', 'p']
slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
slice1Display.OpacityArray = ['POINTS', 'p']
slice1Display.OpacityTransferFunction = 'PiecewiseFunction'
slice1Display.DataAxesGrid = 'GridAxesRepresentation'
slice1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
slice1Display.ScaleTransferFunction.Points = [-0.17207124829292297, 0.0, 0.5, 0.0, 0.00023394700838252902, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
slice1Display.OpacityTransferFunction.Points = [-0.17207124829292297, 0.0, 0.5, 0.0, 0.00023394700838252902, 1.0, 0.5, 0.0]

# show data from contour1
contour1Display = Show(contour1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
contour1Display.Representation = 'Surface'
contour1Display.AmbientColor = [0.0, 0.0, 0.0]
contour1Display.ColorArrayName = ['POINTS', '']
contour1Display.DiffuseColor = [0.0, 0.0, 0.0]
contour1Display.SelectTCoordArray = 'None'
contour1Display.SelectNormalArray = 'None'
contour1Display.SelectTangentArray = 'None'
contour1Display.OSPRayScaleArray = 'streamFunction'
contour1Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour1Display.SelectOrientationVectors = 'U'
contour1Display.ScaleFactor = 0.4427691906690598
contour1Display.SelectScaleArray = 'streamFunction'
contour1Display.GlyphType = 'Arrow'
contour1Display.GlyphTableIndexArray = 'streamFunction'
contour1Display.GaussianRadius = 0.02213845953345299
contour1Display.SetScaleArray = ['POINTS', 'streamFunction']
contour1Display.ScaleTransferFunction = 'PiecewiseFunction'
contour1Display.OpacityArray = ['POINTS', 'streamFunction']
contour1Display.OpacityTransferFunction = 'PiecewiseFunction'
contour1Display.DataAxesGrid = 'GridAxesRepresentation'
contour1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour1Display.ScaleTransferFunction.Points = [-0.07000000029802322, 0.0, 0.5, 0.0, -0.004999999888241291, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour1Display.OpacityTransferFunction.Points = [-0.07000000029802322, 0.0, 0.5, 0.0, -0.004999999888241291, 1.0, 0.5, 0.0]

# show data from contour2
contour2Display = Show(contour2, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
contour2Display.Representation = 'Surface'
contour2Display.ColorArrayName = ['POINTS', '']
contour2Display.SelectTCoordArray = 'None'
contour2Display.SelectNormalArray = 'None'
contour2Display.SelectTangentArray = 'None'
contour2Display.OSPRayScaleArray = 'streamFunction'
contour2Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour2Display.SelectOrientationVectors = 'U'
contour2Display.ScaleFactor = 1.7000000000000002
contour2Display.SelectScaleArray = 'streamFunction'
contour2Display.GlyphType = 'Arrow'
contour2Display.GlyphTableIndexArray = 'streamFunction'
contour2Display.GaussianRadius = 0.085
contour2Display.SetScaleArray = ['POINTS', 'streamFunction']
contour2Display.ScaleTransferFunction = 'PiecewiseFunction'
contour2Display.OpacityArray = ['POINTS', 'streamFunction']
contour2Display.OpacityTransferFunction = 'PiecewiseFunction'
contour2Display.DataAxesGrid = 'GridAxesRepresentation'
contour2Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour2Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.7000000476837158, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour2Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.7000000476837158, 1.0, 0.5, 0.0]

# show data from featureEdges1
featureEdges1Display = Show(featureEdges1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
featureEdges1Display.Representation = 'Surface'
featureEdges1Display.ColorArrayName = ['POINTS', '']
featureEdges1Display.LineWidth = 2.0
featureEdges1Display.SelectTCoordArray = 'None'
featureEdges1Display.SelectNormalArray = 'None'
featureEdges1Display.SelectTangentArray = 'None'
featureEdges1Display.OSPRayScaleArray = 'p'
featureEdges1Display.OSPRayScaleFunction = 'PiecewiseFunction'
featureEdges1Display.SelectOrientationVectors = 'U'
featureEdges1Display.ScaleFactor = 1.7000000000000002
featureEdges1Display.SelectScaleArray = 'p'
featureEdges1Display.GlyphType = 'Arrow'
featureEdges1Display.GlyphTableIndexArray = 'p'
featureEdges1Display.GaussianRadius = 0.085
featureEdges1Display.SetScaleArray = ['POINTS', 'p']
featureEdges1Display.ScaleTransferFunction = 'PiecewiseFunction'
featureEdges1Display.OpacityArray = ['POINTS', 'p']
featureEdges1Display.OpacityTransferFunction = 'PiecewiseFunction'
featureEdges1Display.DataAxesGrid = 'GridAxesRepresentation'
featureEdges1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
featureEdges1Display.ScaleTransferFunction.Points = [-0.17149949073791504, 0.0, 0.5, 0.0, 0.00023394700838252902, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
featureEdges1Display.OpacityTransferFunction.Points = [-0.17149949073791504, 0.0, 0.5, 0.0, 0.00023394700838252902, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for uLUT in view renderView1
uLUTColorBar = GetScalarBar(uLUT, renderView1)
uLUTColorBar.Orientation = 'Horizontal'
uLUTColorBar.WindowLocation = 'AnyLocation'
uLUTColorBar.Position = [0.05043478260869587, 0.8147740667976423]
uLUTColorBar.Title = 'U'
uLUTColorBar.ComponentTitle = 'Magnitude'
uLUTColorBar.ScalarBarLength = 0.3299999999999995

# set color bar visibility
uLUTColorBar.Visibility = 1

# show color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')
uPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(featureEdges1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
