# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1035, 509]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [6.5, 1.5, 1.0]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [6.5, 1.5, 17.83633739832113]
renderView1.CameraFocalPoint = [6.5, 1.5, -1.1141827255906422]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 4.269213302928584
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1035, 509)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.MeshRegions = ['internalMesh']
pvfoam.CellArrays = ['U', 'epsilon', 'k', 'nut', 'p', 'yPlus']
pvfoam.PointArrays = ['streamFunction']

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'cellDist'
cellDistLUT = GetColorTransferFunction('cellDist')
cellDistLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 23.5, 0.865003, 0.865003, 0.865003, 47.0, 0.705882, 0.0156863, 0.14902]
cellDistLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'cellDist'
cellDistPWF = GetOpacityTransferFunction('cellDist')
cellDistPWF.Points = [0.0, 0.0, 0.5, 0.0, 47.0, 1.0, 0.5, 0.0]
cellDistPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Surface With Edges'
pvfoamDisplay.ColorArrayName = ['CELLS', 'cellDist']
pvfoamDisplay.LookupTable = cellDistLUT
pvfoamDisplay.SelectTCoordArray = 'None'
pvfoamDisplay.SelectNormalArray = 'None'
pvfoamDisplay.SelectTangentArray = 'None'
pvfoamDisplay.OSPRayScaleArray = 'p'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'U'
pvfoamDisplay.ScaleFactor = 1.7000000000000002
pvfoamDisplay.SelectScaleArray = 'p'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'p'
pvfoamDisplay.GaussianRadius = 0.085
pvfoamDisplay.SetScaleArray = ['POINTS', 'p']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = ['POINTS', 'p']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
pvfoamDisplay.ScalarOpacityFunction = cellDistPWF
pvfoamDisplay.ScalarOpacityUnitDistance = 1.18725713113989
pvfoamDisplay.OpacityArrayName = ['POINTS', 'p']
pvfoamDisplay.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
pvfoamDisplay.ScaleTransferFunction.Points = [-0.17207124829292297, 0.0, 0.5, 0.0, 0.00023394700838252902, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
pvfoamDisplay.OpacityTransferFunction.Points = [-0.17207124829292297, 0.0, 0.5, 0.0, 0.00023394700838252902, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for cellDistLUT in view renderView1
cellDistLUTColorBar = GetScalarBar(cellDistLUT, renderView1)
cellDistLUTColorBar.Orientation = 'Horizontal'
cellDistLUTColorBar.WindowLocation = 'AnyLocation'
cellDistLUTColorBar.Position = [0.3518840579710146, 0.7440471512770137]
cellDistLUTColorBar.Title = 'cellDist'
cellDistLUTColorBar.ComponentTitle = ''
cellDistLUTColorBar.ScalarBarLength = 0.3299999999999998

# set color bar visibility
cellDistLUTColorBar.Visibility = 1

# show color legend
pvfoamDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
