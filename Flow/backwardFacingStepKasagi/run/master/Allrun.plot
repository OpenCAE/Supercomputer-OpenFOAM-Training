#!/bin/sh
# 実験結果との誤差算出
./Allrun.objective

# plotディレクトリにある全.gpファイルをgnuplotで処理してプロット
for file in plot/*.gp
do
    gnuplot $file
done

# プロットファイルをまとめる
convert -density 300 residual.eps profileU.eps profileV.eps profilek.eps ${PWD##*/}.pdf
