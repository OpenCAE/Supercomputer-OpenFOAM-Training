#!/bin/sh
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM --mpi proc=48
#PJM -S
source ./runShare.sh                             # 共通設定
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions  # 実行用関数定義
mpiexec -of log.sample-par \
  postProcess -func sample -latestTime -parallel
mpiexec -of log.streamFunction-par \
  postProcess -func streamFunction -latestTime -parallel
mpiexec -of log.yPlus-par \
  simpleFoam -postProcess -func yPlus -latestTime -parallel
