set terminal postscript enhanced eps color solid 17
set size ratio 0 1,0.45
set pointsize 0.4
set xtics ("-2" -2, "-1" -1, "0" 0, "1" 1, "2" 2, "3" 3, "4" 4, "5" 5, "6" 6, "7" 7, "8" 8, "9" 9, "10" 10, "11" 11, "12" 12, "13" 13, "14" 14, "15" 15)
set grid
set noytics
set xrange [ -2.5 : 17 ]
set yrange [ 0 : 3 ]
set key horizontal box at 9, 3.45
set xlabel "[x/H]" offset 32,1.55
set lmargin 0
set rmargin 0
unset border
set arrow from -2,3 to 15,3 nohead lw 3
set arrow from -2,1 to 0,1 nohead lw 3
set arrow from 0,1 to 0,0 nohead lw 3
set arrow from 0,0 to 15,0 nohead lw 3
set arrow from 15,0 to 15,3 nohead lw 3 lt 0
set arrow from -2,1 to -2,3 nohead lw 3 lt 0

vRMSE=`cat log.Error.velocity`
kRMSE=`cat log.Error.kineticEnergy`
xR=`cat log.reattachmentLength`

set output "profilek.eps"
scale=0.04
set x2tics ("0" 0, sprintf("%g",scale) 1)
set arrow from 0,3 to 0,3.1 nohead lw 1
set arrow from 1,3 to 1,3.1 nohead lw 1

set label 1 "[k/Uc^2]" left at 1.7,3.3
set label 2 sprintf("x_R: %.4g",xR) left at 9.5,3.3
set label 3 sprintf("RMSE(k): %.4g",kRMSE) left at 12,3.3

plot \
"< awk -F ',' 'NR==2470,NR==2550 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale):1 title "Exp" with p pt 1 lc 1,\
"< awk -F ',' 'NR==2553,NR==2674 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale+1):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==2677,NR==2798 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale+2):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==2801,NR==2922 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale+3):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==2925,NR==3046 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale+4):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==3049,NR==3170 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale+5):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==3173,NR==3294 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale+6):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==3297,NR==3418 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale+7):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==3421,NR==3542 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale+8):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==3545,NR==3666 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale+9):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==3669,NR==3790 {print $2,0.5*($4*$4+$5*$5+$6*$6)}' ../share/exptData/fc_bs007.csv" using ($2/scale+10):1 title "" with p pt 1 lc 1,\
"< cat postProcessing/sample/*/x=-2H_k.xy" using ($4/scale-2):2 title "CFD" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=-1H_k.xy" using ($4/scale-1):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=0H_k.xy" using ($4/scale):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=1H_k.xy" using ($4/scale+1):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=2H_k.xy" using ($4/scale+2):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=3H_k.xy" using ($4/scale+3):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=4H_k.xy" using ($4/scale+4):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=5H_k.xy" using ($4/scale+5):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=6H_k.xy" using ($4/scale+6):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=7H_k.xy" using ($4/scale+7):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=8H_k.xy" using ($4/scale+8):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=9H_k.xy" using ($4/scale+9):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=10H_k.xy" using ($4/scale+10):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=11H_k.xy" using ($4/scale+11):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=12H_k.xy" using ($4/scale+12):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=13H_k.xy" using ($4/scale+13):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=14H_k.xy" using ($4/scale+14):2 title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=15H_k.xy" using ($4/scale+15):2 title "" with l lt 1 lc 0 lw 2


set output "profileU.eps"
scale=1
set x2tics (sprintf("%g",-scale) -1, "0" 0, sprintf("%g",scale) 1)
set arrow from -1,3 to -1,3.1 nohead lw 1

unset label 1
unset label 3
set label 4 "[U/U_c]" left at 1.7,3.3
set label 5 sprintf("RMSE(U,V): %.4g",vRMSE) left at 12,3.3

plot \
"< awk -F ',' 'NR==1105,NR==1185 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale):1 title "Exp" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1188,NR==1309 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale+1):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1312,NR==1433 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale+2):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1436,NR==1557 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale+3):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1560,NR==1681 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale+4):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1684,NR==1805 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale+5):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1808,NR==1929 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale+6):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1932,NR==2053 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale+7):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==2056,NR==2177 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale+8):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==2180,NR==2301 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale+9):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==2304,NR==2425 {print $2,$4}' ../share/exptData/fc_bs007.csv" using ($2/scale+10):1 title "" with p pt 1 lc 1,\
"< cat postProcessing/sample/*/x=-2H_U.xy" using ($4/scale-2):2  title "CFD" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=-1H_U.xy" using ($4/scale-1):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=0H_U.xy" using ($4/scale  ):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=1H_U.xy" using ($4/scale+1):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=2H_U.xy" using ($4/scale+2):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=3H_U.xy" using ($4/scale+3):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=4H_U.xy" using ($4/scale+4):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=5H_U.xy" using ($4/scale+5):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=6H_U.xy" using ($4/scale+6):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=7H_U.xy" using ($4/scale+7):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=8H_U.xy" using ($4/scale+8):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=9H_U.xy" using ($4/scale+9):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=10H_U.xy" using ($4/scale+10):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=11H_U.xy" using ($4/scale+11):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=12H_U.xy" using ($4/scale+12):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=13H_U.xy" using ($4/scale+13):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=14H_U.xy" using ($4/scale+14):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=15H_U.xy" using ($4/scale+15):2  title "" with l lt 1 lc 0 lw 2

set output "profileV.eps"
scale=0.2
set x2tics (sprintf("%g",-scale) -1, "0" 0, sprintf("%g",scale) 1)

unset label 4
set label 6 "[V/U_c]" left at 1.7,3.3

plot \
"< awk -F ',' 'NR==1105,NR==1185 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale):1 title "Exp" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1188,NR==1309 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale+1):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1312,NR==1433 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale+2):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1436,NR==1557 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale+3):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1560,NR==1681 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale+4):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1684,NR==1805 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale+5):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1808,NR==1929 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale+6):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==1932,NR==2053 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale+7):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==2056,NR==2177 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale+8):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==2180,NR==2301 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale+9):1 title "" with p pt 1 lc 1,\
"< awk -F ',' 'NR==2304,NR==2425 {print $2,$5}' ../share/exptData/fc_bs007.csv" using ($2/scale+10):1 title "" with p pt 1 lc 1,\
"< cat postProcessing/sample/*/x=-2H_U.xy" using ($5/scale-2):2  title "CFD" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=-1H_U.xy" using ($5/scale-1):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=0H_U.xy" using ($5/scale  ):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=1H_U.xy" using ($5/scale+1):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=2H_U.xy" using ($5/scale+2):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=3H_U.xy" using ($5/scale+3):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=4H_U.xy" using ($5/scale+4):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=5H_U.xy" using ($5/scale+5):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=6H_U.xy" using ($5/scale+6):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=7H_U.xy" using ($5/scale+7):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=8H_U.xy" using ($5/scale+8):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=9H_U.xy" using ($5/scale+9):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=10H_U.xy" using ($5/scale+10):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=11H_U.xy" using ($5/scale+11):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=12H_U.xy" using ($5/scale+12):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=13H_U.xy" using ($5/scale+13):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=14H_U.xy" using ($5/scale+14):2  title "" with l lt 1 lc 0 lw 2,\
"< cat postProcessing/sample/*/x=15H_U.xy" using ($5/scale+15):2  title "" with l lt 1 lc 0 lw 2
