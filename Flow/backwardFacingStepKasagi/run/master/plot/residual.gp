set terminal postscript eps color solid font "helvetica,17" linewidth 1.5
set output "residual.eps"
set logscale y
set xlabel "Time"
set ylabel "Residual"
set grid
set style data line
plot \
"postProcessing/solverInfo/0/solverInfo.dat" using 1:3  title "Ux",\
"" using 1:6  title "Uy",\
"" using 1:11 title "k",\
"" using 1:16 title "p",\
"" using 1:21 title "epsilon"
