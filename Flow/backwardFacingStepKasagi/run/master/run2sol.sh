#!/bin/sh
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM --mpi proc=48
#PJM -S
source ./runShare.sh                             # 共通設定
source ${WM_PROJECT_DIR}/bin/tools/RunFunctions  # 実行用関数定義
application=$(getApplication)                    # 実行アプリケーション名
jobid=${PJM_SUBJOBID:-$PJM_JOBID}                # ジョブID
log=log.$application.$jobid                      # ログファイル名
$application >& $log                             # ソルバ実行
