#!/bin/sh
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM --mpi proc=48
#PJM -S
source ./runShare.sh                                               # 共通設定
postProcess -func sample -latestTime &> log.sample                 # サンプリング
postProcess -func streamFunction -latestTime &> log.streamFunction # 流れ関数の算出
simpleFoam -postProcess -func yPlus -latestTime &> log.yPlus       # 無次元壁座標y+の算出
