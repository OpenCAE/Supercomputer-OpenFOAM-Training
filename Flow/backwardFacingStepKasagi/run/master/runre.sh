#!/bin/sh
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM --mpi proc=48
#PJM -S
source ./runShare.sh                             # 共通設定
# 領域分割再構築
reconstructPar -latestTime &> log.reconstructPar
