``` CommandLocal
ssh username@flow-fx.cc.nagoya-u.ac.jp
# username は各自のログイン名に置き換える．「-i SSH秘密鍵」の指定は通常不要
```
``` Command
##@flow-fx 講習会レポジトリの複製
# 以下，##@flow-fxと記載の実線枠内のコマンドはType I(flow-fx)のログインノードで実行する
# プロンプトが [username名@flow-fx* directory名]$ である事を確認してから実行する
# 講習会作業用ディレクトリを作成 (mkdir: make directories)
mkdir lectures
# 講習会作業用ディレクトリへ移動 (cd: change the current directory to dir)
cd lectures/
# 講習会レポジトリの複製
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
# レポジトリのディレクトリへ移動
cd Supercomputer-OpenFOAM-Training/
# 本講習会のブランチへの切り替え
git checkout Nagoya-University-Information-Technology-Center-20231018
# ブランチ名の表示
git branch
```
``` Command
## pjstat系コマンドのテスト実行@flow-fx
pjstat
pjstat -E
pjstat -H
pjstat --limit
pjstat2 --rsc
```
``` Command
## バッチキューの詳細構成を見る@flow-fx
pjstat2 --rsc -x
```
``` Command
## 投げられているジョブ数を見る@flow-fx
pjstat2 --rsc -b
```
``` Command
## ノードの利用状況の表示@flow-fx
pjstat2 --use
```
``` Command
## 解析ケースのコピー@flow-fx
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/milkDropCoronet/run/
cp -r master case-1
cd case-1/
```
``` Command
## ファイル構成の確認@flow-fx
# tree : ディレクトリ構造を樹木状に表示(標準コマンドではないが，本スパコンにはインストール済み)
tree
```
``` Command
## ベース格子生成ジョブの投入@flow-fx
pjsub run1base.sh
```
``` Command
## ジョブの状況確認@flow-fx
pjstat
```
``` Command
## ログファイルのトレース@flow-fx
tail -f log.*
```
``` Command
## ジョブの標準出力ファイルの確認@flow-fx
less run1base.sh.*.out
# 本来はJOBIDが付くが，*のワイルドカードで全ての文字にマッチする
# lessコマンドは1画面分の内容を表示したらプロンプトと表示してキー入力待ちとなる．
# 主な操作キー h : ヘルプ，SPC : 前，b : 後，/文字 : 文字を検索，. : 繰り返し，G : 末尾，q：終了
```
``` Command
## ログ確認@flow-fx
less log.blockMesh.*
less log.decomposePar.*
```
``` Command
## ParaViewの起動@lm01
# 実行ディレクトリへの移動
cd lectures/Supercomputer-OpenFOAM-Training/Flow/milkDropCoronet/run/case-1/
# ParaViewによるOpenFOAMデータ可視化用ダミーファイル作成
touch pv.foam
# paraviewをバックグラウンドジョブで起動(pv.foamを自動的に読み込むよう引数に書く)
/opt/ParaView/5.9.1-python3.8/bin/paraview pv.foam &
```
``` Command
## 格子生成ジョブの投入@flow-fx
pjsub run2mesh.sh
```
``` Command
## ジョブの状況確認@flow-fx
pjstat
```
``` Command
## ログファイルのトレース@flow-fx
tail -f log.*
```
``` Command
## ジョブの標準出力ファイルの確認@flow-fx
less run2mesh.sh.*.out
```
``` Command
## snappyHexMeshのログ確認@flow-fx
less log.snappyHexMesh.*
```
``` Command
## checkMeshのログ確認@flow-fx
less log.checkMesh.*
```
``` Command
## 初期値設定ジョブの投入@flow-fx
pjsub run3init.sh
```
``` Command
## ソルバ実行@flow-fx
pjsub run4sol.sh
```
``` Command
## ジョブ確認@flow-fx
pjstat
```
``` Command
## 圧力場のファイル確認@flow-fx
less 0.orig/p_rgh
```
``` Command
## 速度場のファイル確認@flow-fx
less 0.orig/U
```
``` Command
## 速度場のファイル確認@flow-fx
less 0.orig/alpha.liquid
```
``` Command
#液膜高さでのVOF値のサンプリング結果確認@lm01
less postProcessing/sample/0.01/fileSurface_alpha.liquid.xy
```
``` Command
## 王冠直径の時系列の算出@lm01
# 王冠直径の時系列をdiameter.txtに出力
python3 plot/diameter.py fileSurface_alpha.liquid.xy > impact-time.txt
# 液滴の衝突時刻を表示
cat impact-time.txt
```
``` Command
## 王冠直径の時系列を表示@lm01
less diameter.txt
```
``` Command
## 王冠直径の実験値との比較@lm01
# プロット
gnuplot plot/diameter.gp
# プロット画像表示
evince diameter.pdf &
```
``` Command
## ParaViewの起動@lm01
/opt/ParaView/5.9.1-python3.8/bin/paraview postProcessing/isoSurface/0.025/isoSurface.vtp &
```
``` Command
## ParaViewの起動@lm01
./Allrun.animImage
```
``` Command
## FFmpegのmoduleのload@lm01
module unload intel
module load gcc/4.8.5
module load ffmpeg/4.2.3
```
``` Command
## 動画の作成@lm01
./Allrun.anim
```
``` Command
## 動画の再生@lm01
totem anim/liquidSurface.mov
```
``` Command
## POV-Rayファイルの作成@lm01
./Allrun.animPOVExport
```
``` Command
## POV-Rayファイルの修正@lm01
./Allrun.animPOVMod
```
``` Command
## POV-Rayのmoduleのload@lm01
module unload gcc
module load intel/2019.5.281
module load povray/3.7.0.8
```
``` Command
## POV-Rayファイルのレンダリング@lm01
./Allrun.animPOVImage
```
``` Command
## FFmpegのmoduleのload@lm01
module unload intel
module load gcc/4.8.5
module load ffmpeg/4.2.3
```
``` Command
## 動画の作成@lm01
./Allrun.animPOV
```
``` Command
## 動画の再生@lm01
totem animPOV/liquidSurface.mov
```
``` Command
## ケースコピー@flow-fx
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/milkDropCoronet/run/
cp -r master case-2
cd case-2/
```
``` Command
## ステップジョブとして実行@flow-fx
./Allrun.batch -n 2
# -n ノード数
pjstat -E
# ステップジョブのサブジョブを表示する場合は-Eを付ける．
# ジョブが完了したら次に進む
```
``` Command
## プロットと表示@lm01
./Allrun.plot
evince *.pdf &
```
