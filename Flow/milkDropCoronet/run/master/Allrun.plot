#!/bin/sh
python3 plot/diameter.py fileSurface_alpha.liquid.xy > impact-time.txt
python3 plot/rmse.py plot/exp.txt diameter.txt > rmse.txt
gnuplot plot/diameter.gp
