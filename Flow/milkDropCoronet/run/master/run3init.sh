#!/bin/bash
#PJM -L rscgrp=fx-workshop
#PJM -L node=2
#PJM -L elapse=0:30:00
#PJM --mpi proc=96
#PJM -j
#PJM -S
source ./runShare.sh # 共通設定
restore0Dir -processor # 並列計算用の初期値場のコピー
mpiexec -of log.setFields.$jid setFields -parallel # 初期分布作成
