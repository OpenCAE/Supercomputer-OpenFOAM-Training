#!/bin/bash
#PJM -L rscgrp=fx-workshop
#PJM -L node=2
#PJM -L elapse=0:30:00
#PJM --mpi proc=96
#PJM -j
#PJM -S
source ./runShare.sh # 共通設定
mpiexec -of log.snappyHexMesh.$jid snappyHexMesh -overwrite -parallel # 並列格子生成
mpiexec -of log.renumberMesh.$jid renumberMesh -overwrite -parallel # 格子順変更による行列バンド幅縮小
mpiexec -of log.checkMesh.$jid checkMesh -constant -parallel # 格子の品質チェック
