#!/usr/bin/env python3
import sys
import os
import glob

if len(sys.argv) != 2:
    print(f'Usage: {sys.argv[0]} sample_filename', file=sys.stderr)
    exit(0)

fout = open('diameter.txt', 'w')
fout.write('#Time[ms] Diameter(CFD)[cm]\n')

try:
    cd = os.chdir("postProcessing/sample/")
except FileNotFoundError:
    exit(0)

times=sorted(glob.glob('[0-9]*'), key=float)
surfaceAlpha=0.9
impact=False
for time in times:
    alphaOld=1
    xOld=0
    diameter=0
    with open(time+'/'+sys.argv[1]) as f:
        lines = f.readlines()
    for line in reversed(lines):
        x=float(line.split('\t')[0])
        alpha=float(line.split('\t')[1])
        if alpha>surfaceAlpha:
            if not impact:
                t0=time
            impact=True
            r=(alpha-surfaceAlpha)/(alpha-alphaOld)
            diameter=((1-r)*x+r*xOld)*2.0
            break

        alphaOld=alpha
        xOld=x

    if impact:
        fout.write(f'{(float(time)-float(t0))*1000.0:g} {diameter*100.0:g}\n')

fout.close()
print(t0)
