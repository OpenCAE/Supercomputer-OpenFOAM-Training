# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1698, 1248]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
renderView1.CenterOfRotation = [0.0, 0.0, 0.010999999970197682]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [0.0, -0.08555548307359555, 0.02952827830679676]
renderView1.CameraFocalPoint = [0.0, 0.07594067178937416, -0.020264071323052316]
renderView1.CameraViewUp = [0.0, 0.2948698924922992, 0.9555374124028739]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.030091870462120526
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1698, 1248)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.CaseType = 'Decomposed Case'
pvfoam.MeshRegions = ['internalMesh']
pvfoam.CellArrays = ['U', 'alpha.liquid', 'p', 'p_rgh']
pvfoam.Decomposepolyhedra = 0

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(registrationName='AnnotateTimeFilter1', Input=pvfoam)
annotateTimeFilter1.Format = 'Time: %.4f s'

# create a new 'Contour'
contour1 = Contour(registrationName='Contour1', Input=pvfoam)
contour1.ContourBy = ['POINTS', 'alpha.liquid']
contour1.Isosurfaces = [0.5]
contour1.PointMergeMethod = 'Uniform Binning'

# create a new 'Reflect'
reflect1 = Reflect(registrationName='Reflect1', Input=contour1)

# create a new 'Reflect'
reflect2 = Reflect(registrationName='Reflect2', Input=reflect1)
reflect2.Plane = 'Y Min'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from annotateTimeFilter1
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
annotateTimeFilter1Display.Color = [0.0, 0.0, 0.0]
annotateTimeFilter1Display.FontSize = 32
annotateTimeFilter1Display.WindowLocation = 'UpperRightCorner'

# show data from reflect2
reflect2Display = Show(reflect2, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
reflect2Display.Representation = 'Surface'
reflect2Display.ColorArrayName = ['POINTS', '']
reflect2Display.SelectTCoordArray = 'None'
reflect2Display.SelectNormalArray = 'Normals'
reflect2Display.SelectTangentArray = 'None'
reflect2Display.OSPRayScaleArray = 'alpha.liquid'
reflect2Display.OSPRayScaleFunction = 'PiecewiseFunction'
reflect2Display.SelectOrientationVectors = 'U'
reflect2Display.ScaleFactor = 0.00599999986588955
reflect2Display.SelectScaleArray = 'alpha.liquid'
reflect2Display.GlyphType = 'Arrow'
reflect2Display.GlyphTableIndexArray = 'alpha.liquid'
reflect2Display.GaussianRadius = 0.00029999999329447744
reflect2Display.SetScaleArray = ['POINTS', 'alpha.liquid']
reflect2Display.ScaleTransferFunction = 'PiecewiseFunction'
reflect2Display.OpacityArray = ['POINTS', 'alpha.liquid']
reflect2Display.OpacityTransferFunction = 'PiecewiseFunction'
reflect2Display.DataAxesGrid = 'GridAxesRepresentation'
reflect2Display.PolarAxes = 'PolarAxesRepresentation'
reflect2Display.ScalarOpacityUnitDistance = 0.0016601284538858886
reflect2Display.OpacityArrayName = ['POINTS', 'alpha.liquid']
reflect2Display.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
reflect2Display.ScaleTransferFunction.Points = [0.5, 0.0, 0.5, 0.0, 0.5001220703125, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
reflect2Display.OpacityTransferFunction.Points = [0.5, 0.0, 0.5, 0.0, 0.5001220703125, 1.0, 0.5, 0.0]

# ----------------------------------------------------------------
# restore active source
SetActiveSource(reflect2)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')