# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1698, 1248]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [0.0, 0.0, 0.004749999847263098]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-0.06808536912399815, 0.0, 0.036399304649327446]
renderView1.CameraFocalPoint = [-0.010534459770125062, 0.0, 0.009351622294363926]
renderView1.CameraViewUp = [0.4253445320601719, 0.0017136330996417068, 0.9050298848701701]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.04269147945530501
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1698, 1248)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'XML PolyData Reader'
isoSurfacevtp = XMLPolyDataReader(registrationName='isoSurface.vtp', FileName=['postProcessing/isoSurface/0.025/isoSurface.vtp'])

# create a new 'Reflect'
reflect1 = Reflect(registrationName='Reflect1', Input=isoSurfacevtp)

# create a new 'Reflect'
reflect2 = Reflect(registrationName='Reflect2', Input=reflect1)
reflect2.Plane = 'Y Min'

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(registrationName='AnnotateTimeFilter1', Input=isoSurfacevtp)
annotateTimeFilter1.Format = 'Time: %.4f s'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from reflect2
reflect2Display = Show(reflect2, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
reflect2Display.Representation = 'Surface'
reflect2Display.ColorArrayName = ['POINTS', '']
reflect2Display.Ambient = 0.3
reflect2Display.SelectTCoordArray = 'None'
reflect2Display.SelectNormalArray = 'None'
reflect2Display.SelectTangentArray = 'None'
reflect2Display.OSPRayScaleFunction = 'PiecewiseFunction'
reflect2Display.SelectOrientationVectors = 'None'
reflect2Display.ScaleFactor = 0.00599999986588955
reflect2Display.SelectScaleArray = 'None'
reflect2Display.GlyphType = 'Arrow'
reflect2Display.GlyphTableIndexArray = 'None'
reflect2Display.GaussianRadius = 0.00029999999329447744
reflect2Display.SetScaleArray = ['POINTS', '']
reflect2Display.ScaleTransferFunction = 'PiecewiseFunction'
reflect2Display.OpacityArray = ['POINTS', '']
reflect2Display.OpacityTransferFunction = 'PiecewiseFunction'
reflect2Display.DataAxesGrid = 'GridAxesRepresentation'
reflect2Display.PolarAxes = 'PolarAxesRepresentation'
reflect2Display.ScalarOpacityUnitDistance = 0.0013229786312813324
reflect2Display.OpacityArrayName = ['FIELD', 'TimeValue']

# show data from annotateTimeFilter1
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
annotateTimeFilter1Display.Color = [0.0, 0.0, 0.0]
annotateTimeFilter1Display.FontSize = 36
annotateTimeFilter1Display.WindowLocation = 'UpperRightCorner'

# ----------------------------------------------------------------
# restore active source
SetActiveSource(isoSurfacevtp)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')