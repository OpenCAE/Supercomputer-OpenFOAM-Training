# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1698, 1248]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
renderView1.CenterOfRotation = [0.0, 0.0, 0.010999999970197682]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [0.09412239489559025, -0.11768855543280335, 0.0999155860928226]
renderView1.CameraFocalPoint = [0.020794692259127612, 0.009318751135852678, 0.015244048380385554]
renderView1.CameraViewUp = [-0.24999999999999772, 0.43301270189221946, 0.8660254037844393]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.02022357355267314
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1698, 1248)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.CaseType = 'Decomposed Case'
pvfoam.MeshRegions = ['back', 'bottom', 'front', 'left', 'right', 'top']
pvfoam.CellArrays = ['U', 'alpha.liquid']
pvfoam.Decomposepolyhedra = 0

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'alphaliquid'
alphaliquidLUT = GetColorTransferFunction('alphaliquid')
alphaliquidLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Surface'
pvfoamDisplay.ColorArrayName = ['CELLS', 'alpha.liquid']
pvfoamDisplay.LookupTable = alphaliquidLUT
pvfoamDisplay.SelectTCoordArray = 'None'
pvfoamDisplay.SelectNormalArray = 'None'
pvfoamDisplay.SelectTangentArray = 'None'
pvfoamDisplay.OSPRayScaleArray = 'U'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'None'
pvfoamDisplay.ScaleFactor = 0.002999999932944775
pvfoamDisplay.SelectScaleArray = 'None'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'None'
pvfoamDisplay.GaussianRadius = 0.00014999999664723872
pvfoamDisplay.SetScaleArray = ['POINTS', '']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = ['POINTS', '']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
pvfoamDisplay.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
pvfoamDisplay.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for alphaliquidLUT in view renderView1
alphaliquidLUTColorBar = GetScalarBar(alphaliquidLUT, renderView1)
alphaliquidLUTColorBar.Title = 'alpha.liquid'
alphaliquidLUTColorBar.ComponentTitle = ''
alphaliquidLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
alphaliquidLUTColorBar.LabelColor = [0.0, 0.0, 0.0]

# set color bar visibility
alphaliquidLUTColorBar.Visibility = 1

# show color legend
pvfoamDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'alphaliquid'
alphaliquidPWF = GetOpacityTransferFunction('alphaliquid')
alphaliquidPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')