# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [860, 517]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
renderView1.CenterOfRotation = [0.0, 0.0, 0.010999999970197682]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [0.09412239489559025, -0.11768855543280335, 0.0999155860928226]
renderView1.CameraFocalPoint = [0.020794692259127612, 0.009318751135852678, 0.015244048380385554]
renderView1.CameraViewUp = [-0.24999999999999772, 0.43301270189221946, 0.8660254037844393]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.02022357355267314
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(860, 517)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.MeshRegions = ['back', 'bottom', 'front', 'left', 'right', 'top']
pvfoam.CellArrays = ['cellDist']
pvfoam.Decomposepolyhedra = 0

# create a new 'OpenFOAMReader'
pvfoam_1 = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam_1.MeshRegions = ['top', 'front', 'left', 'back', 'right', 'bottom']
pvfoam_1.Decomposepolyhedra = 0
pvfoam_1.Readzones = 1

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'cellDist'
cellDistLUT = GetColorTransferFunction('cellDist')
cellDistLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 47.5, 0.865003, 0.865003, 0.865003, 95.0, 0.705882, 0.0156863, 0.14902]
cellDistLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Surface With Edges'
pvfoamDisplay.ColorArrayName = ['CELLS', 'cellDist']
pvfoamDisplay.LookupTable = cellDistLUT
pvfoamDisplay.SelectTCoordArray = 'None'
pvfoamDisplay.SelectNormalArray = 'None'
pvfoamDisplay.SelectTangentArray = 'None'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'None'
pvfoamDisplay.ScaleFactor = 0.002999999932944775
pvfoamDisplay.SelectScaleArray = 'None'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'None'
pvfoamDisplay.GaussianRadius = 0.00014999999664723872
pvfoamDisplay.SetScaleArray = ['POINTS', '']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = ['POINTS', '']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'

# setup the color legend parameters for each legend in this view

# get color legend/bar for cellDistLUT in view renderView1
cellDistLUTColorBar = GetScalarBar(cellDistLUT, renderView1)
cellDistLUTColorBar.Title = 'cellDist'
cellDistLUTColorBar.ComponentTitle = ''
cellDistLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
cellDistLUTColorBar.LabelColor = [0.0, 0.0, 0.0]

# set color bar visibility
cellDistLUTColorBar.Visibility = 1

# show color legend
pvfoamDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'cellDist'
cellDistPWF = GetOpacityTransferFunction('cellDist')
cellDistPWF.Points = [0.0, 0.0, 0.5, 0.0, 95.0, 1.0, 0.5, 0.0]
cellDistPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')