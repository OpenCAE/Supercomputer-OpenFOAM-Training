# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1700, 1248]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
renderView1.CenterOfRotation = [0.0, 0.0, 0.010999999970197682]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [0.09447773674727729, -0.11804474898997232, 0.09907356068648036]
renderView1.CameraFocalPoint = [0.02115003411081467, 0.008962557578683727, 0.014402022974043341]
renderView1.CameraViewUp = [-0.2499999999999972, 0.43301270189221963, 0.8660254037844394]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.02022357355267314
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1700, 1248)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.CaseType = 'Decomposed Case'
pvfoam.MeshRegions = ['top', 'front', 'left', 'back', 'right', 'bottom']
pvfoam.Cachemesh = 0
pvfoam.Decomposepolyhedra = 0
pvfoam.Readzones = 1

# create a new 'Extract Block'
extractBlock1 = ExtractBlock(registrationName='ExtractBlock1', Input=pvfoam)
extractBlock1.BlockIndices = [12]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from extractBlock1
extractBlock1Display = Show(extractBlock1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
extractBlock1Display.Representation = 'Surface'
extractBlock1Display.ColorArrayName = ['POINTS', '']
extractBlock1Display.SelectTCoordArray = 'None'
extractBlock1Display.SelectNormalArray = 'None'
extractBlock1Display.SelectTangentArray = 'None'
extractBlock1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractBlock1Display.SelectOrientationVectors = 'None'
extractBlock1Display.ScaleFactor = 0.002999999932944775
extractBlock1Display.SelectScaleArray = 'None'
extractBlock1Display.GlyphType = 'Arrow'
extractBlock1Display.GlyphTableIndexArray = 'None'
extractBlock1Display.GaussianRadius = 0.00014999999664723872
extractBlock1Display.SetScaleArray = ['POINTS', '']
extractBlock1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractBlock1Display.OpacityArray = ['POINTS', '']
extractBlock1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractBlock1Display.DataAxesGrid = 'GridAxesRepresentation'
extractBlock1Display.PolarAxes = 'PolarAxesRepresentation'
extractBlock1Display.ScalarOpacityUnitDistance = 0.001233615279133943
extractBlock1Display.OpacityArrayName = ['CELLS', 'CellId']
extractBlock1Display.ExtractedBlockIndex = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(extractBlock1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')