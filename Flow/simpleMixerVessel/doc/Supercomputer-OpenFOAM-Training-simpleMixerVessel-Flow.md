``` CommandLocal
ssh username@flow-fx.cc.nagoya-u.ac.jp
# username は各自のログイン名に置き換える．「-i SSH秘密鍵」の指定は通常不要
```
``` Command
##@flow-fx 講習会レポジトリの複製
# 以下，##@flow-fxと記載の実線枠内のコマンドはType I(flow-fx)のログインノードで実行する
# プロンプトが [username名@flow-fx* directory名]$ である事を確認してから実行する
# 講習会作業用ディレクトリを作成 (mkdir: make directories)
mkdir lectures
# 講習会作業用ディレクトリへ移動 (cd: change the current directory to dir)
cd lectures/
# 講習会レポジトリの複製
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
# レポジトリのディレクトリへ移動
cd Supercomputer-OpenFOAM-Training/
# 本講習会のブランチへの切り替え
git checkout Nagoya-University-Information-Technology-Center-20240207
# ブランチ名の表示
git branch
```
``` Command
##@flow-fx pjstatのテスト実行
pjstat
pjstat -E
pjstat -H
pjstat --limit
```
``` Command
##@flow-fx カスタマイズ・コマンドpjstat2のテスト実行
pjstat2 --rsc
```
``` Command
##@flow-fx バッチキューの詳細構成を見る
pjstat2 --rsc -x
```
``` Command
##@flow-fx 投げられているジョブ数を見る
pjstat2 --rsc -b
```
``` Command
##@flow-fx リソースグループのノード使用率
pjstat2 --use
```
``` Command
##@flow-fx MRF法のケースのコピーと移動
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/simpleMixerVessel/run/
cp -r simpleMixerVesselMRF/ MRF-0
cd MRF-0/
```
``` Command
##@flow-fx ベース格子生成ジョブの投入
pjsub run1base.sh
```
``` Command
##@flow-fx ジョブの状況確認
pjstat
```
``` Command
##@flow-fx ログファイルのトレース
tail -f log.*
```
``` Command
##@flow-fx ジョブの標準出力ファイルの確認
less run1base.sh.*.out
# ジョブの出力ファイル名にはJOBIDが付くが，*のワイルドカードで全ての文字にマッチする
# lessコマンドは1画面分の内容を表示したらプロンプトと表示してキー入力待ちとなる．
# 主な操作キー h : ヘルプ，SPC : 前，b : 後，/文字 : 文字を検索，. : 繰り返し，G : 末尾，q：終了
```
``` Command
##@flow-fx blockMeshのログの確認
less log.blockMesh.*
# [less] スペース等で進め，q で終了
```
``` Command
##@flow-fx decomposeParのログの確認
less log.decomposePar.*
# [less] Maxの検索
/Max
# [less]一行戻る
C-p # カーソル↑やvi的なコマンド(j，C-j, y, C-y)でも良い(hによるヘルプ参照)
# [less] スペース や G 等で進め，q で終了
```
``` Command
##@lm01 ParaViewの起動
# 以下，##@lm01と記載の実線枠内のコマンドはNICE DCV系由でログインした可視化用ノードlm01で実行する
# 実行ディレクトリへの移動
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/simpleMixerVessel/run/MRF-0/
# ParaViewによるOpenFOAMデータ可視化用ダミーファイル作成
# 中身は何でも良いので，touchコマンドで空ファイルを作成する
touch pv.foam
# pv.foamを自動的に読み込むように引数にして，paraviewをバックグラウンドジョブで起動
/opt/ParaView/5.9.1-python3.8/bin/paraview pv.foam &
```
``` Command
##@flow-fx 格子生成ジョブの投入
pjsub run2mesh.sh
```
``` Command
##@flow-fx ジョブの状況確認
pjstat
```
``` Command
##@flow-fx ログファイルのトレース
tail -f log.*
```
``` Command
##@flow-fx checkMeshのログ確認
less log.checkMesh.*
```
``` Command
##@lm01 ParaViewを起動してpv.foamを読み込む
/opt/ParaView/5.9.1-python3.8/bin/paraview pv.foam &
```
``` Command
##@flow-fx 初期値作成
pjsub run3init.sh
```
``` Command
##@lm01 ParaViewを起動してpv.foamを読み込む
/opt/ParaView/5.9.1-python3.8/bin/paraview pv.foam &
```
``` Command
##@flow-fx 流体解析のジョブ投入
pjsub run4sol.sh
```
``` Command
##@lm01 トレーサ濃度のモニター
/home/center/opt/aarch64/apps/tcsds/1.2.31/openfoam/v2106/OpenFOAM-v2106/bin/foamMonitor -r 1 postProcessing/probes/0/Y &
# -r :更新秒
#または以下(上記コマンドをAllrun.monitorに記述している)
./Allrun.monitor &
```
``` Command
##@lm01 トレーサー濃度場時系列プロット
gnuplot plot/C.gp
evince *.pdf &
```
``` Command
##@lm01 ParaViewを起動してpv.foamを読み込む
/opt/ParaView/5.9.1-python3.8/bin/paraview pv.foam &
```
``` Command
##@lm01 ParaViewの起動
/opt/ParaView/5.9.1-python3.8/bin/paraview &
```
``` Command
##@lm01 時系列可視化
./Allrun.animImage
```
``` Command
##@lm01 FFmpegのmoduleのload
module unload intel
module load gcc/4.8.5
module load ffmpeg/4.2.3
```
``` Command
##@lm01 動画の作成
./Allrun.anim
```
``` Command
##@lm01 動画の再生
firefox anim/liquidSurface.mp4
```
``` Command
##@flow-fx 解析結果の再構築の実行
pjsub reconstruct.sh
```
``` Command
##@flow-fx 解適合格子ケースの作成
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/simpleMixerVessel/run/
cp -r simpleMixerVesselMRF/ MRF-AMR
cd MRF-AMR/
cp run4sol.sh run5sol.sh
#解適合格子の流体解析は1時間で終わらないので，2回ジョブを投入するようにする
```
``` Command
##@flow-fx 動的格子設定の変更
vi constant/dynamicMeshDict
# エディタは何でも良い
```
``` Command
##@flow-fx 解法の設定の変更
vi system/fvSolution
# エディタは何でも良い
```
``` Command
##@flow-fx ステップジョブ実行
~/lectures/Supercomputer-OpenFOAM-Training/Flow/bin/Allrun.batch
pjstat -E
# -E: ステップジョブ内のサブジョブを展開して表示
```
``` Command
##@lm01 圧力時系列のモニターと比較プロット
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/simpleMixerVessel/run/MRF-AMR/
# 圧力時系列のモニター
./Allrun.monitor &
# MRF法基本ケースとの圧力時系列の比較プロット
gnuplot plot/comparison-C.gp
# プロットの閲覧
evince *.pdf &
```
``` Command
##@flow-fx interIsoFoamを用いた解析ケースの作成
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/simpleMixerVessel/run/
cp -r simpleMixerVesselMRF/ MRF-Iso
cd MRF-Iso/
```
``` Command
##@flow-fx 実行アプリケーション名の変更
vi system/controlDict
# エディタは何でも良い
```
``` Command
##@flow-fx 液相率のVOF解法設定の変更
vi system/fvSolution
```
``` Command
##@flow-fx トレーサ質量分率Yの輸送方程式の解法設定の変更
vi system/scalarTransport
```
``` Command
##@flow-fx ステップジョブ実行
~/lectures/Supercomputer-OpenFOAM-Training/Flow/bin/Allrun.batch
pjstat -E
# -E: ステップジョブ内のサブジョブを展開して表示
```
``` Command
##@lm01 圧力時系列のモニターと比較プロット
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/simpleMixerVessel/run/MRF-Iso/
# 圧力時系列のモニター
./Allrun.monitor &
# MRF法基本ケースとの圧力時系列の比較プロット
gnuplot plot/comparison-C.gp
# プロットの閲覧
evince *.pdf &
```
``` Command
##@flow-fx 作成済の移動格子解の析ケースのコピーと解析実行
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/simpleMixerVessel/run/
cp -r simpleMixerVesselAMI/ AMI-0
cd AMI-0/
~/lectures/Supercomputer-OpenFOAM-Training/Flow/bin/Allrun.batch
```
``` Command
##@lm01 圧力時系列のモニターと比較プロット
cd ~/lectures/Supercomputer-OpenFOAM-Training/Flow/simpleMixerVessel/run/AMI-0/
# 圧力時系列のモニター
./Allrun.monitor &
# MRF法基本ケースとの圧力時系列の比較プロット
gnuplot plot/comparison-C.gp
# プロットの閲覧
evince *.pdf &
```
