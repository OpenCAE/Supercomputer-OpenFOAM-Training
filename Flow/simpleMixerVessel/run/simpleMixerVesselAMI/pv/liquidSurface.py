# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [590, 516]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [0.0, 0.0, 0.270000010728836]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [1.0, 1.0, -1.0]
renderView1.CameraFocalPoint = [0.0, 0.0, 0.27]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraViewAngle = 1.0
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.297376660288923
renderView1.CameraParallelProjection = 1
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(590, 517)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'STL Reader'
vesselstl = STLReader(registrationName='vessel.stl', FileNames=['constant/triSurface/vessel.stl'])

# create a new 'STL Reader'
bafflestl = STLReader(registrationName='baffle.stl', FileNames=['constant/triSurface/baffle.stl'])

# create a new 'STL Reader'
shaftMovingstl = STLReader(registrationName='shaftMoving.stl', FileNames=['constant/triSurface/shaftMoving.stl'])

# create a new 'STL Reader'
shaftRotatingstl = STLReader(registrationName='shaftRotating.stl', FileNames=['constant/triSurface/shaftRotating.stl'])

# create a new 'XML MultiBlock Data Reader'
simpleMixerVesselAMIvtmseries = XMLMultiBlockDataReader(registrationName='simpleMixerVesselAMI.vtm.series', FileName=['postProcessing/vtkWrite/simpleMixerVesselAMI.vtm.series'])

# create a new 'XML PolyData Reader'
isoSurfacevtp = XMLPolyDataReader(registrationName='isoSurface.vtp', FileName=['postProcessing/isoSurface/0.02/isoSurface.vtp'])
isoSurfacevtp.PointArrayStatus = ['Y']

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(registrationName='AnnotateTimeFilter1', Input=isoSurfacevtp)
annotateTimeFilter1.Format = 'Time: %.2f s'

# create a new 'XML PolyData Reader'
centerPlaneXYvtp = XMLPolyDataReader(registrationName='centerPlaneXY.vtp', FileName=['postProcessing/cuttingPlane/0.02/centerPlaneXY.vtp'])
centerPlaneXYvtp.PointArrayStatus = ['Y']

# create a new 'Clip'
clip1 = Clip(registrationName='Clip1', Input=centerPlaneXYvtp)
clip1.ClipType = 'Plane'
clip1.HyperTreeGridClipper = 'Plane'
clip1.Scalars = ['POINTS', 'Y']
clip1.Value = 0.010077995248138905

# init the 'Plane' selected for 'ClipType'
clip1.ClipType.Origin = [0.0, 0.0, 0.45]
clip1.ClipType.Normal = [0.0, 0.0, 1.0]

# init the 'Plane' selected for 'HyperTreeGridClipper'
clip1.HyperTreeGridClipper.Origin = [0.0, 0.0, 0.27000001072883606]

# create a new 'STL Reader'
turbinestl = STLReader(registrationName='turbine.stl', FileNames=['constant/triSurface/turbine.stl'])

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from bafflestl
bafflestlDisplay = Show(bafflestl, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'STLSolidLabeling'
sTLSolidLabelingLUT = GetColorTransferFunction('STLSolidLabeling')
sTLSolidLabelingLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 1.5, 0.865003, 0.865003, 0.865003, 3.0, 0.705882, 0.0156863, 0.14902]
sTLSolidLabelingLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
bafflestlDisplay.Representation = 'Surface'
bafflestlDisplay.ColorArrayName = ['POINTS', '']
bafflestlDisplay.LookupTable = sTLSolidLabelingLUT
bafflestlDisplay.SelectTCoordArray = 'None'
bafflestlDisplay.SelectNormalArray = 'None'
bafflestlDisplay.SelectTangentArray = 'None'
bafflestlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
bafflestlDisplay.SelectOrientationVectors = 'None'
bafflestlDisplay.ScaleFactor = 0.04800000097602606
bafflestlDisplay.SelectScaleArray = 'STLSolidLabeling'
bafflestlDisplay.GlyphType = 'Arrow'
bafflestlDisplay.GlyphTableIndexArray = 'STLSolidLabeling'
bafflestlDisplay.GaussianRadius = 0.002400000048801303
bafflestlDisplay.SetScaleArray = ['POINTS', '']
bafflestlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
bafflestlDisplay.OpacityArray = ['POINTS', '']
bafflestlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
bafflestlDisplay.DataAxesGrid = 'GridAxesRepresentation'
bafflestlDisplay.PolarAxes = 'PolarAxesRepresentation'

# show data from shaftMovingstl
shaftMovingstlDisplay = Show(shaftMovingstl, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
shaftMovingstlDisplay.Representation = 'Surface'
shaftMovingstlDisplay.ColorArrayName = ['POINTS', '']
shaftMovingstlDisplay.LookupTable = sTLSolidLabelingLUT
shaftMovingstlDisplay.SelectTCoordArray = 'None'
shaftMovingstlDisplay.SelectNormalArray = 'None'
shaftMovingstlDisplay.SelectTangentArray = 'None'
shaftMovingstlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
shaftMovingstlDisplay.SelectOrientationVectors = 'None'
shaftMovingstlDisplay.ScaleFactor = 0.02199999988079071
shaftMovingstlDisplay.SelectScaleArray = 'STLSolidLabeling'
shaftMovingstlDisplay.GlyphType = 'Arrow'
shaftMovingstlDisplay.GlyphTableIndexArray = 'STLSolidLabeling'
shaftMovingstlDisplay.GaussianRadius = 0.0010999999940395355
shaftMovingstlDisplay.SetScaleArray = ['POINTS', '']
shaftMovingstlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
shaftMovingstlDisplay.OpacityArray = ['POINTS', '']
shaftMovingstlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
shaftMovingstlDisplay.DataAxesGrid = 'GridAxesRepresentation'
shaftMovingstlDisplay.PolarAxes = 'PolarAxesRepresentation'

# show data from shaftRotatingstl
shaftRotatingstlDisplay = Show(shaftRotatingstl, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
shaftRotatingstlDisplay.Representation = 'Surface'
shaftRotatingstlDisplay.ColorArrayName = ['POINTS', '']
shaftRotatingstlDisplay.LookupTable = sTLSolidLabelingLUT
shaftRotatingstlDisplay.SelectTCoordArray = 'None'
shaftRotatingstlDisplay.SelectNormalArray = 'None'
shaftRotatingstlDisplay.SelectTangentArray = 'None'
shaftRotatingstlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
shaftRotatingstlDisplay.SelectOrientationVectors = 'None'
shaftRotatingstlDisplay.ScaleFactor = 0.04300000220537186
shaftRotatingstlDisplay.SelectScaleArray = 'STLSolidLabeling'
shaftRotatingstlDisplay.GlyphType = 'Arrow'
shaftRotatingstlDisplay.GlyphTableIndexArray = 'STLSolidLabeling'
shaftRotatingstlDisplay.GaussianRadius = 0.002150000110268593
shaftRotatingstlDisplay.SetScaleArray = ['POINTS', '']
shaftRotatingstlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
shaftRotatingstlDisplay.OpacityArray = ['POINTS', '']
shaftRotatingstlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
shaftRotatingstlDisplay.DataAxesGrid = 'GridAxesRepresentation'
shaftRotatingstlDisplay.PolarAxes = 'PolarAxesRepresentation'

# show data from vesselstl
vesselstlDisplay = Show(vesselstl, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
vesselstlDisplay.Representation = 'Feature Edges'
vesselstlDisplay.ColorArrayName = ['POINTS', '']
vesselstlDisplay.LookupTable = sTLSolidLabelingLUT
vesselstlDisplay.SelectTCoordArray = 'None'
vesselstlDisplay.SelectNormalArray = 'None'
vesselstlDisplay.SelectTangentArray = 'None'
vesselstlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
vesselstlDisplay.SelectOrientationVectors = 'None'
vesselstlDisplay.ScaleFactor = 0.05420000086305663
vesselstlDisplay.SelectScaleArray = 'STLSolidLabeling'
vesselstlDisplay.GlyphType = 'Arrow'
vesselstlDisplay.GlyphTableIndexArray = 'STLSolidLabeling'
vesselstlDisplay.GaussianRadius = 0.0027100000431528317
vesselstlDisplay.SetScaleArray = ['POINTS', '']
vesselstlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
vesselstlDisplay.OpacityArray = ['POINTS', '']
vesselstlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
vesselstlDisplay.DataAxesGrid = 'GridAxesRepresentation'
vesselstlDisplay.PolarAxes = 'PolarAxesRepresentation'

# show data from isoSurfacevtp
isoSurfacevtpDisplay = Show(isoSurfacevtp, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'Y'
yLUT = GetColorTransferFunction('Y')
yLUT.AutomaticRescaleRangeMode = 'Never'
yLUT.RGBPoints = [0.00020000000000000004, 0.231373, 0.298039, 0.752941, 0.0020000000000000005, 0.865003, 0.865003, 0.865003, 0.020000000000000004, 0.705882, 0.0156863, 0.14902]
yLUT.UseLogScale = 1
yLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
isoSurfacevtpDisplay.Representation = 'Surface'
isoSurfacevtpDisplay.ColorArrayName = ['POINTS', 'Y']
isoSurfacevtpDisplay.LookupTable = yLUT
isoSurfacevtpDisplay.SelectTCoordArray = 'None'
isoSurfacevtpDisplay.SelectNormalArray = 'None'
isoSurfacevtpDisplay.SelectTangentArray = 'None'
isoSurfacevtpDisplay.OSPRayScaleArray = 'Y'
isoSurfacevtpDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
isoSurfacevtpDisplay.SelectOrientationVectors = 'None'
isoSurfacevtpDisplay.ScaleFactor = 0.021999940276145935
isoSurfacevtpDisplay.SelectScaleArray = 'None'
isoSurfacevtpDisplay.GlyphType = 'Arrow'
isoSurfacevtpDisplay.GlyphTableIndexArray = 'None'
isoSurfacevtpDisplay.GaussianRadius = 0.0010999970138072968
isoSurfacevtpDisplay.SetScaleArray = ['POINTS', 'Y']
isoSurfacevtpDisplay.ScaleTransferFunction = 'PiecewiseFunction'
isoSurfacevtpDisplay.OpacityArray = ['POINTS', 'Y']
isoSurfacevtpDisplay.OpacityTransferFunction = 'PiecewiseFunction'
isoSurfacevtpDisplay.DataAxesGrid = 'GridAxesRepresentation'
isoSurfacevtpDisplay.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
isoSurfacevtpDisplay.ScaleTransferFunction.Points = [0.0013628657907247543, 0.0, 0.5, 0.0, 0.02764485590159893, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
isoSurfacevtpDisplay.OpacityTransferFunction.Points = [0.0013628657907247543, 0.0, 0.5, 0.0, 0.02764485590159893, 1.0, 0.5, 0.0]

# show data from annotateTimeFilter1
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
annotateTimeFilter1Display.FontSize = 32
annotateTimeFilter1Display.WindowLocation = 'UpperRightCorner'
annotateTimeFilter1Display.Position = [0.01, 0.955183752417795]

# show data from clip1
clip1Display = Show(clip1, renderView1, 'UnstructuredGridRepresentation')

# get opacity transfer function/opacity map for 'Y'
yPWF = GetOpacityTransferFunction('Y')
yPWF.Points = [0.0002, 0.0, 0.5, 0.0, 0.02, 1.0, 0.5, 0.0]
yPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
clip1Display.Representation = 'Surface'
clip1Display.ColorArrayName = ['POINTS', 'Y']
clip1Display.LookupTable = yLUT
clip1Display.SelectTCoordArray = 'None'
clip1Display.SelectNormalArray = 'None'
clip1Display.SelectTangentArray = 'None'
clip1Display.OSPRayScaleArray = 'Y'
clip1Display.OSPRayScaleFunction = 'PiecewiseFunction'
clip1Display.SelectOrientationVectors = 'None'
clip1Display.ScaleFactor = 0.020000000298023225
clip1Display.SelectScaleArray = 'None'
clip1Display.GlyphType = 'Arrow'
clip1Display.GlyphTableIndexArray = 'None'
clip1Display.GaussianRadius = 0.0010000000149011613
clip1Display.SetScaleArray = ['POINTS', 'Y']
clip1Display.ScaleTransferFunction = 'PiecewiseFunction'
clip1Display.OpacityArray = ['POINTS', 'Y']
clip1Display.OpacityTransferFunction = 'PiecewiseFunction'
clip1Display.DataAxesGrid = 'GridAxesRepresentation'
clip1Display.PolarAxes = 'PolarAxesRepresentation'
clip1Display.ScalarOpacityFunction = yPWF
clip1Display.ScalarOpacityUnitDistance = 0.022065651030946067
clip1Display.OpacityArrayName = ['POINTS', 'Y']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
clip1Display.ScaleTransferFunction.Points = [-1.8051560812809275e-29, 0.0, 0.5, 0.0, 6.112845339721268e-20, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
clip1Display.OpacityTransferFunction.Points = [-1.8051560812809275e-29, 0.0, 0.5, 0.0, 6.112845339721268e-20, 1.0, 0.5, 0.0]

# show data from simpleMixerVesselAMIvtmseries
simpleMixerVesselAMIvtmseriesDisplay = Show(simpleMixerVesselAMIvtmseries, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
simpleMixerVesselAMIvtmseriesDisplay.Representation = 'Surface'
simpleMixerVesselAMIvtmseriesDisplay.ColorArrayName = ['POINTS', '']
simpleMixerVesselAMIvtmseriesDisplay.SelectTCoordArray = 'None'
simpleMixerVesselAMIvtmseriesDisplay.SelectNormalArray = 'None'
simpleMixerVesselAMIvtmseriesDisplay.SelectTangentArray = 'None'
simpleMixerVesselAMIvtmseriesDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
simpleMixerVesselAMIvtmseriesDisplay.SelectOrientationVectors = 'None'
simpleMixerVesselAMIvtmseriesDisplay.ScaleFactor = 0.021879252791404725
simpleMixerVesselAMIvtmseriesDisplay.SelectScaleArray = 'None'
simpleMixerVesselAMIvtmseriesDisplay.GlyphType = 'Arrow'
simpleMixerVesselAMIvtmseriesDisplay.GlyphTableIndexArray = 'None'
simpleMixerVesselAMIvtmseriesDisplay.GaussianRadius = 0.0010939626395702362
simpleMixerVesselAMIvtmseriesDisplay.SetScaleArray = ['POINTS', '']
simpleMixerVesselAMIvtmseriesDisplay.ScaleTransferFunction = 'PiecewiseFunction'
simpleMixerVesselAMIvtmseriesDisplay.OpacityArray = ['POINTS', '']
simpleMixerVesselAMIvtmseriesDisplay.OpacityTransferFunction = 'PiecewiseFunction'
simpleMixerVesselAMIvtmseriesDisplay.DataAxesGrid = 'GridAxesRepresentation'
simpleMixerVesselAMIvtmseriesDisplay.PolarAxes = 'PolarAxesRepresentation'

# setup the color legend parameters for each legend in this view

# get color legend/bar for yLUT in view renderView1
yLUTColorBar = GetScalarBar(yLUT, renderView1)
yLUTColorBar.WindowLocation = 'AnyLocation'
yLUTColorBar.Position = [0.8310989414801009, 0.04061895551257241]
yLUTColorBar.Title = 'Y'
yLUTColorBar.ComponentTitle = ''
yLUTColorBar.ScalarBarLength = 0.850309477756287

# set color bar visibility
yLUTColorBar.Visibility = 1

# show color legend
isoSurfacevtpDisplay.SetScalarBarVisibility(renderView1, True)

# show color legend
clip1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'STLSolidLabeling'
sTLSolidLabelingPWF = GetOpacityTransferFunction('STLSolidLabeling')
sTLSolidLabelingPWF.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]
sTLSolidLabelingPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(clip1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
