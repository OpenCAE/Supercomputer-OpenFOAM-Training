# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [212, 498]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
renderView1.CenterOfRotation = [-1.11758708953857e-08, 0.000142261385917664, 0.271508538047783]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [1.14355584778127, 0.095107917913514, 0.220685803963411]
renderView1.CameraFocalPoint = [-1.11758708953857e-08, 0.095107917913514, 0.220685803963411]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 0.223006640837517
renderView1.CameraParallelProjection = 1
renderView1.Background = [1.0, 0.999969481956207, 0.999984740978103]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.EnvironmentNorth = [1.0, 0.0, 0.0]
renderView1.EnvironmentEast = [0.0, 1.0, 0.0]
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(212, 498)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.CaseType = 'Decomposed Case'
pvfoam.MeshRegions = ['internalMesh', 'top', 'bottom', 'vessel', 'baffle_baffle1', 'baffle_baffle2', 'baffle_baffle3', 'baffle_baffle4', 'turbine_turbineLow1', 'turbine_turbineLow2', 'turbine_turbineHigh1', 'turbine_turbineHigh2', 'shaftMoving_shaftMovingLow', 'shaftMoving_shaftMovingHigh', 'shaftRotating_shaftRotatingLow', 'shaftRotating_shaftRotatingHigh']
pvfoam.CellArrays = ['Y', 'alpha.water', 'U', 'alpha.water_0', 'epsilon', 'k', 'nut', 'p', 'p_rgh']
pvfoam.Readzones = 1

# create a new 'Extract Block'
extractBlock1 = ExtractBlock(registrationName='ExtractBlock1', Input=pvfoam)
extractBlock1.BlockIndices = [16, 11, 10, 13, 12, 15, 14, 17]

# create a new 'Slice'
slice1 = Slice(registrationName='Slice1', Input=pvfoam)
slice1.SliceType = 'Plane'
slice1.HyperTreeGridSlicer = 'Plane'
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [0.0, 0.0, 0.270000010728836]

# create a new 'Calculator'
calculator1 = Calculator(registrationName='Calculator1', Input=slice1)
calculator1.ResultArrayName = 'Urz'
calculator1.Function = '(U_Y*jHat+U_Z*kHat)/(300/60)/0.1'

# create a new 'GlyphLegacy'
glyph1 = GlyphLegacy(registrationName='Glyph1', Input=calculator1,
    GlyphType='2D Glyph')
glyph1.Scalars = ['POINTS', 'None']
glyph1.Vectors = ['POINTS', 'Urz']
glyph1.ScaleMode = 'vector'
glyph1.ScaleFactor = 0.03
glyph1.MaximumNumberOfSamplePoints = 1000
glyph1.GlyphTransform = 'Transform2'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from calculator1
calculator1Display = Show(calculator1, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'Urz'
urzLUT = GetColorTransferFunction('Urz')
urzLUT.RGBPoints = [0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0352941176470588, 1.0, 0.294013553174781, 0.0, 0.796078431372549, 1.0, 2.0080501012388, 1.0, 0.972549019607843, 0.0, 2.0080501012388, 1.0, 0.0, 0.0]
urzLUT.ColorSpace = 'HSV'
urzLUT.NanColor = [0.498039215686, 0.498039215686, 0.498039215686]
urzLUT.NumberOfTableValues = 24
urzLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
calculator1Display.Representation = 'Surface'
calculator1Display.ColorArrayName = ['POINTS', 'Urz']
calculator1Display.LookupTable = urzLUT
calculator1Display.OSPRayScaleArray = 'U'
calculator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1Display.SelectOrientationVectors = 'Urz'
calculator1Display.ScaleFactor = 0.0540000021457672
calculator1Display.SelectScaleArray = 'None'
calculator1Display.GlyphType = 'Arrow'
calculator1Display.GlyphTableIndexArray = 'None'
calculator1Display.GaussianRadius = 0.00270000010728836
calculator1Display.SetScaleArray = ['POINTS', 'U']
calculator1Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator1Display.OpacityArray = ['POINTS', 'U']
calculator1Display.OpacityTransferFunction = 'PiecewiseFunction'
calculator1Display.DataAxesGrid = 'GridAxesRepresentation'
calculator1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
calculator1Display.ScaleTransferFunction.Points = [-1.21660327911377, 0.0, 0.5, 0.0, 1.20250022411346, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
calculator1Display.OpacityTransferFunction.Points = [-1.21660327911377, 0.0, 0.5, 0.0, 1.20250022411346, 1.0, 0.5, 0.0]

# show data from glyph1
glyph1Display = Show(glyph1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = [None, '']
glyph1Display.PointSize = 1.0
glyph1Display.LineWidth = 1.5
glyph1Display.OSPRayScaleArray = 'GlyphVector'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'GlyphVector'
glyph1Display.ScaleFactor = 0.0643569387495518
glyph1Display.SelectScaleArray = 'GlyphVector'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'GlyphVector'
glyph1Display.GaussianRadius = 0.00321784693747759
glyph1Display.SetScaleArray = ['POINTS', 'GlyphVector']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = ['POINTS', 'GlyphVector']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.4024082854607e+38, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.4024082854607e+38, 1.0, 0.5, 0.0]

# show data from extractBlock1
extractBlock1Display = Show(extractBlock1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
extractBlock1Display.Representation = 'Feature Edges'
extractBlock1Display.AmbientColor = [0.0, 0.0, 0.0]
extractBlock1Display.ColorArrayName = ['POINTS', '']
extractBlock1Display.DiffuseColor = [0.0, 0.0, 0.0]
extractBlock1Display.LineWidth = 2.0
extractBlock1Display.SelectTCoordArray = 'None'
extractBlock1Display.SelectNormalArray = 'None'
extractBlock1Display.SelectTangentArray = 'None'
extractBlock1Display.OSPRayScaleArray = 'p'
extractBlock1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractBlock1Display.SelectOrientationVectors = 'U'
extractBlock1Display.ScaleFactor = 0.054000002145767216
extractBlock1Display.SelectScaleArray = 'p'
extractBlock1Display.GlyphType = 'Arrow'
extractBlock1Display.GlyphTableIndexArray = 'p'
extractBlock1Display.GaussianRadius = 0.0027000001072883606
extractBlock1Display.SetScaleArray = ['POINTS', 'p']
extractBlock1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractBlock1Display.OpacityArray = ['POINTS', 'p']
extractBlock1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractBlock1Display.DataAxesGrid = 'GridAxesRepresentation'
extractBlock1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
extractBlock1Display.ScaleTransferFunction.Points = [-6.384536266326904, 0.0, 0.5, 0.0, 4344.63134765625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
extractBlock1Display.OpacityTransferFunction.Points = [-6.384536266326904, 0.0, 0.5, 0.0, 4344.63134765625, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for urzLUT in view renderView1
urzLUTColorBar = GetScalarBar(urzLUT, renderView1)
urzLUTColorBar.WindowLocation = 'AnyLocation'
urzLUTColorBar.Position = [0.6564516642656741, 0.01758444436051413]
urzLUTColorBar.Title = 'z'
urzLUTColorBar.ComponentTitle = '[m]'
urzLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
urzLUTColorBar.TitleFontFamily = 'Times'
urzLUTColorBar.TitleFontSize = 18
urzLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
urzLUTColorBar.LabelFontFamily = 'Times'
urzLUTColorBar.LabelFontSize = 18
urzLUTColorBar.AutomaticLabelFormat = 0
urzLUTColorBar.DrawTickMarks = 0
urzLUTColorBar.DrawTickLabels = 0
urzLUTColorBar.RangeLabelFormat = '%.0g'
urzLUTColorBar.ScalarBarThickness = 28
urzLUTColorBar.ScalarBarLength = 0.94755485893417

# set color bar visibility
urzLUTColorBar.Visibility = 1

# show color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'Urz'
urzPWF = GetOpacityTransferFunction('Urz')
urzPWF.Points = [0.0, 0.0, 0.5, 0.0, 2.0080501012388, 1.0, 0.5, 0.0]
urzPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(extractBlock1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')