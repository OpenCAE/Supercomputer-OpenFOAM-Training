# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [269, 503]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [-1.11758708953857e-08, 1.60187482833862e-07, 0.270000010728836]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [1.4778688347276543, 1.541712364636296, 1.154425042267844]
renderView1.CameraFocalPoint = [-0.031924245006818404, 0.03191928490183031, 0.27000873253643004]
renderView1.CameraViewUp = [-0.27059805007310006, -0.27059805007309984, 0.923879532511286]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.297376660288923
renderView1.CameraParallelProjection = 1
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(269, 503)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.CaseType = 'Decomposed Case'
pvfoam.MeshRegions = ['internalMesh']

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'nonOrthoAngle'
nonOrthoAngleLUT = GetColorTransferFunction('nonOrthoAngle')
nonOrthoAngleLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 32.5, 0.865003, 0.865003, 0.865003, 65.0, 0.705882, 0.0156863, 0.14902]
nonOrthoAngleLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'nonOrthoAngle'
nonOrthoAnglePWF = GetOpacityTransferFunction('nonOrthoAngle')
nonOrthoAnglePWF.Points = [0.0, 0.0, 0.5, 0.0, 65.0, 1.0, 0.5, 0.0]
nonOrthoAnglePWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Volume'
pvfoamDisplay.ColorArrayName = ['CELLS', 'nonOrthoAngle']
pvfoamDisplay.LookupTable = nonOrthoAngleLUT
pvfoamDisplay.SelectTCoordArray = 'None'
pvfoamDisplay.SelectNormalArray = 'None'
pvfoamDisplay.SelectTangentArray = 'None'
pvfoamDisplay.OSPRayScaleArray = 'aspectRatio'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'None'
pvfoamDisplay.ScaleFactor = 0.054000002145767216
pvfoamDisplay.SelectScaleArray = 'None'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'None'
pvfoamDisplay.GaussianRadius = 0.0027000001072883606
pvfoamDisplay.SetScaleArray = ['POINTS', 'aspectRatio']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = ['POINTS', 'aspectRatio']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
pvfoamDisplay.ScalarOpacityFunction = nonOrthoAnglePWF
pvfoamDisplay.ScalarOpacityUnitDistance = 0.014568520889149969
pvfoamDisplay.OpacityArrayName = ['POINTS', 'aspectRatio']
pvfoamDisplay.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
pvfoamDisplay.ScaleTransferFunction.Points = [1.0041897296905518, 0.0, 0.5, 0.0, 3.012808084487915, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
pvfoamDisplay.OpacityTransferFunction.Points = [1.0041897296905518, 0.0, 0.5, 0.0, 3.012808084487915, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for nonOrthoAngleLUT in view renderView1
nonOrthoAngleLUTColorBar = GetScalarBar(nonOrthoAngleLUT, renderView1)
nonOrthoAngleLUTColorBar.WindowLocation = 'AnyLocation'
nonOrthoAngleLUTColorBar.Position = [0.7385130125341708, 0.033586214337343476]
nonOrthoAngleLUTColorBar.Title = 'nonOrthoAngle'
nonOrthoAngleLUTColorBar.ComponentTitle = ''
nonOrthoAngleLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
nonOrthoAngleLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
nonOrthoAngleLUTColorBar.AutomaticLabelFormat = 0
nonOrthoAngleLUTColorBar.LabelFormat = '%g'
nonOrthoAngleLUTColorBar.AddRangeLabels = 0
nonOrthoAngleLUTColorBar.ScalarBarLength = 0.910708661417323

# set color bar visibility
nonOrthoAngleLUTColorBar.Visibility = 1

# show color legend
pvfoamDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
