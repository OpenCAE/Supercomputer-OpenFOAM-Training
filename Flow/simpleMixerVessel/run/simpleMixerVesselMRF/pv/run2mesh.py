# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [223, 594]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
renderView1.CenterOfRotation = [-1.11758708953857e-08, 1.60187482833862e-07, 0.270000010728836]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [1.5097930685586, 1.50979323992195, 1.15441632046025]
renderView1.CameraFocalPoint = [-1.11758708953857e-08, 1.60187482833862e-07, 0.270000010728836]
renderView1.CameraViewUp = [-0.2705980500730989, -0.2705980500730989, 0.9238795325112865]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.29512004615432474
renderView1.CameraParallelProjection = 1
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(223, 594)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.CaseType = 'Decomposed Case'
pvfoam.Adddimensionalunitstoarraynames = 1
pvfoam.MeshRegions = ['baffle_baffle1', 'baffle_baffle2', 'baffle_baffle3', 'baffle_baffle4', 'bottom', 'shaftMoving_shaftMovingHigh', 'shaftMoving_shaftMovingLow', 'shaftRotating_shaftRotatingHigh', 'shaftRotating_shaftRotatingLow', 'sides', 'top', 'turbine_turbineHigh1', 'turbine_turbineHigh2', 'turbine_turbineLow1', 'turbine_turbineLow2', 'vessel']
pvfoam.Readzones = 1

# create a new 'Extract Block'
extractBlock1 = ExtractBlock(registrationName='ExtractBlock1', Input=pvfoam)
extractBlock1.BlockIndices = [7, 6, 5, 11, 10, 9, 8, 15, 14, 13, 12, 3, 2, 16]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Wireframe'
pvfoamDisplay.AmbientColor = [0.0, 0.0, 0.0]
pvfoamDisplay.ColorArrayName = [None, '']
pvfoamDisplay.DiffuseColor = [0.0, 0.0, 0.0]
pvfoamDisplay.Opacity = 0.2
pvfoamDisplay.SelectTCoordArray = 'None'
pvfoamDisplay.SelectNormalArray = 'None'
pvfoamDisplay.SelectTangentArray = 'None'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'None'
pvfoamDisplay.ScaleFactor = 0.054000002145767216
pvfoamDisplay.SelectScaleArray = 'None'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'None'
pvfoamDisplay.GaussianRadius = 0.0027000001072883606
pvfoamDisplay.SetScaleArray = [None, '']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = [None, '']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
pvfoamDisplay.ScalarOpacityUnitDistance = 0.014568520889149969
pvfoamDisplay.OpacityArrayName = ['FIELD', 'CasePath']
pvfoamDisplay.ExtractedBlockIndex = 1

# show data from extractBlock1
extractBlock1Display = Show(extractBlock1, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'vtkBlockColors'
vtkBlockColorsLUT = GetColorTransferFunction('vtkBlockColors')
vtkBlockColorsLUT.InterpretValuesAsCategories = 1
vtkBlockColorsLUT.AnnotationsInitialized = 1
vtkBlockColorsLUT.Annotations = ['0', '0', '1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '7', '7', '8', '8', '9', '9', '10', '10', '11', '11']
vtkBlockColorsLUT.ActiveAnnotatedValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11']
vtkBlockColorsLUT.IndexedColors = [1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.63, 0.63, 1.0, 0.67, 0.5, 0.33, 1.0, 0.5, 0.75, 0.53, 0.35, 0.7, 1.0, 0.75, 0.5]

# trace defaults for the display properties.
extractBlock1Display.Representation = 'Surface'
extractBlock1Display.ColorArrayName = ['FIELD', 'vtkBlockColors']
extractBlock1Display.LookupTable = vtkBlockColorsLUT
extractBlock1Display.SelectTCoordArray = 'None'
extractBlock1Display.SelectNormalArray = 'None'
extractBlock1Display.SelectTangentArray = 'None'
extractBlock1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractBlock1Display.SelectOrientationVectors = 'None'
extractBlock1Display.ScaleFactor = 0.054000002145767216
extractBlock1Display.SelectScaleArray = 'None'
extractBlock1Display.GlyphType = 'Arrow'
extractBlock1Display.GlyphTableIndexArray = 'None'
extractBlock1Display.GaussianRadius = 0.0027000001072883606
extractBlock1Display.SetScaleArray = [None, '']
extractBlock1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractBlock1Display.OpacityArray = [None, '']
extractBlock1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractBlock1Display.DataAxesGrid = 'GridAxesRepresentation'
extractBlock1Display.PolarAxes = 'PolarAxesRepresentation'

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'vtkBlockColors'
vtkBlockColorsPWF = GetOpacityTransferFunction('vtkBlockColors')

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
