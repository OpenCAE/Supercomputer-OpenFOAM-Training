# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [194, 503]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [-1.11758708953857e-08, 1.60187482833862e-07, 0.270000010728836]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [1.5097930685586, 1.50979323992195, 1.15441632046025]
renderView1.CameraFocalPoint = [-1.11758708953857e-08, 1.60187482833862e-07, 0.270000010728836]
renderView1.CameraViewUp = [-0.2705980500730989, -0.2705980500730989, 0.9238795325112865]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.2938295653841314
renderView1.CameraParallelProjection = 1
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(194, 503)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.CaseType = 'Decomposed Case'
pvfoam.MeshRegions = ['baffle_baffle1', 'baffle_baffle2', 'baffle_baffle3', 'baffle_baffle4', 'bottom', 'internalMesh', 'shaftMoving_shaftMovingHigh', 'shaftMoving_shaftMovingLow', 'shaftRotating_shaftRotatingHigh', 'shaftRotating_shaftRotatingLow', 'sides', 'top', 'turbine_turbineHigh1', 'turbine_turbineHigh2', 'turbine_turbineLow1', 'turbine_turbineLow2', 'vessel']
pvfoam.CellArrays = ['U', 'Y', 'alpha.water', 'epsilon', 'k', 'nut', 'omega', 'p_rgh']

# create a new 'Extract Block'
extractBlock2 = ExtractBlock(registrationName='ExtractBlock2', Input=pvfoam)
extractBlock2.BlockIndices = [1]

# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=extractBlock2)
threshold1.Scalars = ['CELLS', 'alpha.water']
threshold1.ThresholdRange = [0.5, 1.0]

# create a new 'Extract Block'
extractBlock1 = ExtractBlock(registrationName='ExtractBlock1', Input=pvfoam)
extractBlock1.BlockIndices = [13, 14, 15, 8, 9, 10, 11, 4, 6, 7, 16, 17, 3, 12]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Wireframe'
pvfoamDisplay.AmbientColor = [0.0, 0.0, 0.0]
pvfoamDisplay.ColorArrayName = [None, '']
pvfoamDisplay.DiffuseColor = [0.0, 0.0, 0.0]
pvfoamDisplay.Opacity = 0.2
pvfoamDisplay.SelectTCoordArray = 'None'
pvfoamDisplay.SelectNormalArray = 'None'
pvfoamDisplay.SelectTangentArray = 'None'
pvfoamDisplay.OSPRayScaleArray = 'Y'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'None'
pvfoamDisplay.ScaleFactor = 0.054000002145767216
pvfoamDisplay.SelectScaleArray = 'None'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'None'
pvfoamDisplay.GaussianRadius = 0.0027000001072883606
pvfoamDisplay.SetScaleArray = ['POINTS', 'Y']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = ['POINTS', 'Y']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
pvfoamDisplay.ScalarOpacityUnitDistance = 0.014568520889149969
pvfoamDisplay.OpacityArrayName = ['POINTS', 'Y']
pvfoamDisplay.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
pvfoamDisplay.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.02847369946539402, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
pvfoamDisplay.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.02847369946539402, 1.0, 0.5, 0.0]

# show data from extractBlock1
extractBlock1Display = Show(extractBlock1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
extractBlock1Display.Representation = 'Surface'
extractBlock1Display.ColorArrayName = [None, '']
extractBlock1Display.SelectTCoordArray = 'None'
extractBlock1Display.SelectNormalArray = 'None'
extractBlock1Display.SelectTangentArray = 'None'
extractBlock1Display.OSPRayScaleArray = 'Y'
extractBlock1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractBlock1Display.SelectOrientationVectors = 'None'
extractBlock1Display.ScaleFactor = 0.054000002145767216
extractBlock1Display.SelectScaleArray = 'None'
extractBlock1Display.GlyphType = 'Arrow'
extractBlock1Display.GlyphTableIndexArray = 'None'
extractBlock1Display.GaussianRadius = 0.0027000001072883606
extractBlock1Display.SetScaleArray = ['POINTS', 'Y']
extractBlock1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractBlock1Display.OpacityArray = ['POINTS', 'Y']
extractBlock1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractBlock1Display.DataAxesGrid = 'GridAxesRepresentation'
extractBlock1Display.PolarAxes = 'PolarAxesRepresentation'
extractBlock1Display.ScalarOpacityUnitDistance = 0.014568520889149969
extractBlock1Display.OpacityArrayName = ['POINTS', 'Y']
extractBlock1Display.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
extractBlock1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.02847369946539402, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
extractBlock1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.02847369946539402, 1.0, 0.5, 0.0]

# show data from threshold1
threshold1Display = Show(threshold1, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'Y'
yLUT = GetColorTransferFunction('Y')
yLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.01423684973269701, 0.865003, 0.865003, 0.865003, 0.02847369946539402, 0.705882, 0.0156863, 0.14902]
yLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'Y'
yPWF = GetOpacityTransferFunction('Y')
yPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.02847369946539402, 1.0, 0.5, 0.0]
yPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
threshold1Display.Representation = 'Surface'
threshold1Display.ColorArrayName = ['CELLS', 'Y']
threshold1Display.LookupTable = yLUT
threshold1Display.SelectTCoordArray = 'None'
threshold1Display.SelectNormalArray = 'None'
threshold1Display.SelectTangentArray = 'None'
threshold1Display.OSPRayScaleArray = 'Y'
threshold1Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold1Display.SelectOrientationVectors = 'None'
threshold1Display.ScaleFactor = 0.044000771641731266
threshold1Display.SelectScaleArray = 'None'
threshold1Display.GlyphType = 'Arrow'
threshold1Display.GlyphTableIndexArray = 'None'
threshold1Display.GaussianRadius = 0.002200038582086563
threshold1Display.SetScaleArray = ['POINTS', 'Y']
threshold1Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold1Display.OpacityArray = ['POINTS', 'Y']
threshold1Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold1Display.DataAxesGrid = 'GridAxesRepresentation'
threshold1Display.PolarAxes = 'PolarAxesRepresentation'
threshold1Display.ScalarOpacityFunction = yPWF
threshold1Display.ScalarOpacityUnitDistance = 0.013161931527535817
threshold1Display.OpacityArrayName = ['POINTS', 'Y']
threshold1Display.ExtractedBlockIndex = 1

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.02847369946539402, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.02847369946539402, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for yLUT in view renderView1
yLUTColorBar = GetScalarBar(yLUT, renderView1)
yLUTColorBar.WindowLocation = 'AnyLocation'
yLUTColorBar.Position = [0.30220748859847457, 0.1510934393638173]
yLUTColorBar.Title = 'Y'
yLUTColorBar.ComponentTitle = ''
yLUTColorBar.ScalarBarLength = 0.4234393638170972

# set color bar visibility
yLUTColorBar.Visibility = 1

# show color legend
threshold1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
