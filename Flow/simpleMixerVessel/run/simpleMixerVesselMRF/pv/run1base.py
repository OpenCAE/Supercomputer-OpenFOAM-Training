# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [303, 600]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [-1.11758708953857e-08, 1.60187482833862e-07, 0.270000010728836]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [1.5097930685586, 1.50979323992195, 1.15441632046025]
renderView1.CameraFocalPoint = [-1.11758708953857e-08, 1.60187482833862e-07, 0.270000010728836]
renderView1.CameraViewUp = [-0.2705980500730989, -0.2705980500730989, 0.9238795325112865]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.3124327661451168
renderView1.CameraParallelProjection = 1
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(303, 600)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.MeshRegions = ['bottom', 'sides', 'top']

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1, 'GeometryRepresentation')

# get color transfer function/color map for 'cellDist'
cellDistLUT = GetColorTransferFunction('cellDist')
cellDistLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 23.5, 0.865003, 0.865003, 0.865003, 47.0, 0.705882, 0.0156863, 0.14902]
cellDistLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Surface With Edges'
pvfoamDisplay.ColorArrayName = ['CELLS', 'cellDist']
pvfoamDisplay.LookupTable = cellDistLUT
pvfoamDisplay.SelectTCoordArray = 'None'
pvfoamDisplay.SelectNormalArray = 'None'
pvfoamDisplay.SelectTangentArray = 'None'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'None'
pvfoamDisplay.ScaleFactor = 0.054000002145767216
pvfoamDisplay.SelectScaleArray = 'None'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'None'
pvfoamDisplay.GaussianRadius = 0.0027000001072883606
pvfoamDisplay.SetScaleArray = [None, '']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = [None, '']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'cellDist'
cellDistPWF = GetOpacityTransferFunction('cellDist')
cellDistPWF.Points = [0.0, 0.0, 0.5, 0.0, 47.0, 1.0, 0.5, 0.0]
cellDistPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
