#!/bin/bash
#PJM -L rscgrp=fx-workshop
#PJM -L node=1
#PJM -L elapse=0:30:00
#PJM -S
source ./runShare.sh                             # 共通設定
reconstructParMesh -constant &> log.reconstructParMesh.$jid # 格子の再構築
reconstructPar -fields '(alpha.water Y U)' &> log.reconstructPar.$jid # 解析結果の再構築
# -fields '(alpha.water Y U)' : alpha.water,Y,Uの解析結果のみ
