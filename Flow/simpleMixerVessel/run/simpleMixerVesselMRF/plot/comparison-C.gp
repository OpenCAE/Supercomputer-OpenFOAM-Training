set terminal pdfcairo enhanced color solid font "Times,16" lw 2
set size 0.7,1
set xlabel "N.t"
set ylabel "C/<C>"
set style data line
set xrange [0:80]
set xtics 20
N=300/60
rho=998
Cmean=0.64544
set yrange [0:3]
set output "comparison-Ctop.pdf"
plot \
"../share/expt/Ctop.txt" using 1:2 title "Exp" with p\
,"< cat ../MRF-0/postProcessing/probes/*/Y | sort -n" using (N*$1):($2*rho/Cmean) title "CFD(MRF-0)" with l lw 2\
,"< cat postProcessing/probes/*/Y | sort -n" using (N*$1):($2*rho/Cmean) title "CFD(Present)" with l lw 2
set yrange [0:1.2]
set output "comparison-Cbottom.pdf"
plot \
"../share/expt/Cbottom.txt" using 1:2 title "Exp" with p\
,"< cat ../MRF-0/postProcessing/probes/*/Y | sort -n" using (N*$1):($3*rho/Cmean) title "CFD(MRF-0)" with l lw 2\
,"< cat postProcessing/probes/*/Y | sort -n" using (N*$1):($3*rho/Cmean) title "CFD(Present)" with l lw 2
