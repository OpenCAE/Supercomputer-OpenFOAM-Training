# state file generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [927, 489]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [1.61000001430511, 0.0, 0.499999996873555]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-0.271758686641025, -2.7724400175163, 2.76899733692044]
renderView1.CameraFocalPoint = [2.65338709222251, 1.79815250973452, -1.31912008615726]
renderView1.CameraViewUp = [0.22027131639954403, 0.5681965861895442, 0.7928639143094861]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 1.75843681978879

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(927, 489)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(registrationName='pv.foam', FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.MeshRegions = ['internalMesh']

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1, 'UnstructuredGridRepresentation')

# get color transfer function/color map for 'cellDist'
cellDistLUT = GetColorTransferFunction('cellDist')
cellDistLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 23.5, 0.865003, 0.865003, 0.865003, 47.0, 0.705882, 0.0156863, 0.14902]
cellDistLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'cellDist'
cellDistPWF = GetOpacityTransferFunction('cellDist')
cellDistPWF.Points = [0.0, 0.0, 0.5, 0.0, 47.0, 1.0, 0.5, 0.0]
cellDistPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Surface With Edges'
pvfoamDisplay.ColorArrayName = ['CELLS', 'cellDist']
pvfoamDisplay.LookupTable = cellDistLUT
pvfoamDisplay.SelectTCoordArray = 'None'
pvfoamDisplay.SelectNormalArray = 'None'
pvfoamDisplay.SelectTangentArray = 'None'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'None'
pvfoamDisplay.ScaleFactor = 0.3240000009536743
pvfoamDisplay.SelectScaleArray = 'None'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'None'
pvfoamDisplay.GaussianRadius = 0.016200000047683717
pvfoamDisplay.SetScaleArray = [None, '']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = [None, '']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'
pvfoamDisplay.ScalarOpacityFunction = cellDistPWF
pvfoamDisplay.ScalarOpacityUnitDistance = 0.09556340873035993
pvfoamDisplay.OpacityArrayName = ['FIELD', 'CasePath']
pvfoamDisplay.ExtractedBlockIndex = 1

# setup the color legend parameters for each legend in this view

# get color legend/bar for cellDistLUT in view renderView1
cellDistLUTColorBar = GetScalarBar(cellDistLUT, renderView1)
cellDistLUTColorBar.Title = 'cellDist'
cellDistLUTColorBar.ComponentTitle = ''

# set color bar visibility
cellDistLUTColorBar.Visibility = 1

# show color legend
pvfoamDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
