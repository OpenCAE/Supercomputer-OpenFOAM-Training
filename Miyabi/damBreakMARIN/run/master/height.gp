set term pdf font "helvetica,17" linewidth 1.5 #端末,フォント,線幅
set xrange [0:2] #X軸のレンジ
set style data line #データのプロットスタイルはline
set key bottom
do for [i=1:4:1] { #実験値とCFD結果(probes関数出力)のプロット
  set yrange [*:*] #X軸のレンジ
  set output sprintf("height-H%d.pdf", i)
  plot '../SPHERIC_Test2/case.txt' u 1:i+9 t sprintf("H%d(Exp.)", i)\
  ,'< cat postProcessing/interfaceHeight/0/height.dat | sort -g' u 1:i+4 t sprintf("H%d(CFD)", i)\
}
