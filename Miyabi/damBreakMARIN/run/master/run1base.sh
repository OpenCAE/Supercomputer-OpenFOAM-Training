#!/bin/bash
#PBS -q lecture-mig
#PBS -l select=1
#PBS -l walltime=00:15:00
#PBS -W group_list=gt00
cd ${PBS_O_WORKDIR} # ジョブ投入ディレクトリに移動
source ./runShare.sh # 共通設定
blockMesh &> log.blockMesh.$jid # ベース格子生成
decomposePar -cellDist &> log.decomposePar.$jid # 領域分割
# -cellDist: 領域分割の可視化の場cellDistを出力する(必須ではない)
