#!/bin/bash
module purge # 初期設定でloadされているnvidiaのmoduleはgccと衝突するので全てunload
module load gcc/12.4.0 # GCCのmoduleをload
module load cuda/12.6 # CUDAのmoduleをload
module load ompi-cuda/4.1.6-12.6 # OpenMPIのmoduleをload
module load amgx/2.5.0 # AmgXのmoduleをload
module load openfoam-gpu/v2406 # GPU版OpenFOAMのmoduleをload
source ${WM_PROJECT_DIR}/etc/bashrc # OpenFOAMの環境設定
eval $(foamEtcFile -sh -config petsc -- -force) # PETScの設定
source $WM_PROJECT_DIR/bin/tools/RunFunctions # 実行用関数定義
jid=${PBS_JOBID} # ジョブIDをjid変数に代入
