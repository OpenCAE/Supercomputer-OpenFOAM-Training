#!/bin/bash
#PBS -q lecture-mig
#PBS -l select=1
#PBS -l walltime=00:15:00
#PBS -W group_list=gt00
cd ${PBS_O_WORKDIR} # ジョブ投入ディレクトリに移動
source ./runShare.sh # 共通設定
reconstructParMesh -constant &> log.reconstructParMesh.$jid # 格子の再構築
reconstructPar &> log.reconstructPar.$jid # 解析結果の再構築
