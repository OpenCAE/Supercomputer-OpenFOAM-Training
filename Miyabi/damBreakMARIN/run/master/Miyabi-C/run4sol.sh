#!/bin/bash
#PBS -q lecture-c
#PBS -l select=1:mpiprocs=112
#PBS -l walltime=00:15:00
#PBS -W group_list=gt00
cd ${PBS_O_WORKDIR} # ジョブ投入ディレクトリに移動
source ./runShare.sh # 共通設定
application=$(getApplication) # アプリケーション名を取得
mpirun $application -parallel -lib petscFoam &> log.$application.$jid # 並列流体解析
