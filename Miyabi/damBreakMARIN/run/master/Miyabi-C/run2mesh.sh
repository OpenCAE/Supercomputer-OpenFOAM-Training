#!/bin/bash
#PBS -q lecture-c
#PBS -l select=1:mpiprocs=112
#PBS -l walltime=00:15:00
#PBS -W group_list=gt00
cd ${PBS_O_WORKDIR} # ジョブ投入ディレクトリに移動
source ./runShare.sh # 共通設定
mpirun snappyHexMesh -parallel -overwrite &> log.snappyHexMesh.$jid # 並列格子生成
mpirun renumberMesh  -parallel -overwrite &> log.renumberMesh.$jid # 格子順変更による行列バンド幅縮小
mpirun checkMesh     -parallel -constant  &> log.checkMesh.$jid # 格子の品質チェック
