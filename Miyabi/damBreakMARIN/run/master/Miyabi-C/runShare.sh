#!/bin/bash
module purge # 初期設定でloadされているintelのmoduleはgccと衝突するので全てunload
module load gcc/11.4.1 # GCCのmoduleをload
module load impi/2021.10.0 # Intel MPIのmoduleをload
module load openfoam/v2406 # OpenFOAMのmoduleをload
source ${WM_PROJECT_DIR}/etc/bashrc # OpenFOAMの環境設定
source $WM_PROJECT_DIR/bin/tools/RunFunctions # 実行用関数定義
jid=${PBS_JOBID} # ジョブIDをjid変数に代入
