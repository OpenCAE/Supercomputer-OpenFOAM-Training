``` Command
## 講習会レポジトリの複製 (Open OnDemandのDesktop環境におけるTerminalで実行)
# 作業用ディレクトリの作成 (mkdir: make directories)
mkdir /work/gt00/$(id -un)/opt
# シンボリックリンク作成 (ln: make links between files)
ln -s /work/gt00/$(id -un)/opt ./
# 作業用ディレクトリへ移動 (cd: change the current directory to dir)
cd ~/opt
# 講習会レポジトリの複製   
git clone https://gitlab.com/OpenCAE/Supercomputer-OpenFOAM-Training.git
# レポジトリのディレクトリへ移動
cd Supercomputer-OpenFOAM-Training
# 本講習会のブランチへの切り替え
git checkout Information-Technology-Center-The-University-of-Tokyo-20250129
# ブランチ名の表示
git branch
```
``` Command
## 利用可能な全てのmoduleの表示
show_module -a
# 表示が長いので，パイプ機能(|)でページャーのlessに渡して検索できるようにする
show_module -a | less
# lessのコマンド例: h:ヘルプ, スペース:次の画面, b:前の画面, /正規表現:正規表現検索, q:終了
```
``` Command
## lessのコマンドでopenfoamを検索
/openfoam
```
``` Command
## lessを終了
q
```
``` Command
## 現環境で利用可能なmoduleの表示
module avail -l
# 自動的にlessにパイプされているので，openfoamを検索する．
/openfoam
# BaseCompilerやMPIのmoduleをloadしていないので，OpenFOAMのmoduleは表示されない
```
``` Command
## BaseCompilerとMPIのmoduleのload
module load gcc/11.4.1
module load impi/2021.10.0
```
``` Command
## 現環境で利用可能なmoduleの表示
module avail -l
```
``` Command
## OpenFOAM v2406のmoduleのload
module load openfoam/v2406
```
``` Command
## moduleのhelp表示
module help openfoam/v2406
```
``` Command
## OpenFOAMの環境設定
source $WM_PROJECT_DIR/etc/bashrc
```
``` Command
## バージョンの表示
foamVersion
```
``` Command
## 環境設定手順をalias(別名)として登録するスクリプトを表示
cat ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/.bashrc.d/openfoam
```
``` Command
## OpenFOAMの環境設定のalias化
source ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/.bashrc.d/openfoam 
```
``` Command
## aliasの確認
alias
```
``` Command
## プリポスト・ログインノードC用OpenFOAM v2406の環境設定aliasの実行
OFv2406
```
``` Command
## bashシェル実行時にalias登録を自動的に行うように設定をコピー
mkdir ~/.bashrc.d
cp ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/.bashrc.d/openfoam ~/.bashrc.d/
```
``` Command
## 作成済の解析ケースのコピー
cd ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/damBreakMARIN/run/ # 実行ディレクトリ
cp -r master/ case-0 # デフォルトケースをコピー．ディレクトリなので-r(recursive)オプションが必要
cd case-0/ # ケースディレクトリに移動
```
``` Command
## 混雑状況の確認
qstat --rsc -b # 投げられているジョブ数
qstat --rscuse # ノード，および，MIG インスタンスの利用状況表示
```
``` Command
## ベース格子生成・領域分割のジョブ投入
qsub run1base.sh
```
``` Command
## ジョブの状況確認
qstat
```
``` Command
## blockMeshのログ確認
less log.blockMesh.*
# *の場所には本来JOB IDを打つ必要があるが，ワイルドカードの*でマッチする
# スペース等で進め，q で終了
```
``` Command
## decomposeParのログの確認(領域分割のジョブ終了後)
less log.decomposePar.*
# [less] Maxの検索
/Max
# [less] 一行戻る
C-p # カーソル↑やvi的なコマンド(j，C-j, y, C-y)でも良い(hによるヘルプ参照)
# [less] lessの終了
q
```
``` Command
## ParaViewによるOpenFOAMデータ可視化用ダミーファイル作成
# ダミーファイル(拡張子は.foam．ファイル名は任意)を作成
touch pv.foam
```
``` Command
## プリポストノード用のparaviewのmoduleをload
module load paraview
```
``` Command
## paraviewをバックグラウンドジョブで起動
paraview pv.foam &
```
``` Command
## 格子生成のジョブ投入
qsub run2mesh.sh
```
``` Command
## ログのモニター
tail -f log.*
# いつでもC-cで終了して良い
```
``` Command
## snappyHexMeshのログ確認
less log.snappyHexMesh.*
# [less] 末尾にジャンプ(大文字のG) → 終了
G
q
```
``` Command
## renumberMeshのログ確認 (適宜qで終了)
less log.renumberMesh.*
```
``` Command
## checkMeshのログ確認 (適宜qで終了)
less log.checkMesh.*
```
``` Command
paraview pv.foam &
```
``` Command
## 初期値設定ジョブの投入
qsub run3init.sh
```
``` Command
## OpenFOAMの環境設定aliasの実行(既に実行している場合には不要)
OFv2406
```
``` Command
## OpenFOAMの設定ファイル例の表示と検索
tree $FOAM_ETC/caseDicts | less # /probes でprobesを検索する．適宜qで終了
```
``` Command
## 流体解析実行
qsub run4sol.sh
```
``` Command
## OpenFOAMの環境設定aliasの実行(既に実行している場合には不要)
OFv2406
```
``` Command
## 圧力時系列のモニター
foamMonitor -r 1 postProcessing/probes/0/p & # -r :更新秒(デフォルト10)
# foamMonitorのプロットを強制終了させるには，以下を実行する
killall gnuplot gnuplot_x11
```
``` Command
## gnuplotによるプロット
gnuplot pressure.gp
```
``` Command
## firefoxによるプロット結果閲覧(firefoxで閲覧すると図をタブで切り替えられて便利)
firefox *.pdf &
```
``` Command
paraview pv.foam &
```
``` Command
## ParaViewの起動
paraview postProcessing/isoSurface/2/isoSurface.vtp &
```
``` Command
## 全時刻に対する可視化画像の作成
./Allrun.animImage
```
``` Command
## 動画の作成
./Allrun.anim
```
``` Command
## 動画の再生
firefox anim/*.gif &
```
``` Command
## 領域分割された格子と計算結果の再構築ジョブの投入
qsub reconstruct.sh
# 実行しなくて良い  
```
``` Command
## ケースの複製
cd ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/damBreakMARIN/run/ # 実行ディレクトリ
cp -r master/ case-gpu # デフォルトケースをコピー
cd case-0/ # case-0ディレクリに移動
cp --parents -r processor*/{0,constant} ../case-gpu/ # 格子と初期値を複製(--parents: ディレクトリ名付き)
cd ../case-gpu/
# 圧力場p_rghの線形方程式の線形ソルバとしてAmgXを使うように変更
cp system/fvSolution.AmgX system/fvSolution
```
``` Command
## 流体解析実行
qsub run4sol.sh
```
``` Command
## 解析ソルバの計算時間比較
cd ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/damBreakMARIN/run/
tail */log.interFoam* | grep -e == -e Time
```
``` Command
## interIsoFoamを用いた解析ケースの作成
cd ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/damBreakMARIN/run/ # 実行ディレクトリ
cp -r master/ case-Iso # デフォルトケースをコピー
cd case-0/ # case-0ディレクリに移動
cp --parents -r processor*/{0,constant} ../case-Iso/ # 格子と初期値を複製(--parents: ディレクトリ名付き)
cd ../case-Iso # ケースディレクトリに移動
```
``` Command
## 実行アプリケーション名の変更(エディタはvi, emacs, nano等何でも良い)
vi system/controlDict
```
``` Command
## 液相率のVOF解法設定の変更
vi system/fvSolution
```
``` Command
## 流体解析実行
qsub run4sol.sh
## 圧力時系列のモニター
foamMonitor -r 1 postProcessing/probes/0/p &
```
``` Command
## 基本ケースとの圧力時系列の比較プロット
gnuplot comparison-pressure.gp
```
``` Command
## firefoxによるプロット結果閲覧
firefox comparison-pressure*.pdf &
```
``` Command
## 解適合格子ケースの作成
cd ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/damBreakMARIN/run/ # 実行ディレクトリ
cp -r master/ case-AMR
cd case-0/ # case-0ディレクリに移動
cp --parents -r processor*/{0,constant} ../case-AMR/ # 格子と初期値を複製(--parents: ディレクトリ名付き)
cd ../case-AMR # ケースディレクトリに移動
```
``` Command
## 動的格子設定の変更
vi constant/dynamicMeshDict
```
``` Command
## 流体解析実行
qsub run4sol.sh
qstat
```
``` Command
## 圧力時系列のモニター
# ジョブ実行後にpostProcessing/probes/0/pができるまで時間がかかるので，暫くしてから実行
foamMonitor -r 1 postProcessing/probes/0/p &
# foamMonitorのプロットを強制終了させるには，以下を実行する
killall gnuplot gnuplot_x11
```
``` Command
## 基本ケースとの圧力時系列の比較プロット
gnuplot comparison-pressure.gp
```
``` Command
## firefoxによるプロット結果閲覧
firefox comparison-pressure*.pdf &
```
``` Command
## 基本ケースのコピー・設定変更
cd ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/damBreakMARIN/run/
cp -r master/ case-XX #XXは任意
cd case-XX/
#エディタで設定変更
```
``` Command
## ステップジョブの投入・サブジョブの確認
../../../bin/Allrun.batch
qstat
# run4sol.sh実行後，暫くしてから次に進む
foamMonitor -r 1 postProcessing/probes/0/p &
```
``` Command
## 比較プロット
gnuplot comparison-pressure.gp
firefox comparison-pressure*.pdf &
```
``` Command
## interFoamのチュートリアルで使用されている速度Uの移流項スキームを調べる
# grep -r: ディレクトリ検索
grep -r "div(rhoPhi,U)" $FOAM_TUTORIALS/multiphase | less
# 出力略
```
``` Command
## interFoamのチュートリアルで使用されている速度Uの移流項スキームをユニークに表示
# grep -h: ファイル名を非表示
# tr -s "[:blank:]": 空白文字の繰り返しを1文字に置換
# sort | uniq: ソートして重複排除
grep -rh "div(rhoPhi,U)" $FOAM_TUTORIALS/multiphase | tr -s "[:blank:]" | sort | uniq
```
``` Command
## Miyabi-Cでの実行
cd ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/damBreakMARIN/run/
cp -r master/ case-C
cd case-C/
# Miyabi-C用ジョブファイル・decomposeParDict(112並列)コピー
cp -r Miyabi-C/* ./
```
``` Command
## ステップジョブの投入
../../../bin/Allrun.batch
```
``` Command
## 解析ソルバの計算時間比較
cd ~/opt/Supercomputer-OpenFOAM-Training/Miyabi/damBreakMARIN/run/
tail */log.interFoam* | grep -e == -e Time
```
