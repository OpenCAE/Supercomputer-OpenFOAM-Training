#!/bin/bash
#PBS -q lecture-g
#PBS -l select=1:mpiprocs=72
#PBS -l walltime=00:15:00
#PBS -W group_list=gt00
cd ${PBS_O_WORKDIR}
module purge
module load gcc/12.4.0
module load cuda/12.6
module load ompi-cuda/4.1.6-12.6
module load amgx/2.5.0
module load openfoam-gpu/v2406
source ${WM_PROJECT_DIR}/etc/bashrc #OpenFOAMの環境設定
foamRunTutorials #foamRunTutorialsコマンドでチュートリアルを実行
